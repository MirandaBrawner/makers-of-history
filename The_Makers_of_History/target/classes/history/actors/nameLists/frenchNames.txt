1794
#gender-neutral given names
Alex
Alexis
Amour
Anastasie
Ariel
Camille
Candide
Claude
Cyrille
Dany
Dominique
Florence
Hyacinthe
Jocelyn
Jordan
José
Lilian
Lou
Lucrèce
Léonide
Modeste
Narcisse
Noam
Placide
Prudence
Robin

#feminine given names
Adrienne
Adèle
Adélaïde
Adélie
Agathe
Agnès
Aimée
Alberte
Albertine
Albine
Alice
Aline
Alison
Alizée
Ambre
Amélie
Anastasie
Anaïs
Andréa
Angeline
Angélique
Annabelle
Anne
Anne-Laure
Anne-Marie
Anne-Sophie
Annette
Antoinette
Arianne
Arlette
Astride
Athénaïs
Aurore
Aurélie
Bernadette
Bernardine
Blanche
Brigitte
Béatrice
Bérénice
Carine
Carole
Caroline
Catherine
Cerise
Chantal
Charline
Charlotte
Chloé
Christine
Claire
Clarisse
Claudine
Clémentine
Constance
Cosette
Cécile
Céleste
Céline
Danielle
Daphné
Delphine
Denise
Diane
Dieudonnée
Doriane
Dorothée
Désirée
Eléonore
Emmanuelle
Ernestine
Estelle
Esther
Eugénie
Eveline
Fabienne
Fleur
Flore
Francine
Françoise
Félicie
Félicité
Gabrielle
Geneviève
Georgette
Georgine
Giselle
Géraldine
Henriette
Hermine
Hortense
Héloïse
Hélène
Inès
Irène
Isabelle
Jacinthe
Jacqueline
Jasmine
Jeanette
Jeanine
Jeanne
Joséphine
Judith
Julie
Julienne
Juliette
Justine
Karine
Laure
Lilianne
Lise
Louise
Lucille
Lydie
Léa
Léonie
Madeline
Marcelle
Margot
Marguerite
Marianne
Marie
Marie Antoinette
Marie-Christine
Marie-Claude
Marie-Hélène
Marie-José
Marie-Laure
Marie-Louise
Marine
Marion
Marthe
Martine
Mathilde
Michelle
Mireille
Monique
Mélanie
Mélisande
Nathalie
Nicole
Noémi
Odette
Olympe
Ophélie
Paulette
Pauline
Priscille
Pétronille
Renée
Roberte
Rosalie
Rose
Roseline
Rosette
Roxane
Sabine
Salomé
Sibylle
Solange
Sophie
Stéphanie
Sybille
Sylvie
Sébastienne
Tatienne
Thérèse
Valentine
Valérie
Violette
Véronique
Yolande
Yseult
Yvette
Yyvonne
Zoé
Édith
Élisabeth
Élise
Élodie
Éloïse
Émilie

#masculine given names
Aaron
Abel
Abraham
Adophe
Adélard
Aimé
Alain
Alban
Albert
Alexandre
Alphonse
Amand
Ambroise
André
Anselme
Antoine
Arien
Aristide
Armand
Arnaud
Arthur
Auguste
Augustin
Aurèle
Barthélémy
Bastien
Baudouin
Benjamin
Benoît
Bernard
Bertrand
Blaise
Bruno
Casimir
Charles
Christophe
Clément
Constantin
Cyril
Cédric
César
Damien
Daniel
David
Denis
Didier
Dieudonné
Désiré
Edgar
Edmond
Emmanuel
Eugène
Fabien
Fabrice
Ferdinand
Fernand
Florent
François
Frédéric
Félix
Gabriel
Gaspard
Gaston
Gauthier
Geoffroy
Georges
Germain
Gilbert
Grégoire
Guillaume
Gustave
Gérald
Gérard
Hector
Henri
Herbert
Hercule
Hilaire
Hippolyte
Horace
Hubert
Hugo
Hugues
Isidore
Ismaël
Jacques
Jean
Jean-Baptiste
Jean-Charles
Jean-Christophe
Jean-Claude
Jean-François
Jean-Jacques
Jean-Luc
Jean-Marc
Jean-Marie
Jean-Michel
Jean-Paul
Jean-Philippe
Jean-Pierre
Joachim
Joseph
Jules
Julien
Justin
Jérôme
Laurent
Lazare
Lothaire
Louis
Luc
Lucas
Lucien
Ludovic
Léandre
Léo
Léonard
Léopold
Manuel
Marc
Marcel
Marius
Martin
Mathieu
Maurice
Maxime
Maximilien
Michel
Napoléon
Nathan
Nathanaël
Nicolas
Noël
Olivier
Oscar
Pascal
Patrice
Paul
Philippe
Pierre
Quentin
Raoul
Raphaël
Renaud
René
Richard
Robert
Rodolphe
Roger
Roland
Romain
Rémy
Salomon
Samuel
Serge
Simon
Stéphane
Sylvain
Sylvestre
Sébastien
Thibault
Thierry
Thomas
Théo
Théodore
Toussaint
Tristan
Urbain
Valentin
Valère
Valéry
Victor
Vivien
Yves
Édouard
Émile
Émilien
Éric
Étienne

#family names
Albert
Allard
André
Arnaud
Aubert
Babin
Barbier
Barre
Baudin
Beaufort
Beaulieu
Beaumont
Bellerose
Belmont
Berger
Bernard
Bertrand
Blaise
Blanc
Blanchard
Bonfils
Bonheur
Bonnaire
Bonnet
Bouchard
Bourdillon
Béranger
Charbonneau
Charles
Charpentier
Chastain
Chauvin
Chevalier
Chevrolet
Christian
Cloutier
Clément
Colbert
Côté
David
De La Fontaine
Delcroix
Denis
Deschamps
Descoteaux
Desjardins
Desroches
Desrosiers
Dubois
Duchamp
Dufort
Dumont
Dupont
Durand
Duval
Fabien
Faure
Fournier
François
Gagnon
Gauthier
Girard
Granger
Guillaume
Guérin
Gérard
Géroux
Hébert
Jaques
Jean
Julien
Labelle
Lacroix
Lambert
Laurent
Lavigne
Lavoie
Lebeau
Leblanc
Lebrun
Leclerc
Lefebvre
Legrand
Leroy
Louis
Lucas
Lyon
Léon
Lévesque
Marchand
Marie
Martel
Martin
Mathieu
Mercier
Michel
Monet
Montagne
Moreau
Morel
Moulin
Neuville
Nicolas
Olivier
Paul
Pellé
Perrault
Perrin
Perrot
Petit
Philippe
Pierre
Plourde
Poirier
Poulin
Périgord
Renard
Renaud
Richard
Richard
Robert
Roche
Roger
Rose
Rousseau
Roux
Roy
Salomon
Samtre
Samuel
Sault
Segal
Sergeant
Simon
St Martin
St Pierre
Tailler
Tasse
Thibault
Thomas
Travert
Tremblay
Trudeau
Victor
Vidal
Villeneuve
Vincent
Émile

#patronyms
[FATHER]

##
Sources: 
"French Names". Behind the Name.
https://www.behindthename.com/names/usage/french
"List of most common surnames in Europe". Wikipedia.
https://en.wikipedia.org/wiki/List_of_most_common_surnames_in_Europe