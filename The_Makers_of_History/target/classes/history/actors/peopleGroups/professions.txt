[0]ID;[1]Plural;[2]Singular;[3]Parent Groups
2;Manufacturers;s;
3;Farmers;s;
4;Ranchers;s;
5;Hunter-Gatherers;s;
6;Unspecialized Workers;s;
7;Miners;s;
8;Household Workers;s;
9;Artists;s;
10;Soldiers;s;
11;Transportation Workers;s;
12;Medical Workers;s;
13;Hospitality Workers;s;
14;Academic Workers;s;
15;Finance Workers;s;
16;Students;s;
17;Police Officers;s;18
18;Judicial Workers;s;
19;Politicians;s;
20;Retailers;s;
21;Religious Workers;s;
22;Fishers;s;
23;Scribes;s;
24;Visual Artists;s;9
25;Writers;s;
26;Performers;s;9
27;City Planners;s;9
28;Performance Assistants;s;9
29;Teachers;s;14
30;School Staff Members;s;14
31;Researchers;s;14
32;Performance Directors;s;9
33;Software Developers;s;9
34;Assistant Priests;s;21
35;Monastery Members;s;21
36;Monastic Priests;s;21
37;Parish Priests;s;21
38;High Priests;s;21
39;Missionaries;Missionary;21
40;Sailors;s;11
41;Pilots;s;11
42;Air Traffic Controllers;s;11
43;Professional Drivers;s;11
44;Athletes;s;
45;Journalists;s;
46;Columnists;s;25,45
47;Video Journalists;s;45,26
48;Radio Journalists;s;45,26
49;Judges;s;18,19
50;Lawyers;s;18
51;Corrections Officers;s;18
52;Security Guards;s;
53;Technicians;s;
54;Plumbers;s;53
55;Electricians;s;53
56;Footsoldiers;s;10
57;Cavaliers;s;10
58;Artillery Soliders;s;10,53
59;Military Land Vehicle Operators;s;10,43
60;Sailors of the Navy;Sailor of the Navy;10,40
61;Military Pilots;s;10,41
62;Legislators;s;19
63;Executive Politicians;s;19
64;Civilian Sailors;s;40
65;Civilian Pilots;s;41
66;Civilian Professional Drivers;s;43