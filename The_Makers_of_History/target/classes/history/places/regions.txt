[0]ID;[1]Name;[2]Parent Regions
0;The World;
1;Afro-Eurasia;0
2;The Americas;0
3;Australia;0
4;Africa;1
5;Eurasia;1
6;North America;2
7;South America;2
8;The Caribbean Islands;2
9;Europe;5
10;Southwest Asia;5
11;South Asia;5
12;East Asia;5
13;Nusantara;5
14;Central Asia;5
15;North Eurasia;5
16;Antarctica;0
17;The Pacific Islands;0
18;North Africa;4,19
19;The Lands of the Mediterranean;1
20;Kalaallit Nunaat;0
21;Papua;13,17
22;Borneo;13
23;The Islands of the Western Indian Ocean;0
24;Madagascar;23,4
25;Qikiqtaaluk;29
26;Sumatra;13
27;The Japanese Islands;12
28;Honshu;27
29;The Canadian Arctic Islands;6
30;Kitlineq;29
31;Great Britain;9
32;Umingmak Nuna;29
33;Sulawesi;13
34;Aotearoa;17
35;Te Waipounamu;34
36;Java;13
37;Te Ika-a-Maui;34
38;The Philippine Archipelago;12
39;Luzon;38
40;Northeast North America;6
41;Newfoundland;40
42;Cuba;8
43;Iceland;9
44;Mindanao;38
45;Ireland;9
46;Hokkaido;27
47;Hispaniola;8
48;Sakhalin;12
49;Banks Island;29
50;Sri Lanka;11
51;Tasmania;3
52;Tatlurutit;29
53;Alexander Island;16
54;Isla Grande de Tierra del Fuego;7
55;Novaya Zemlya;9
56;North Novaya Zemlya;55
57;South Novaya Zemlya;55
58;Berkner Island;16
59;Melville Island;29
60;Axel Heiberg Island;29
61;Shugliaq;29
62;Maraj�;7
63;Svalbard;9
64;Kyushu;27
65;New Britain;17
66;Taiwan;12
67;Prince of Wales Island;29
68;Hainan;12
69;The North American Pacific Northwest;40
70;Vancouver Island;69
71;Timor;13
72;Sicily;9,19
73;The Iberian Peninsula;9,19
74;The Italian Peninsula;9,19
75;Fennoscandia;9,19
76;The Arabian Peninusla;10
77;Hejaz;76
78;Najd;76
79;Mesopotamia;10
80;The Levant;10
81;The Sahara;4
82;The Sahel;4
83;The Nile Valley;4
84;The Nile Delta;18,83
85;The Central Nile;18,83
86;Inland West Africa;88
87;The Sudanian Savanna;86
88;West Africa;4
89;East Africa;4
90;The Ethiopian Highlands;89
91;The Somalian Coast;89
92;The African Red Sea Coast;4
93;The African Southern Red Sea Coast;92,89
94;The Egyptian Red Sea Coast;92,18
95;Southern Africa;4
96;The Namib Desert;95
97;The African Cape;95
98;Central Africa;4
99;The Congo Basin;98
100;The Chad Basin;98
101;The African Great Lakes Region;89
102;The Lower Gangetic Plain;11
103;Atlantic Equatorial Africa;98
104;The Angolan Coast;98
105;Southeast Coastal Africa;95
106;The Himalayas;5
107;The East Deccan Highlands;11
108;The Lower Brahmaputra Valley;11
109;Malabar;11
110;Meghalaya;11
111;The Mizoram-Manipur-Kachin Region;11
112;The Western Ghats;11
113;Odisha;11
114;The Upper Gangetic Plain;11
115;The Central Deccan Plateau;11
116;The Narmada Valley;11
117;Kathiawar-Gir;11
118;Chhota-Nagpur;11
119;Terai-Duar;11
120;The Runn of Kutch;11
121;The Thar Desert;11
122;Indus River Basin;11