Alp Arslan Statue:
Sirpig, "Alp Arslan". Wikimedia Commons.
https://commons.wikimedia.org/wiki/File:Alp-arslan.jpg

Anawrahta Statue:
Phyo WP. "Statue of Anawrahta in fron of the National Museum". Wikimedia Commons.
https://commons.wikimedia.org/wiki/File:Anawrahta_at_National_museum.JPG

Constantine X Coin:
Classical Numismatic Group, LLC. "Constantino X - histamenon - Sear 1847v (reverse)".
Wikimedia Commons. https://commons.wikimedia.org/wiki/File:Costantino_X_-_histamenon_-_Sear_1847v_(reverse).jpg