let currentPanel = "laws";
function setup() {
	let tabNames = [["Individuals", "person"],
		["Families", "families"],
		["Population", "population"],
		["Government", "government"],
		["Laws", "laws"],
		["Organizations", "organizations"],
		["Economy", "economy"],
		["Technology", "technology"],
		["Nature", "nature"]];
	let navBarContent = "";
	for (row in tabNames) {
		console.log(row);
		let visibleText = tabNames[row][0];
		console.log(visibleText);
		let id = tabNames[row][1];
		console.log(id);
		navBarContent += "<button class=\"panelSelector\"";
		navBarContent += " id=\'" + id + "\'";
		navBarContent += " onclick=\"selectPanel('" + id + "');\">";
		navBarContent += visibleText + "  </button>";
	}
	document.getElementById("navBar").innerHTML = navBarContent;
	selectPanel(currentPanel);
}

function selectPanel(panel) {
	let currentPanel = panel;
	let tabList = document.getElementsByClassName("panelSelector");
	for (i = 0; i < tabList.length; i++) {
		tabList[i].style.backgroundColor = "rgb(255, 255, 255, 0.75)";
		tabList[i].style.color = "darkslategrey";
	}
	document.getElementById(panel).style.backgroundColor = "darkslategrey";
	document.getElementById(panel).style.color = "white";
	
	let mainPanelContent = "";
	if (panel == "person") {
		let found = false;
		let personIndex = -1;
		for (r = 0; !found && r < characters.length; r++) {
			if (characters[r][0] == selectedPerson) {
				found = true;
				personIndex = r;
			}
		}
		if (found) {
			mainPanelContent += "<h1 class=\"panelTitle\">" + selectedPerson + "</h1>";
			let imageURL = characters[personIndex][3];
			mainPanelContent += "<img class=\"portrait\" src=\"" + imageURL + "\"></img>";
			let compoundTitle = characters[personIndex][2];
			let titleArray = compoundTitle.split(":");
			let government = titleArray[0];
			let shortTitle = titleArray[1];
			mainPanelContent += "<p class=\"panelItem\">" + shortTitle + " of the "
			mainPanelContent += government + " </p>";
			mainPanelContent += "<p class=\"panelItem\">Born in " 
			mainPanelContent += characters[personIndex][1]+ "</p>";
		} else {
			mainPanelContent += "<p>We could not find any characters " +
					"that matched your search.</p>";
		}
	} else if (panel == "laws") {
		let found = false;
		let govIndex = -1;
		for (r = 0; !found && r < governments.length; r++) {
			if (governments[r][0] == selectedGovernment) {
				found = true;
				govIndex = r;
			}
		}
		if (found) {
			mainPanelContent += "<h2 class=\"panelTitle\">Laws of "; 
			mainPanelContent += governments[govIndex][1] + governments[govIndex][0] + "</h1>";
			let imageURL = governments[govIndex][3];
			mainPanelContent += "<img class=\"flag\" src=\"" + imageURL + "\"></img>";
			mainPanelContent += "<p><span class=\"label\"> Choose a different jurisdiction</span>";
			mainPanelContent += "<input class=\"smalltextfield\" type=\"text\" name=\"lookupGovernment\" />";
			mainPanelContent += "<button class=\"searchButton\"><img class=\"searchIcon\" src=\"../images/searchIcon.png\"></button></p>";
			mainPanelContent += `<table>`
			for (i = 0; i < issues.length; i+= 2) {
				mainPanelContent += `<tr><td>${issues[i]}:</td>`;
				mainPanelContent += `<td><input type="range" min=0 max=100 value=50 class="slider" /></td>`;
				if (i + 1 < issues.length) {
					mainPanelContent += `<td>${issues[i + 1]}:</td>`;
					mainPanelContent += `<td><input type="range" min=0 max=100 value=50 class="slider" /></td></tr>`;
				}
			}
		} else {
			mainPanelContent += "<p>We could not find any governments " +
					"that matched your search.</p>";
		}
	}
	document.getElementById("mainPanel").innerHTML = mainPanelContent;
}