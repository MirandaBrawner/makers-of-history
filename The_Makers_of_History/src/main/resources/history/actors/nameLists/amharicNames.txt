774
#gender-neutral given names
Alemayehu
Berhane
Desta
Lishan

#feminine given names
Abeba
Aberash
Aster
Christina
Eden
Ejigayehu
Eleni
Etenesh
Gelila
Hanna
Haregewoin
Helen
Hiwot
Kidist
Konjit
Lidya
Liyay
Lulit
Mariam
Mariamawit
Martha
Meklit
Menen
Meron
Naomie
Ruth
Seble
Sehin
Werknesh
Yetnebersh
Yewubdar

#masculine given names
Abel
Abraham
Amda Seyon
Ammanuel
Andualem
Asfaw
Asnaf Seged
Asrat
Baalu
Baeda Maryam
Bakaffa
Belay
Berhaneyesus
Berhanu
Dawit
Dejen
Eskender
Fasilides
Gabreal
Gebre
Gelawdewos
Getatchew
Giyorgis
Gorgoryos
Haddis
Haile
Haile Selassie
Heruy
Hussein
Iyasu
Makonnen
Markos
Menas
Menelik
Mikael
Mohammed
Na'od
Nahom
Newaya
Petros
Sarsa Dengel
Seifu
Sisay
Susenyos
Tadesse
Tafari
Tamrat
Tariku
Tesfaye
Tewodros
Workneh
Yaqob
Yekuno
Yeshaq
Yohannes
Yonas
Yonathan
Zara Yaqob

#family names
[FATHER]

#patronyms
[FATHER]

##
Sources: "List of most popular given names". Wikipedia.
https://en.wikipedia.org/wiki/List_of_most_popular_given_names
"Amhara people". Wikipedia.
https://en.wikipedia.org/wiki/Amhara_people
"Amharic Names". Behind the Name. 
https://www.behindthename.com/names/usage/amharic