2117
#gender-neutral given names
Amadou
Oumarou

#feminine given names
Aaminata
Aisha
Aïssata
Amina
Coumba
Djaili
Eloïne
Falala
Fatoumata
Gbemisola
Haddy
Hindou
Isatou
Kadiatou
Khadidiatou
Khady
Madina
Mariama
Nana
Rabiatou
Ruqayyah
Samira
Serah

#masculine given names
Abdullahi
Abubakar
Adama
Alhaji
Alioune
Aliyu
Atiku
Bala
Bello
Bokar
Bukola
Ibrahim
Kabir
Karamokho
Khadim
Lamido
Mallam
Moctar
Modibo
Muhammad
Muhammadu
Murtala
Musa
Nuhu
Rabiu
Rilwanu
Sa'adu
Samba
Sanusi
Shehu
Siddiq
Sule
Tidiane
Tijjani
Usman
Yezo

#family names
Abubakar
Alfa
Amal
Atiku
Baldé
Barkindo
Barqué
Bawumia
Bayero
Bello
Biro
Buhari
Cissé
Dabo
Dan Fodio
Dia
Diallo
Dikko
Djaló
Embaló
Gambari
Ibrahim
Jalloh
Kandeh
Kane
Ly-Tall
Muhammad
Muhammad-Bande
Nagogo
Njie-Saidy
Ribadu
Sall
Sallah
Saraki
Shagari
Sissoco
Sori
Sow
Tall
Tambajang
Umar
Usman
Yar'Adua

#patronyms
[FATHER]
Dan [FATHER]

##
Sources: 
"List of Fula people". Wikipedia.
https://en.wikipedia.org/wiki/List_of_Fula_people
"Fula Names". Behind the Name.
https://www.behindthename.com/names/usage/fula