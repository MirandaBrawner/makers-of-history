[0]ID;[1]Plural Noun;[2]Singular Noun;[3]Plural Adjective;[4]Singular Adjective;[5]Gender number (0M <-> 1F)[6]Assigned [7]Apparent [8]Parent Groups
2;Women;Woman;Women;Woman;1;-1;-1;
3;Non-Binary People;Non-Binary Person;Non-Binary;;0.5;-1;-1;5
4;Men;Man;Men;Man;0;-1;-1;
5;Transgender People;Transgender Person;Transgender;;-1;-2;-2;
6;Trans Women;Trans Woman;Trans Women;Trans Woman;1;0;-1;2,5
7;Trans Men;Trans Man;Trans Men;Trans Man;0;1;-1;4,5
8;Cisgender People;Cisgender Person;Cisgender;;-1;-1;-1;
9;Cis Women;Cis Woman;Cis Women;Cis Woman;1;1;-1;2,8
10;Cis Men;Cis Man;Cis Men;Cis Man;0;0;-1;4,8