package models.ideas;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.eclipse.collections.api.list.MutableList;

import models.actors.Actor;
import systemState.Game;

public class Policy implements Comparable<Policy> {
	
	public Game game;
	private long id;
	private String name;
	private double defaultValue;
	private List<String> labels;
	private List<Double> thresholds;
	private MutableList<String> categories;
	private TreeMap<Actor, Double> currentValues;
	
	public Policy(Game game, long id, String name, List<String> labels, MutableList<String> policyCategories) {
		this(game, id, name, labels, policyCategories, 0.5);
	}
	
	public Policy(Game game, long id, String name, List<String> labels, MutableList<String> categories, 
			double defaultValue) {
		this.game = game;
		this.id = id;
		this.name = name;
		this.defaultValue = defaultValue;
		this.labels = labels;
		thresholds = new ArrayList<>();
		this.categories = categories;
		if (labels.size() >= 2) {
			double interval = 1.0 / labels.size();
			for (int step = 1; step < labels.size(); step++) {
				thresholds.add(interval * step);
			}
		}
		currentValues = new TreeMap<>();
	}
	
	public Policy(Game game, long id, String name, List<String> labels, List<Double> thresholds, 
			MutableList<String> categories, double defaultValue) {
		this.game = game;
		this.id = id;
		this.name = name;
		this.defaultValue = defaultValue;
		this.labels = labels;
		this.thresholds = thresholds;
		this.categories = categories;
		currentValues = new TreeMap<>();
	}
	
	public String valueToLabel(double value) {
		for (int i = 0; i < thresholds.size(); i++) {
			if (value < thresholds.get(i)) {
				return labels.get(i);
			}
		}
		return labels.get(labels.size() - 1);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(double defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<Double> getThresholds() {
		return thresholds;
	}

	public void setThresholds(List<Double> thresholds) {
		this.thresholds = thresholds;
	}

	public MutableList<String> getCategories() {
		return categories;
	}

	public void setCategories(MutableList<String> categories) {
		this.categories = categories;
	}

	public TreeMap<Actor, Double> getCurrentValues() {
		return currentValues;
	}

	public void setCurrentValues(TreeMap<Actor, Double> currentValues) {
		this.currentValues = currentValues;
	}
	
	@Override 
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof Policy)) return false;
		Policy secondPolicy = (Policy) obj;
		return compareTo(secondPolicy) == 0;
	}

	@Override
	public int compareTo(Policy secondPolicy) {
		if (secondPolicy == null) return 1;
		return name.compareTo(secondPolicy.name);
	}
	
	
}
