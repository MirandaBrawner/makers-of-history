package models.ideas;

import org.eclipse.collections.api.list.ListIterable;
import org.eclipse.collections.api.list.MutableList;

import systemState.Game;

public class Decision implements Comparable<Decision> {
	
	private Game game;
	private String internalName;
	private String title;
	private String text;
	private ListIterable<String> picturePaths;
	private ListIterable<String> choices;
	private MutableList<String> inputStrings;
	private ListIterable<?> participants;
	private int defaultChoiceIndex;
	private double numberOfDays;
	private boolean alwaysPause;
	
	/** Note: Picture paths should be relative to the resources/images folder. For example, 
	 the picture located at src/main/resources/images/portraits/douglass.jpg should be 
	 written as portraits/douglass.jpg when constructing an instance of this class. */
	public Decision(Game game, String internalName, String title, String text, ListIterable<String> picturePaths,
			ListIterable<String> choices, MutableList<String> inputStrings, ListIterable<?> participants, 
			int defaultChoiceIndex, double numberOfDays, boolean alwaysPause) {
		this.game = game;
		this.internalName = internalName;
		this.title = title;
		this.text = text;
		this.picturePaths = picturePaths;
		this.choices = choices;
		this.inputStrings = inputStrings;
		this.participants = participants;
		this.defaultChoiceIndex = defaultChoiceIndex;
		this.numberOfDays = numberOfDays;
		this.alwaysPause = alwaysPause;
	}
	
	public Game getGame() {
		return game;
	}
	public String getInternalName() {
		return internalName;
	}
	public String getTitle() {
		return title;
	}
	public String getText() {
		return text;
	}
	public ListIterable<String> getPicturePaths() {
		return picturePaths;
	}
	public ListIterable<String> getChoices() {
		return choices;
	}
	public MutableList<String> getInputStrings() {
		return inputStrings;
	}
	public ListIterable<?> getParticipants() {
		return participants;
	}
	public int getDefaultChoiceIndex() {
		return defaultChoiceIndex;
	}
	@Override
	public int compareTo(Decision otherDecision) {
		return internalName.compareTo(otherDecision.internalName);
	}
	@Override
	public String toString() {
		return title;
	}
	public double getNumberOfDays() {
		return numberOfDays;
	}
	public boolean getAlwaysPause() {
		return alwaysPause;
	}
	
	
}