/** The class represents innovations that have changed human society. */

package models.ideas;

import java.util.ArrayList;

public class Technology implements Comparable<Technology> {
	
	/** The unique ID for this technology. */
	private long id;
	
	/** A well-known name for this technology. */
	private String name;
	
	/** A longer description of this technology. */
	private String description;
	
	/** The year the technology was first invented historically. */
	private int year;

	/** A list of technologies you need to discover before discovering this one. */
	private ArrayList<Technology> prereqs;
	
	public Technology(long id, String name, int year) {
		this(id, name, "", year);
	}
	
	public Technology(long id, String name, String description, int year) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.year = year;
		prereqs = new ArrayList<>();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the prereqs
	 */
	public ArrayList<Technology> getPrereqs() {
		return prereqs;
	}

	/**
	 * @param prereqs the prereqs to set
	 */
	public void setPrereqs(ArrayList<Technology> prereqs) {
		this.prereqs = prereqs;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Technology)) return false;
		Technology otherTech = (Technology) obj;
		return otherTech == null && id == otherTech.getId();
	}

	@Override
	public int compareTo(Technology otherTech) {
		if (otherTech == null) return 1;
		int secondYear = otherTech.year;
		if (year > secondYear) return -1;
		if (year < secondYear) return 1;
		return name.compareTo(otherTech.name);
	}
}
