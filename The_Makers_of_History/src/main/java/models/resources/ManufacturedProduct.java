/** This class represents non-edible commodities that require some sort of processing to create. */

package models.resources;

import java.util.TreeMap;

import models.ideas.Technology;

public class ManufacturedProduct extends Resource {
    
    /** A table of technologies that are either necessary or highly 
     beneficial in the manufacture of this resource. Each relevant 
     technology is assigned a number representing its importance to 
     this resource, ranging from 0 (not important at all) to 1 
     (absolutely necessary). */
    private TreeMap<Technology, Double> prerequisites;
	
}
