/** This class represents all usable raw materials, commodities, and tokens of exchange. */
package models.resources;

import java.util.ArrayList;

public class Resource implements Comparable<Resource> {

    /** The name of this resource. */
    private String name;
    
	private String category;
	

    /** A list of the names of resources needed to produce this resource.*/
    private ArrayList<String> ingredients;

    /**
     * Returns true if two objects are instances of the same kind of resource.
     * @param other The other resource being compared with this one.
     * @return true if this and the other resource are of the same type. */
    public boolean equals(Resource other) {
        return name.equals(other.name);
    }

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the ingredients
	 */
	public ArrayList<String> getIngredients() {
		return ingredients;
	}

	/**
	 * @param ingredients the ingredients to set
	 */
	public void setIngredients(ArrayList<String> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int compareTo(Resource otherRes) {
		if (otherRes == null) return 1;
		return name.compareTo(otherRes.name);
	}

}
