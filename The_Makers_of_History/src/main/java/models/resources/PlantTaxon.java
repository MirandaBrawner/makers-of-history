/** This class represents a taxon, or grouping, of wild or domesticated plants. 
 It can be broad or narrow. */

package models.resources;

import java.util.ArrayList;

public class PlantTaxon extends Resource {
	
	/** A list of any broader, more general groups of plants that this 
	 group of animals is a part of. */
	public ArrayList<PlantTaxon> supertaxa;
	
	/** A list of any specific varieties of this group of plants. */
	public ArrayList<PlantTaxon> subtaxa;
	
}
