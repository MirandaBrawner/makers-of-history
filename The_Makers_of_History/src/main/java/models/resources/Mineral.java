/** This class represents raw natural resources that do not come from living or recently 
 deceased organisms. Instead, they come from either long-dead organisms (e.g. coal), or 
 are never-alive minerals (e.g. iron). */

package models.resources;

public class Mineral extends Resource {

}
