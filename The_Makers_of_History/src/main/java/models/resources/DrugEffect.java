/** A list of categories of psychiatric drugs. A drug can fall into several categories. [1] */

package models.resources;

public enum DrugEffect {
	ANESTHETIC,
	ANALGESIC,
	ANTIPSYCHOTIC,
	ANXIOLYTIC,
	DEPRESSANT,
	EMPATHOGEN_ENTACTOGEN,
	HALLUCINOGEN,
	MOOD_STABILIZER,
	STIMULANT
}

/** Sources Used:
[1] "Psychoactive drug." Wikipedia. Retrieved from 
https://en.wikipedia.org/wiki/Psychoactive_drug, 13 November 2019. */