/** This class represents food items that are ready for human consumption, without the need for 
 further processing. This includes raw, cooked, and processed foods. */

package models.resources;

import java.util.TreeMap;

public class FoodResource extends Resource {
	

	/** A list of nutrients that this food contains, and their concentrations. 
	 The numbers in the map represent the fraction of the food (measured by mass)
	 that a particular nutrient makes up. The sum of the fractions should not
	 exceed 1, but it can be less than 1, since some parts of the food item
	 might not have any nutritional value. */
	private TreeMap<Nutrient, Double> nutrients;
	/**
	 * @return the nutrients
	 */
	public TreeMap<Nutrient, Double> getNutrients() {
		return nutrients;
	}

	/**
	 * @param nutrients the nutrients to set
	 */
	public void setNutrients(TreeMap<Nutrient, Double> nutrients) {
		this.nutrients = nutrients;
	}
	
}
