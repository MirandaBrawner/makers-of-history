/** A class to represent machines that travel across land, sea, air, or space. */

package models.resources;

public class Vehicle extends ManufacturedProduct {

	/** Is this vehicle capable of land travel? */
	private boolean landCapable;
	
	/** Is this vehicle capable of traveling across water? */
	private boolean seaCapable;
	
	/** Is this water vehicle a submarine? */
	private boolean underseaCapable;
	
	/** Is this vehicle capable of sustained flight? */
	private boolean airCapable;
	
	/** Is this vehicle capable of sustained space travel? */
	private boolean spaceCapable;
	
	/** How fast the vehicle can travel, in meters per second. */
	private double maxSpeed;
        
        /** The minimum number of people needed to pilot one vehicle. */
        private int minimumCrewSize;
        
        /** The maximum number of passengers this vehicle can carry, 
         not including crew. */
        private int passengerCapacity;
        
	
}
