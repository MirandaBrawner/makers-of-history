/** This class represents all mind-altering substances that are used to relieve suffering,
 experience pleasure, or treat illness. */

package models.resources;

import java.util.TreeMap;

public class Drug extends Resource {

	/** A measure of how easy it is to become dependent on this drug. */
	private int addictiveness;
	
	/** A list of this drug's effects, and the strength of each effect.
	 See DrugEffect.java for a list of categories of effects. */
	private TreeMap<DrugEffect, Integer> effects;
	
}
