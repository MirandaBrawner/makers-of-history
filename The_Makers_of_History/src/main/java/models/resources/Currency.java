/** A class for storing national, international, or regional currencies. */

package models.resources;

import java.util.ArrayList;

import models.actors.Government;

public class Currency {
	
	/** The long form of the currency's name, often including the name of the
	 government(s) issuing it. */
	private String fullName;
	
	/** A short form of the currency's name, used in situation where no 
	 the full name would sound awkward. */
	private String shortName;
	
	/** The worth of one unit of this currency, in terms of the game's generic 
	 world currency. */
	private double value;
	
	/** List of jurisdictions where this currency is legal tender. */
	private ArrayList<Government> governments;

}
