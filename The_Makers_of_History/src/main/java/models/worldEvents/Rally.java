/** A large gathering of people, typically for the purpose of protest. 
 If the rally turns violent, an Attack or ArmedConflict event will be
 created. */

package models.worldEvents;

import java.util.ArrayList;

public class Rally extends WorldEvent {
    
    /** The total number of people participating in the rally. 
     Does not include counter-protesters who are assembled in opposition
     to the rally. Counter-protesters should be accounted for in a 
     separate Rally event, linked to this one by the counterProtests
     field. */
    private long numberOfParticipants;
    
    /** A list of rallies that broke out in opposition to this one. */
    private ArrayList<Rally> counterProtests;
    
}
