/** This class represents any occasions where a group of people elects an individual or a smaller 
 group of people to hold a position. */

package models.worldEvents;

import java.util.ArrayList;

import models.actors.Office;
import models.actors.Person;

public class Election extends WorldEvent {

	/** A list of all offices that are up for election. */
	public ArrayList<Office> offices;
	
	/** A list of all candidates who won the election. */
	public ArrayList<Person> winners;
	
	/** A list of all candidates who lost the election. */
	public ArrayList<Person> losers;
}
