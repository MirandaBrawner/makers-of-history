/** A class to represent notable historical events. */

package models.worldEvents;

import java.util.GregorianCalendar;

public class WorldEvent {
	
	/** The date that this event started, stored as a Gregorian date. 
	In game, this date will be converted to the appropriate local calendar. */
	private GregorianCalendar startDate;
	
	/** The date that this event ended, stored as a Gregorian date. 
	 In game, this date will be converted to the appropriate local calendar. */
	private GregorianCalendar endDate; 

}
