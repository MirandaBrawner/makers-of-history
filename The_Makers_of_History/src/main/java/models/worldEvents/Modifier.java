/** An ongoing effect that impacts the game world in some way. */

package models.worldEvents;

public class Modifier {
    
    /** Call this method to turn on the modifier. */
    public void begin() {
        
    }
    
    /** Call this method to turn off the modifier. */
    public void end() {
        
    }
    
}
