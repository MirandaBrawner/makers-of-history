/** A class to represent any armed confrontation, that involves some
 * use of force by multiple conflicting parties. Acts of violence that
 * are one-sided (assassinations, massacres) should use the Attack class. */
package models.worldEvents;

import java.util.ArrayList;

import models.actors.Alliance;

public class ArmedConflict extends WorldEvent {

    /** The total number of people who died as a result of the conflict. */
    private long numberOfDeaths;

    /** The total number of people severely wounded by the conflict. */
    private long numberGravelyInjured;

    /** A list of the opposing coalitions of governments and factions. */
    private ArrayList<Alliance> alliances;

}
