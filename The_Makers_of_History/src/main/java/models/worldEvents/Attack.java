/** A class to represent one-sided acts of violence, where the guilt 
 falls entirely on one party. If multiple opposing parties act violently,
 use the ArmedConflict class. */

package models.worldEvents;

import java.util.ArrayList;

import models.actors.Person;

public class Attack extends WorldEvent {
	
	/** A list of the people most responsible for the attack, that is,
	 those who played leadership roles in it. */
	private ArrayList<Person> leadAttackers;
	
	/** A list of notable people who participated in the attack. */
	private ArrayList<Person> attackers;
	
	/** The total number of participants in the attack, notable or not. */
	private long numberOfAttackers;
	
	/** A list of notable people who were directly targeted by the attack. */
	private ArrayList<Person> victims;
	
	/** The total number of people who were directly targeted by the attack,
	 notable or not. */
	private long numberOfVictims;
	
	/** The total number of people who died as a result of the attack. */
	private long numberOfDeaths;

}
