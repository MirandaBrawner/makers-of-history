/** This class represents agreements and conferences between governments. */

package models.worldEvents;

import java.util.ArrayList;

import models.actors.Government;

public class Treaty extends WorldEvent {
	
	/** A list of governments participating in this treaty. */
	private ArrayList<Government> signatories;

}
