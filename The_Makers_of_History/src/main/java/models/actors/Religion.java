/** A class to represent religious and spiritual movements and traditions, both organized and decentralized. */

package models.actors;

import java.util.GregorianCalendar;
import java.util.Set;

import systemState.Game;

public class Religion extends PeopleGroup {
    
    public Religion(Game game) {
		super(game);

	}

	/** The name for a religion that is most widely used by its adherents. */
    private String autonym;

    /** A list of specific forms of this religion. */
    private Set<Religion> varieties;

    /** A list of deities who are venerated by this religion. */
    private Set<Deity> deities;

    /** A list of historical figures who played an important positive 
     role in the religion, and are recognized for doing so. */
    private Set<Person> heroes;

    private GregorianCalendar foundingDate;
    
	/**
	 * @return the autonym
	 */
	public String getAutonym() {
		return autonym;
	}

	/**
	 * @param autonym the autonym to set
	 */
	public void setAutonym(String autonym) {
		this.autonym = autonym;
	}

	/**
	 * @return the varieties
	 */
	public Set<Religion> getVarieties() {
		return varieties;
	}

	/**
	 * @param varieties the varieties to set
	 */
	public void setVarieties(Set<Religion> varieties) {
		this.varieties = varieties;
	}

	/**
	 * @return the deities
	 */
	public Set<Deity> getDeities() {
		return deities;
	}

	/**
	 * @param deities the deities to set
	 */
	public void setDeities(Set<Deity> deities) {
		this.deities = deities;
	}

	/**
	 * @return the heroes
	 */
	public Set<Person> getHeroes() {
		return heroes;
	}

	/**
	 * @param heroes the heroes to set
	 */
	public void setHeroes(Set<Person> heroes) {
		this.heroes = heroes;
	}

	public GregorianCalendar getFoundingDate() {
		return foundingDate;
	}

	public void setFoundingDate(GregorianCalendar foundingDate) {
		this.foundingDate = foundingDate;
	}

}
