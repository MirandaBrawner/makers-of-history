/** A class to represent various offices a person might hold, 
 such as political, religious, or military titles. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Office extends Actor {

	public Office(Game game) {
		super(game);
		incumbents = new ArrayList<>();
		pastHolders = new ArrayList<>();
	}

	/** The name of this office.*/
	public String fullName;
	
	/** A shorter name for this office. */
	public String shortName;
	
	/** Feminine form of the long name. */
	public String feminineFullName;
	
	/** Feminine form of the short name. */
	public String feminineShortName;
	
	/** Masculine form of the long name. */
	public String masculineFullName;
	
	/** Masculine form of the short name. */
	public String masculineShortName;
	
	/** A list of the people currently holding this office. */
	public ArrayList<Person> incumbents;
	
	/** The maximum number of people who can hold this office at once. */
	public int maxIncumbents;
	
	/** The minimum number of people who must be holding this office at any time. */
	public int minIncumbents;
	
	/** A list of all known people who held this office in the past. */
	public ArrayList<Person> pastHolders;

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the feminineFullName
	 */
	public String getFeminineFullName() {
		return feminineFullName;
	}

	/**
	 * @param feminineFullName the feminineFullName to set
	 */
	public void setFeminineFullName(String feminineFullName) {
		this.feminineFullName = feminineFullName;
	}

	/**
	 * @return the feminineShortName
	 */
	public String getFeminineShortName() {
		return feminineShortName;
	}

	/**
	 * @param feminineShortName the feminineShortName to set
	 */
	public void setFeminineShortName(String feminineShortName) {
		this.feminineShortName = feminineShortName;
	}

	/**
	 * @return the masculineFullName
	 */
	public String getMasculineFullName() {
		return masculineFullName;
	}

	/**
	 * @param masculineFullName the masculineFullName to set
	 */
	public void setMasculineFullName(String masculineFullName) {
		this.masculineFullName = masculineFullName;
	}

	/**
	 * @return the masculineShortName
	 */
	public String getMasculineShortName() {
		return masculineShortName;
	}

	/**
	 * @param masculineShortName the masculineShortName to set
	 */
	public void setMasculineShortName(String masculineShortName) {
		this.masculineShortName = masculineShortName;
	}

	/**
	 * @return the incumbents
	 */
	public ArrayList<Person> getIncumbents() {
		return incumbents;
	}

	/**
	 * @param incumbents the incumbents to set
	 */
	public void setIncumbents(ArrayList<Person> incumbents) {
		this.incumbents = incumbents;
	}

	/**
	 * @return the maxIncumbents
	 */
	public int getMaxIncumbents() {
		return maxIncumbents;
	}

	/**
	 * @param maxIncumbents the maxIncumbents to set
	 */
	public void setMaxIncumbents(int maxIncumbents) {
		this.maxIncumbents = maxIncumbents;
	}

	/**
	 * @return the minIncumbents
	 */
	public int getMinIncumbents() {
		return minIncumbents;
	}

	/**
	 * @param minIncumbents the minIncumbents to set
	 */
	public void setMinIncumbents(int minIncumbents) {
		this.minIncumbents = minIncumbents;
	}

	/**
	 * @return the pastHolders
	 */
	public ArrayList<Person> getPastHolders() {
		return pastHolders;
	}

	/**
	 * @param pastHolders the pastHolders to set
	 */
	public void setPastHolders(ArrayList<Person> pastHolders) {
		this.pastHolders = pastHolders;
	}
	
}
