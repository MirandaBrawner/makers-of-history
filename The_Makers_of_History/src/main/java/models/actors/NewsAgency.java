/** An agency for sharing news, expressing opinions, and organizing 
 mass movements. */

package models.actors;

import java.util.ArrayList;

public class NewsAgency {

    /** A list of governments who own or control this news agency. 
     Should be left empty of the agency is not state-controlled. */
    private ArrayList<Government> governments;
    
}
