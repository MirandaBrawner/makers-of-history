/** This class represents groups of people with a shared agenda, 
 including formal political parties as well as informal interest groups. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Faction extends PeopleGroup {
	
	public Faction(Game game) {
		super(game);
		leaders = new ArrayList<>();
		notableMembers = new ArrayList<>();
	}

	/** A list of all people generally recognized as leaders of this faction. */
	private ArrayList<Person> leaders;
	
	/** A list of all notable individuals who are part of this organization. */
	private ArrayList<Person> notableMembers;
	
	private String shortName;
	private String fullName;

	/**
	 * @return the leaders
	 */
	public ArrayList<Person> getLeaders() {
		return leaders;
	}

	/**
	 * @param leaders the leaders to set
	 */
	public void setLeaders(ArrayList<Person> leaders) {
		this.leaders = leaders;
	}

	/**
	 * @return the notableMembers
	 */
	public ArrayList<Person> getNotableMembers() {
		return notableMembers;
	}

	/**
	 * @param notableMembers the notableMembers to set
	 */
	public void setNotableMembers(ArrayList<Person> notableMembers) {
		this.notableMembers = notableMembers;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
