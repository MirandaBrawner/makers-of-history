/** A class to represent human languages and dialects. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Language extends Culture {
	
	public Language(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	/** A list of specific forms of this language, often based on region or social class. */
	public ArrayList<Language> varieties;
}
