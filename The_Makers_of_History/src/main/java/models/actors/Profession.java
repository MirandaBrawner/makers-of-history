/** This class represents groups of people who share similar professions,
 such as farmers, miners, factory workers, domestic servants, etc. */

package models.actors;

import java.util.TreeMap;

import models.resources.Resource;
import systemState.Game;

public class Profession extends PeopleGroup {
    
    public Profession(Game game) {
		super(game);
	}

	/** A table of the resources required for this group to perform 
     their job, not including basic life needs like food and water, 
     which are counted separately. The value for each resource is in 
     grams per person per day. */
    private TreeMap<Resource, Double> occupationalNeeds;

    /** A table of the resources produced by people in this profession, 
     measured in grams per person per day. */
    private TreeMap<Resource, Double> output;
}
