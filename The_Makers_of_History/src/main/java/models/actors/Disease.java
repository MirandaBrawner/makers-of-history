/** This class represents all illnesses that effect people or animals. */

package models.actors;

public class Disease {

    /** The most common name for this illness. */
    private String name;
    
    /** A measure of how easily this illness is transmitted from one host
     to another. Illnesses that are not contagious at all should have a 
     value of 0. Higher values indicate greater transferability. */
    private int contagionLevel;
    
    /** A measure of how likely the host is to die on any given day. 
     Can range from 0 (no greater chance of death than without the 
     disease) to 1 (certain death within 24 hours). */
    private double dailyChanceOfDeath;
    
    /** A measure of how likely the host is to recover on any given day. 
     Can range from 0 (no hope of recovering by the next day) to 1 (recovery
     within 24 hours is certain). */
    private double dailyChanceOfRecovery;
}
