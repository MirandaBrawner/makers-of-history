/** A class for storing information about individual people, real or fictional, within the game. */

package models.actors;

import java.util.GregorianCalendar;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import javafx.application.Platform;
import models.LivingStatus;
import models.places.MapLocation;
import systemState.Game;
import systemState.Player;
import view.Launch;

@SuppressWarnings("restriction")
public class Person extends Actor {
	
	/** The name that a person is most commonly referred to by. Optional. */
	private String canonicalName = "";

    /** A longer form of a person's name, which will appear as the title of their profile page
     if they do not have a canonical name. */
	private String fullName = "";

	/** A shorter form of a person's name, often their family name, if they have one, but it can be 
	 a given name or nickname. Will sometimes appear in place of the full name, such as when 
	 referring to the person for the second time in a fragment of text. */
	private String shortName = "";
	
	/** The name given at birth that distinguishes a person from their family. */
	private String givenName = "";

	/** An optional field for a person's family name, which is, by default, passed along from parent 
	  to child. Stored in a generic form like Romanov(a). */
	private String familyName = "";
	
	/** An optional field for a person's family name, which is, by default, passed along from parent 
	  to child. Customized to the person's gender if need be. */
	private String customFamilyName = "";

	/** A collection of variables relating to the person's gender, 
	     sexuality, and fertility. */
	private Gender gender;

	/** A list of cultures the person identifies with. */
	private MutableList<Culture> cultures;

	/** A list of religions that the person identifies with. */
	private MutableList<Religion> religions;

	/** A list of titles this person currently holds. */
	private MutableList<Office> offices;
	
	/** A list of anyone this person is married to. */
	private MutableList<Long> spouses;
	
	/** A list of people who are recognized as this person's parents, by law or consensus. */
	private MutableList<Long> legalParents;

	/** A list of people this person has or had parental custody over. */
	private MutableList<Long> legalChildren;

	/** A list of people who were raised by the same guardians as this person. */
	private MutableList<Long> legalSiblings;

	/** The person's natural parents, if known. */
	private MutableList<Long> naturalParents;

	/** The person's natural children, if known. */
	private MutableList<Long> naturalChildren;

	/** The person's natural siblings (full or half), if known. */
	private MutableList<Long> naturalSiblings;
	
	private MutableList<Long> heirs;

	/** A list of anyone this person is involved with romantically. */
	private MutableList<Long> lovers;

	/** A list of this person's close friends. */
	private MutableList<Long> friends;

	/** A list of anyone this person answers to in a manager-employee or master-slave relationship. */
	private MutableList<Long> bosses;

	/** A list of anyone this person holds (non-parental) authority over, including employees and slaves. */
	private MutableList<Long> subordinates;
	
	/** A list of political parties this person is a member of. */
	private MutableList<Faction> parties;

	/** A measure of how likely this person is to tell the truth, keep their word, and play fairly. 
	 High values indicate honesty, low values indicate deceitfulness. */
	private int honor;

	/** A measure of how much this person treats people of other genders, races, and religions as equals. 
	 High values indicate tolerance and inclusiveness, low values indicate bigotry. */
	private int tolerance;

	/** A measure of how willing this person is to face their fears and sacrifice themselves for others. 
	 High values indicate courage, low values indicate cowardice. */
	private int bravery;

	/** A measure of how readily this person gives away their time and resources. 
	 High values indicate generosity, low values indicate greed and selfishness. */
	private int generosity;

	/** A measure of how skillfully this person bonds with others.
	 High values indicate friendliness and social grace, low values indicate social awkwardness. */
	private int socialSkill;

	/** A measure of how much this person is able to acknowledge their faults and swallow their pride.
	 High values indicate humility, low values indicate arrogance. */
	private int humility;

	/** A measure of how hard this person is willing to work to achieve their goals.
	 Unlike the bravery value, which pertains specifically to dangerous situations, this value
	 pertains more to low risk, but tedious tasks. 
	 High values indicate hard work and patience, low values indicate giving up easily. */
	private int dedication;

	/** An overall measure of the person's various athletic abilities. 
	 High values indicate strength, speed, agility, endurance, and so on, low values indicate a lake of athletic ability. */
	private int athleticism;

	/** A measure of how much the person believes in widespread political participation. 
	 High values indicate a preference for democratic government, low values indicate authoritarianism. */
	private int democracy;

	/** A measure of how willing the person is to use force to achieve their aims. 
	 High values indicate commitment to non-violence, low values indicate a preference for violence. */
	private int pacifism;

	/** A measure of how willing the person is to try new ideas and criticize traditions. 
	 High values indicate an open-minded critical attitude, low values indicate reverence for tradition. */
	private int innovation;
	
	private boolean agingOn;

	/** The place where this person currently is. */
	private MapLocation location;
	
	/** How well-known the person is. Ranges from 0 (only known by a few people) to 1 (world famous). */
	private double fame;
	
	public Person(Game game, String fullName) {
		super(game);
		this.fullName = fullName;
		cultures = Lists.mutable.empty();
		religions = Lists.mutable.empty();
		offices = Lists.mutable.empty();
		spouses = Lists.mutable.empty();
		legalParents = Lists.mutable.empty();
		legalChildren = Lists.mutable.empty();
		legalSiblings = Lists.mutable.empty();
		naturalParents = Lists.mutable.empty();
		naturalChildren = Lists.mutable.empty();
		naturalSiblings = Lists.mutable.empty();
		heirs = Lists.mutable.empty();
		lovers = Lists.mutable.empty();
		friends = Lists.mutable.empty();
		bosses = Lists.mutable.empty();
		subordinates = Lists.mutable.empty();
		parties = Lists.mutable.empty();
		agingOn = true;
		setFame(0);
	}
	
	public double oldAgeDeathChance(double age, double interval) {
		double yearlyChance = 0.001;
		double[] thresholds = {20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120};
		double[] values = {0, 0.001, 0.002, 0.005, 0.011, 0.023, 0.058, 0.164, 0.352, 0.573, 0.9};
		if (age <= thresholds[0]) {
			yearlyChance = values[0];
		} else if (age > thresholds[thresholds.length - 1]) {
			yearlyChance = values[thresholds.length - 1];
		} else {
			for (int i = 1; i < thresholds.length; i++) {
				if (age <= thresholds[i] && age > thresholds[i - 1]) {
					double deltaY = values[i] - values[i - 1];
					double deltaX = thresholds[i] - thresholds[i - 1];
					double slope = deltaY / deltaX;
					double offset = age - thresholds[i - 1];
					yearlyChance = slope * offset + values[i - 1];
				}
			}
		}
		return yearlyChance * interval / 365.2425;
	}
	
	public void getOlder(double interval) {
		if (this.getLivingStatus().equals(LivingStatus.ALIVE)) {
			double motherChance = Math.random();
			if (motherChance < gender.getBaseMaternalFertility() * getMaternalFertility() * interval / 1500) { 
				haveChild(Lists.mutable.empty());
			}
			double fatherChance = Math.random();
			if (fatherChance < gender.getBasePaternalFertility() * getPaternalFertility() * interval / 1500) {
				haveChild(Lists.mutable.empty());
			}
			double deathSeed = Math.random();
			if (deathSeed < oldAgeDeathChance(getAgeYears(), interval)) {
				die();
			}
		}
	}
	
	public void die() {
		int deathYear = this.getGame().getIngameDate().get(GregorianCalendar.YEAR);
		int deathMonth = this.getGame().getIngameDate().get(GregorianCalendar.MONTH);
		int deathDate = this.getGame().getIngameDate().get(GregorianCalendar.DAY_OF_MONTH);
		GregorianCalendar gc = new GregorianCalendar(deathYear, deathMonth, deathDate);
		gc.set(GregorianCalendar.ERA, this.getGame().getIngameDate().get(GregorianCalendar.ERA));
		this.setDateOfDeath(gc);
		this.getGame().getLivingPeople().remove(this);
	}
	
	/** A measure of how capable the person is of having children as the mother, based on their age.
	 The maximum value of 1 does not mean they are certain to have children,
	 just that their age is not a barrier for them. */
	public double getMaternalFertility() {
		double[][] values = {
			{8, 0.005},
			{12, 0.45},
			{20, 1},
			{25, 0.94},
			{30, 0.9},
			{35, 0.81},
			{40, 0.53},
			{45, 0.14},
			{50, 0.01},
			{55, 0.001}
		};
		if (this.getAgeYears() < values[0][0]) {
			return values[0][1];
		}
		for (int row = 0; row < values.length - 1; row++) {
			if (this.getAgeYears() < values[row + 1][0]) {
				double slope = (values[row + 1][1] - values[row][1]) / (values[row + 1][0] - values[row][0]);
				double ageDifference = this.getAgeYears() - values[row][0];
				return values[row][1] + (ageDifference * slope);
			}
		}
		return values[values.length - 1][1];
	}
	
	/** A measure of how capable the person is of having children as the father, based on their age.
	 The maximum value of 1 does not mean they are certain to have children,
	 just that their age is not a barrier for them. */
	public double getPaternalFertility() {
		double[][] values = {
			{8, 0.005},
			{12, 0.35},
			{20, 0.95},
			{25, 1},
			{30, 0.9},
			{50, 0.6},
			{70, 0.3},
			{100, 0.04}
		};
		if (this.getAgeYears() < values[0][0]) {
			return values[0][1];
		}
		for (int row = 0; row < values.length - 1; row++) {
			if (this.getAgeYears() < values[row + 1][0]) {
				double slope = (values[row + 1][1] - values[row][1]) / (values[row + 1][0] - values[row][0]);
				double ageDifference = this.getAgeYears() - values[row][0];
				return values[row][1] + (ageDifference * slope);
			}
		}
		return values[values.length - 1][1];
	}
	
	
	public void haveChild(MutableList<Person> otherParents) {
		Person child = new Person(this.getGame(), fullName);
		child.setId(this.getGame().nextActorId());
		child.setGender(new Gender(this.getGame()));
		MutableList<Person> parentList = Lists.mutable.ofAll(otherParents);
		parentList.add(this);
		child.setCultures(parentList.flatCollect(parent -> parent.getCultures()));
		child.setReligions(parentList.flatCollect(parent -> parent.getReligions()));
		parentList.forEach(par -> {
			child.heirs.add(0, par.getId());
			for (long siblingId: par.naturalChildren) {
				if (!child.naturalSiblings.contains(siblingId)) {
					child.naturalSiblings.add(siblingId);
					if (!child.heirs.contains(siblingId)) {
						child.heirs.add(siblingId);
					}
					Person sibling = (Person) getGame().lookupActorByID(siblingId);
					if (sibling != null && !sibling.heirs.contains(child.getId())) {
						sibling.heirs.add(child.getId());
					}
				}
			}
			for (long siblingId: par.legalChildren) {
				if (!child.legalSiblings.contains(siblingId)) {
					child.legalSiblings.add(siblingId);
					if (!child.heirs.contains(siblingId)) {
						child.heirs.add(siblingId);
					}
					Person sibling = (Person) getGame().lookupActorByID(siblingId);
					if (sibling != null && !sibling.heirs.contains(child.getId())) {
						sibling.heirs.add(child.getId());
					}
				}
			}
			child.legalParents.add(par.getId());
			child.naturalParents.add(par.getId());
			par.legalChildren.add(child.getId());
			par.naturalChildren.add(child.getId());
			par.heirs.add(child.getId());
;		});
		this.getGame().getNameHandler().nameChild(parentList, child);
		child.setDateOfBirth(this.getGame().getIngameDate());
		child.setDateOfDeath(null);
		//child.cultures.addAll(cultures);
		//child.religions.addAll(religions);
		if (this.getGame().hasConsoles()) {
			Platform.runLater(() -> {
				this.getGame().getIndividuals().add(child);
				this.getGame().getLivingPeople().add(child);
				this.getGame().getActors().add(child);
			}); 
		} else {
			this.getGame().getIndividuals().add(child);
			this.getGame().getLivingPeople().add(child);
			this.getGame().getActors().add(child);
		}
		for (Person parent: parentList) {
			Player parentPlayer = this.getGame().getPlayerByCharacter(parent);
			if (parentPlayer != null) {
				Launch console = this.getGame().getPlayerConsoles().get(parentPlayer);
				console.tempChildList.add(child);
			}
		}
	}
	
	public boolean isCloseTo(Person otherPerson) {
		return legalChildren.contains(otherPerson.getId()) || legalParents.contains(otherPerson.getId()) ||
				legalSiblings.contains(otherPerson.getId()) || spouses.contains(otherPerson.getId()) ||
				lovers.contains(otherPerson.getId()) || friends.contains(otherPerson.getId());
	}
	
	public String shortDisplayName(Person viewer) {
		if (viewer == null) return shortName;
		return isCloseTo(viewer) ? givenName : shortName;
	}
	
	public String getCanonicalName() {
		return canonicalName;
	}

	public void setCanonicalName(String canonicalName) {
		this.canonicalName = canonicalName;
	}

	/**
     * A longer form of a person's name, which will appear as the title of their profile page.
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * A longer form of a person's name, which will appear as the title of their profile page.
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * A shorter form of a person's name, often their family name, if they have one, but it can be
     * a given name or nickname. Will sometimes appear in place of the full name, such as when
     * referring to the person for the second time in a fragment of text.
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * A shorter form of a person's name, often their family name, if they have one, but it can be
     * a given name or nickname. Will sometimes appear in place of the full name, such as when
     * referring to the person for the second time in a fragment of text.
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getGivenName() {
		return givenName;
	}

    /** The name given at birth that distinguishes a person from their family.
     * Note: Changing this given name will also update the full name. */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
		if (!customFamilyName.isEmpty()) {
			Culture namingCulture = null;
			if (cultures != null && !cultures.isEmpty()) {
				namingCulture = cultures.get((int)(Math.random() * cultures.size()));
			}
			fullName = this.getGame().getNameHandler().getOrderedNameByCulture(
					namingCulture, givenName, customFamilyName, "");
		}
	}

	/**
     * An optional field for a person's family name, which is, by default, passed along from parent
     * to child.
     * @return the familyName
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * An optional field for a person's family name, which is, by default, passed along from parent
     * to child.
     * @param familyName the familyName to set
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCustomFamilyName() {
		return customFamilyName;
	}

	public void setCustomFamilyName(String customFamilyName) {
		this.customFamilyName = customFamilyName;
	}

	/**
     * A collection of variables relating to the person's gender,
     * sexuality, and fertility.
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * A collection of variables relating to the person's gender,
     * sexuality, and fertility.
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * A list of cultures the person identifies with.
     * @return the cultures
     */
    public MutableList<Culture> getCultures() {
        return cultures;
    }

    /**
     * A list of cultures the person identifies with.
     * @param cultures the cultures to set
     */
    public void setCultures(MutableList<Culture> cultures) {
        this.cultures = cultures;
    }

    /**
     * A list of religions that the person identifies with.
     * @return the religions
     */
    public MutableList<Religion> getReligions() {
        return religions;
    }

    /**
     * A list of religions that the person identifies with.
     * @param religions the religions to set
     */
    public void setReligions(MutableList<Religion> religions) {
        this.religions = religions;
    }

    /**
	 * @return the offices
	 */
	public MutableList<Office> getOffices() {
		return offices;
	}

	/**
	 * @param offices the offices to set
	 */
	public void setOffices(MutableList<Office> offices) {
		this.offices = offices;
	}

	/**
     * A list of anyone this person is married to.
     * @return the spouses
     */
    public MutableList<Long> getSpouses() {
        return spouses;
    }

    /**
     * A list of anyone this person is married to.
     * @param spouses the spouses to set
     */
    public void setSpouses(MutableList<Long> spouses) {
        this.spouses = spouses;
    }

    /**
     * A list of people who are recognized as this person's parents, by law or consensus.
     * @return the legalParents
     */
    public MutableList<Long> getLegalParents() {
        return legalParents;
    }

    /**
     * A list of people who are recognized as this person's parents, by law or consensus.
     * @param legalParents the legalParents to set
     */
    public void setLegalParents(MutableList<Long> legalParents) {
        this.legalParents = legalParents;
    }

    /**
     * A list of people this person has or had parental custody over.
     * @return the legalChildren
     */
    public MutableList<Long> getLegalChildren() {
        return legalChildren;
    }

    /**
     * A list of people this person has or had parental custody over.
     * @param legalChildren the legalChildren to set
     */
    public void setLegalChildren(MutableList<Long> legalChildren) {
        this.legalChildren = legalChildren;
    }

    /**
     * A list of people who were raised by the same guardians as this person.
     * @return the legalSiblings
     */
    public MutableList<Long> getLegalSiblings() {
        return legalSiblings;
    }

    /**
     * A list of people who were raised by the same guardians as this person.
     * @param legalSiblings the legalSiblings to set
     */
    public void setLegalSiblings(MutableList<Long> legalSiblings) {
        this.legalSiblings = legalSiblings;
    }

    /**
     * The person's natural parents, if known.
     * @return the naturalParents
     */
    public MutableList<Long> getNaturalParents() {
        return naturalParents;
    }

    /**
     * The person's natural parents, if known.
     * @param naturalParents the naturalParents to set
     */
    public void setNaturalParents(MutableList<Long> naturalParents) {
        this.naturalParents = naturalParents;
    }

    /**
     * The person's natural children, if known.
     * @return the naturalChildren
     */
    public MutableList<Long> getNaturalChildren() {
        return naturalChildren;
    }

    /**
     * The person's natural children, if known.
     * @param naturalChildren the naturalChildren to set
     */
    public void setNaturalChildren(MutableList<Long> naturalChildren) {
        this.naturalChildren = naturalChildren;
    }

    /**
     * The person's natural siblings (full or half), if known.
     * @return the naturalSiblings
     */
    public MutableList<Long> getNaturalSiblings() {
        return naturalSiblings;
    }

    /**
     * The person's natural siblings (full or half), if known.
     * @param naturalSiblings the naturalSiblings to set
     */
    public void setNaturalSiblings(MutableList<Long> naturalSiblings) {
        this.naturalSiblings = naturalSiblings;
    }

    public MutableList<Long> getHeirs() {
		return heirs;
	}

	public void setHeirs(MutableList<Long> heirs) {
		this.heirs = heirs;
	}

	/**
     * A list of anyone this person is involved with romantically.
     * @return the lovers
     */
    public MutableList<Long> getLovers() {
        return lovers;
    }

    /**
     * A list of anyone this person is involved with romantically.
     * @param lovers the lovers to set
     */
    public void setLovers(MutableList<Long> lovers) {
        this.lovers = lovers;
    }

    /**
     * A list of this person's close friends.
     * @return the friends
     */
    public MutableList<Long> getFriends() {
        return friends;
    }

    /**
     * A list of this person's close friends.
     * @param friends the friends to set
     */
    public void setFriends(MutableList<Long> friends) {
        this.friends = friends;
    }

    /**
     * A list of anyone this person answers to in a manager-employee or master-slave relationship.
     * @return the bosses
     */
    public MutableList<Long> getBosses() {
        return bosses;
    }

    /**
     * A list of anyone this person answers to in a manager-employee or master-slave relationship.
     * @param bosses the bosses to set
     */
    public void setBosses(MutableList<Long> bosses) {
        this.bosses = bosses;
    }

    /**
     * A list of anyone this person holds (non-parental) authority over, including employees and slaves.
     * @return the subordinates
     */
    public MutableList<Long> getSubordinates() {
        return subordinates;
    }

    /**
     * A list of anyone this person holds (non-parental) authority over, including employees and slaves.
     * @param subordinates the subordinates to set
     */
    public void setSubordinates(MutableList<Long> subordinates) {
        this.subordinates = subordinates;
    }

    /**
	 * @return the parties
	 */
	public MutableList<Faction> getParties() {
		return parties;
	}

	/**
	 * @param parties the parties to set
	 */
	public void setParties(MutableList<Faction> parties) {
		this.parties = parties;
	}

	/**
     * A measure of how likely this person is to tell the truth, keep their word, and play fairly.
     * High values indicate honesty, low values indicate deceitfulness.
     * @return the honor
     */
    public int getHonor() {
        return honor;
    }

    /**
     * A measure of how likely this person is to tell the truth, keep their word, and play fairly.
     * High values indicate honesty, low values indicate deceitfulness.
     * @param honor the honor to set
     */
    public void setHonor(int honor) {
        this.honor = honor;
    }

    /**
     * A measure of how much this person treats people of other genders, races, and religions as equals.
     * High values indicate tolerance and inclusiveness, low values indicate bigotry.
     * @return the tolerance
     */
    public int getTolerance() {
        return tolerance;
    }

    /**
     * A measure of how much this person treats people of other genders, races, and religions as equals.
     * High values indicate tolerance and inclusiveness, low values indicate bigotry.
     * @param tolerance the tolerance to set
     */
    public void setTolerance(int tolerance) {
        this.tolerance = tolerance;
    }

    /**
     * A measure of how willing this person is to face their fears and sacrifice themselves for others.
     * High values indicate courage, low values indicate cowardice.
     * @return the bravery
     */
    public int getBravery() {
        return bravery;
    }

    /**
     * A measure of how willing this person is to face their fears and sacrifice themselves for others.
     * High values indicate courage, low values indicate cowardice.
     * @param bravery the bravery to set
     */
    public void setBravery(int bravery) {
        this.bravery = bravery;
    }

    /**
     * A measure of how readily this person gives away their time and resources.
     * High values indicate generosity, low values indicate greed and selfishness.
     * @return the generosity
     */
    public int getGenerosity() {
        return generosity;
    }

    /**
     * A measure of how readily this person gives away their time and resources.
     * High values indicate generosity, low values indicate greed and selfishness.
     * @param generosity the generosity to set
     */
    public void setGenerosity(int generosity) {
        this.generosity = generosity;
    }

    /**
     * A measure of how skillfully this person bonds with others.
     * High values indicate friendliness and social grace, low values indicate social awkwardness.
     * @return the socialSkill
     */
    public int getSocialSkill() {
        return socialSkill;
    }

    /**
     * A measure of how skillfully this person bonds with others.
     * High values indicate friendliness and social grace, low values indicate social awkwardness.
     * @param socialSkill the socialSkill to set
     */
    public void setSocialSkill(int socialSkill) {
        this.socialSkill = socialSkill;
    }

    /**
     * A measure of how much this person is able to acknowledge their faults and swallow their pride.
     * High values indicate humility, low values indicate arrogance.
     * @return the humility
     */
    public int getHumility() {
        return humility;
    }

    /**
     * A measure of how much this person is able to acknowledge their faults and swallow their pride.
     * High values indicate humility, low values indicate arrogance.
     * @param humility the humility to set
     */
    public void setHumility(int humility) {
        this.humility = humility;
    }

    /**
     * A measure of how hard this person is willing to work to achieve their goals.
     * Unlike the bravery value, which pertains specifically to dangerous situations, this value
     * pertains more to low risk, but tedious tasks.
     * High values indicate hard work and patience, low values indicate giving up easily.
     * @return the dedication
     */
    public int getDedication() {
        return dedication;
    }

    /**
     * A measure of how hard this person is willing to work to achieve their goals.
     * Unlike the bravery value, which pertains specifically to dangerous situations, this value
     * pertains more to low risk, but tedious tasks.
     * High values indicate hard work and patience, low values indicate giving up easily.
     * @param dedication the dedication to set
     */
    public void setDedication(int dedication) {
        this.dedication = dedication;
    }

    /**
     * An overall measure of the person's various athletic abilities.
     * High values indicate strength, speed, agility, endurance, and so on, low values indicate a lake of athletic ability.
     * @return the athleticism
     */
    public int getAthleticism() {
        return athleticism;
    }

    /**
     * An overall measure of the person's various athletic abilities.
     * High values indicate strength, speed, agility, endurance, and so on, low values indicate a lake of athletic ability.
     * @param athleticism the athleticism to set
     */
    public void setAthleticism(int athleticism) {
        this.athleticism = athleticism;
    }

    /**
     * A measure of how much the person believes in widespread political participation.
     * High values indicate a preference for democratic government, low values indicate authoritarianism.
     * @return the democracy
     */
    public int getDemocracy() {
        return democracy;
    }

    /**
     * A measure of how much the person believes in widespread political participation.
     * High values indicate a preference for democratic government, low values indicate authoritarianism.
     * @param democracy the democracy to set
     */
    public void setDemocracy(int democracy) {
        this.democracy = democracy;
    }

    /**
     * A measure of how willing the person is to use force to achieve their aims.
     * High values indicate commitment to non-violence, low values indicate a preference for violence.
     * @return the pacifism
     */
    public int getPacifism() {
        return pacifism;
    }

    /**
     * A measure of how willing the person is to use force to achieve their aims.
     * High values indicate commitment to non-violence, low values indicate a preference for violence.
     * @param pacifism the pacifism to set
     */
    public void setPacifism(int pacifism) {
        this.pacifism = pacifism;
    }

    /**
     * A measure of how willing the person is to try new ideas and criticize traditions.
     * High values indicate an open-minded critical attitude, low values indicate reverence for tradition.
     * @return the innovation
     */
    public int getInnovation() {
        return innovation;
    }

    /**
     * A measure of how willing the person is to try new ideas and criticize traditions.
     * High values indicate an open-minded critical attitude, low values indicate reverence for tradition.
     * @param innovation the innovation to set
     */
    public void setInnovation(int innovation) {
        this.innovation = innovation;
    }

    /**
	 * @return the agingOn
	 */
	public boolean isAgingOn() {
		return agingOn;
	}

	/**
	 * @param agingOn the agingOn to set
	 */
	public void setAgingOn(boolean agingOn) {
		this.agingOn = agingOn;
	}

	/**
     * The place where this person currently is.
     * @return the location
     */
    public MapLocation getLocation() {
        return location;
    }

    /**
     * The place where this person currently is.
     * @param location the location to set
     */
    public void setLocation(MapLocation location) {
        this.location = location;
    }
    
    public double getFame() {
		return fame;
	}

	public void setFame(double fame) {
		this.fame = fame;
	}

	@Override
    public boolean equals(Object other) {
    	if (other == null) return false;
    	if (!(other instanceof Person)) return false;
    	Person otherPerson = (Person) other;
    	return this.getId() == otherPerson.getId();
    }
	
}
