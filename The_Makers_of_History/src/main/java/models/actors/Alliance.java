/** A coalition of various governments and factions, fighting on the same 
 side in a conflict. */

package models.actors;

import java.util.ArrayList;

public class Alliance {
    
    /** A list of governments participating in this alliance. */
    private ArrayList<Government> governments;
    
    /** A list of non-governmental organizations who are part of this
     alliance. */
    private ArrayList<NGO> organizations;

}
