package models.actors;

import java.util.ArrayList;

public interface Taxon {
	
	ArrayList<Taxon> getSuperTaxa();
	
}
