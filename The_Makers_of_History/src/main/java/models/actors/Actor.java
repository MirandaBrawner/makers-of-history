/** A generic actor, such as a person, animal, or group. */

package models.actors;

import java.util.GregorianCalendar;
import java.util.TreeMap;

import org.eclipse.collections.api.factory.Maps;
import org.eclipse.collections.api.map.MutableMap;

import models.LivingStatus;
import models.resources.Resource;
import systemState.Game;
import systemState.Player;
import systemState.Server;
import systemState.Utilities;

public class Actor implements Comparable<Actor> {

	public static void main(String[] args) {
		Server server = new Server();
		Game game = new Game(server, "12345", new Player(server, "12345", "fake email"));
		game.setIngameDate(new GregorianCalendar());
		for (int year = 1910; year < 2020; year++) {
			Person person = new Person(game, Utilities.generatePassword(16));
			GregorianCalendar gc = new GregorianCalendar(year, 1, 1);
			person.setDateOfBirth(gc);
			double motherhood = person.getMaternalFertility();
			double fatherhood = person.getPaternalFertility();
			String fertilityString = String.format("Paternal Fertility: %.3f", fatherhood);
			if (year % 2 == 0) {
				fertilityString = String.format("Maternal Fertility: %.3f", motherhood);
			}
			System.out.printf("%s. Born %d. %s\n", person.getFullName(), year, fertilityString);
		}
	}

	/** The game that this actor is part of. */
	private Game game;

	/** A unique number to identify this actor. */
	private long id;

	/** The commonly accepted name for the actor. */
	private String name;

	/** The absolute bare necessities this actor needs to survive. */
	private TreeMap<Resource, Double> survivalNeeds;

	/**
	 * What the actor needs to be seen as a productive member of their society.
	 */
	private TreeMap<Resource, Double> socialNeeds;

	/** What the actor needs to feel personally satisfied with their life. */
	private TreeMap<Resource, Double> contentmentNeeds;

	/**
	 * What the actor needs to be seen as a high-status member of their society.
	 */
	private TreeMap<Resource, Double> prestigeNeeds;

	/** The time that the actor was born or created. */
	private GregorianCalendar dateOfBirth;

	/** The time that the actor died or was abolished. */
	private GregorianCalendar dateOfDeath;

	/** The file name where the actor's portrait is located. */
	private String portraitPath;

	/** This actor's opinions on other actors. */
	private MutableMap<Actor, Byte> opinions;

	public Actor(Game game) {
		this.game = game;
		id = game.nextActorId();
		portraitPath = "";
		dateOfBirth = game.getIngameDate();
		setOpinions(Maps.mutable.empty());
	}

	public LivingStatus getLivingStatus() {
		if (dateOfBirth == null)
			return LivingStatus.NOT_YET_BORN;
		double timeSinceBirth = Utilities.getDuration(dateOfBirth, game.getIngameDate(), "year");
		if (timeSinceBirth < 0)
			return LivingStatus.NOT_YET_BORN;
		if (dateOfDeath == null)
			return LivingStatus.ALIVE;
		double timeSinceDeath = Utilities.getDuration(dateOfDeath, game.getIngameDate(), "year");
		if (timeSinceDeath < 0)
			return LivingStatus.ALIVE;
		return LivingStatus.DEAD;
	}

	/** The time in years since the actor was born or created. */
	public double getAgeYears() {
		if (getLivingStatus().equals(LivingStatus.NOT_YET_BORN)) {
			return Double.NEGATIVE_INFINITY;
		} else if (getLivingStatus().equals(LivingStatus.ALIVE)) {
			double age = Utilities.getDuration(dateOfBirth, game.getIngameDate(), "year");
			if (game.getIngameDate().get(GregorianCalendar.DAY_OF_MONTH) == dateOfBirth
					.get(GregorianCalendar.DAY_OF_MONTH)
					&& game.getIngameDate().get(GregorianCalendar.MONTH) == dateOfBirth.get(GregorianCalendar.MONTH)) {
				return age + 1;
			} else {
				return age;
			}
		} else {
			double age = Utilities.getDuration(dateOfBirth, dateOfDeath, "year");
			if (dateOfDeath.get(GregorianCalendar.DAY_OF_MONTH) == dateOfBirth.get(GregorianCalendar.DAY_OF_MONTH)
					&& dateOfDeath.get(GregorianCalendar.MONTH) == dateOfBirth.get(GregorianCalendar.MONTH)) {
				return age + 1;
			} else {
				return age;
			}
		}
	}

	public void changeOpinion(Actor actor, int amountChange) {
		if (amountChange == 0)
			return;
		int oldAmount = 0;
		if (opinions.containsKey(actor)) {
			oldAmount = (int) opinions.get(actor);
		}
		int newAmount = oldAmount + amountChange;
		if (amountChange > 0) {
			if (newAmount > 100 || newAmount <= oldAmount) {
				newAmount = 100;
			}
		}
		if (amountChange < 0) {
			if (newAmount < -100 || newAmount >= oldAmount) {
				newAmount = -100;
			}
		}
		byte result = (byte) newAmount;
		opinions.put(actor, result);
	}

	public String opinionOf(Actor actor) {
		if (!opinions.containsKey(actor)) {
			return "No Opinion";
		}
		byte opNum = opinions.get(actor);
		if (opNum < -75) {
			return "Furious Anger";
		} else if (opNum < -50) {
			return "Strong Contempt";
		} else if (opNum < -25) {
			return "Serious Disapproval";
		} else if (opNum < -5) {
			return "Mild Disapproval";
		} else if (opNum <= 5) {
			return "Mixed Feelings";
		} else if (opNum <= 25) {
			return "Mild Approval";
		} else if (opNum <= 50) {
			return "Good Feelings";
		} else if (opNum <= 75) {
			return "Strong Respect";
		} else {
			return "Adoration";
		}
	}

	/**
	 * The game that this actor is part of.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * The game that this actor is part of.
	 * 
	 * @param game the game to set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * The commonly accepted name for the actor.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * The commonly accepted name for the actor.
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * The absolute bare necessities this actor needs to survive.
	 * 
	 * @return the survivalNeeds
	 */
	public TreeMap<Resource, Double> getSurvivalNeeds() {
		return survivalNeeds;
	}

	/**
	 * The absolute bare necessities this actor needs to survive.
	 * 
	 * @param survivalNeeds the survivalNeeds to set
	 */
	public void setSurvivalNeeds(TreeMap<Resource, Double> survivalNeeds) {
		this.survivalNeeds = survivalNeeds;
	}

	/**
	 * What the actor needs to be seen as a productive member of their society.
	 * 
	 * @return the socialNeeds
	 */
	public TreeMap<Resource, Double> getSocialNeeds() {
		return socialNeeds;
	}

	/**
	 * What the actor needs to be seen as a productive member of their society.
	 * 
	 * @param socialNeeds the socialNeeds to set
	 */
	public void setSocialNeeds(TreeMap<Resource, Double> socialNeeds) {
		this.socialNeeds = socialNeeds;
	}

	/**
	 * What the actor needs to feel personally satisfied with their life.
	 * 
	 * @return the contentmentNeeds
	 */
	public TreeMap<Resource, Double> getContentmentNeeds() {
		return contentmentNeeds;
	}

	/**
	 * What the actor needs to feel personally satisfied with their life.
	 * 
	 * @param contentmentNeeds the contentmentNeeds to set
	 */
	public void setContentmentNeeds(TreeMap<Resource, Double> contentmentNeeds) {
		this.contentmentNeeds = contentmentNeeds;
	}

	/**
	 * What the actor needs to be seen as a high-status member of their society.
	 * 
	 * @return the prestigeNeeds
	 */
	public TreeMap<Resource, Double> getPrestigeNeeds() {
		return prestigeNeeds;
	}

	/**
	 * What the actor needs to be seen as a high-status member of their society.
	 * 
	 * @param prestigeNeeds the prestigeNeeds to set
	 */
	public void setPrestigeNeeds(TreeMap<Resource, Double> prestigeNeeds) {
		this.prestigeNeeds = prestigeNeeds;
	}

	/**
	 * The time that the actor was born or created.
	 * 
	 * @return the dateOfBirth
	 */
	public GregorianCalendar getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * The time that the actor was born or created.
	 * 
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(GregorianCalendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * The time that the actor died or was abolished.
	 * 
	 * @return the dateOfDeath
	 */
	public GregorianCalendar getDateOfDeath() {
		return dateOfDeath;
	}

	/**
	 * The time that the actor died or was abolished.
	 * 
	 * @param dateOfDeath the dateOfDeath to set
	 */
	public void setDateOfDeath(GregorianCalendar dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	/**
	 * @return the portraitPath
	 */
	public String getPortraitPath() {
		return portraitPath;
	}

	/**
	 * @param portraitPath the portraitPath to set
	 */
	public void setPortraitPath(String portraitPath) {
		this.portraitPath = portraitPath;
	}

	public MutableMap<Actor, Byte> getOpinions() {
		return opinions;
	}

	public void setOpinions(MutableMap<Actor, Byte> opinions) {
		this.opinions = opinions;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Actor))
			return false;
		Actor otherActor = (Actor) obj;
		return id == otherActor.id;
	}

	@Override
	public int compareTo(Actor otherActor) {
		if (otherActor == null)
			return 1;
		if (this instanceof Person && otherActor instanceof Person) {
			Person thisPerson = (Person) this;
			Person otherPerson = (Person) otherActor;
			return thisPerson.getFullName().compareTo(otherPerson.getFullName());
		} else if (this instanceof Government && otherActor instanceof Government) {
			Government thisGov = (Government) this;
			Government otherGov = (Government) otherActor;
			return thisGov.getFullName().compareTo(otherGov.getFullName());
		} else {
			if (id < otherActor.id) {
				return -1;
			} else if (id > otherActor.id) {
				return 1;
			} else
				return 0;
		}

	}
}
