package models.actors;

import systemState.Game;

public class Sexuality extends PeopleGroup {
	
	private double genderAttraction;
	
	private double libido;

	public Sexuality(Game game, double genderAttraction, double libido) {
		super(game);
	}

	/** Ranges from 0 (straight) to 1 (gay) */
	public double getGenderAttraction() {
		return genderAttraction;
	}

	/** Ranges from 0 (straight) to 1 (gay) */
	public void setGenderAttraction(double genderAttraction) {
		this.genderAttraction = genderAttraction;
	}

	/** Ranges from 0 (asexual) to 1 (sexual) */
	public double getLibido() {
		return libido;
	}

	/** Ranges from 0 (asexual) to 1 (sexual) */
	public void setLibido(double libido) {
		this.libido = libido;
	}

}
