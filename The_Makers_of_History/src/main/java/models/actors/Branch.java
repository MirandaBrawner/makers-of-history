/** This class represents a branch of a government. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Branch extends PeopleGroup {
	
	public Branch(Game game) {
		super(game);
		governments = new ArrayList<>();
		offices = new ArrayList<>();
		leadershipOffices = new ArrayList<>();
	}

	/** The long form of the name of this branch of government. 
	 It will often include the name of the government it is part of. */
	private String fullName;
	
	/** The short form of this branch's name. 
	It will often omit the name of the government. */
	private String shortName;

	/** A list of any governments that this branch is part of. */
	private ArrayList<Government> governments;
	
	/** A list of any offices that are part of this branch of government. */
	private ArrayList<Office> offices;
	
	/** A list of the highest ranking offices in this branch of government. */
	private ArrayList<Office> leadershipOffices;

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the governments
	 */
	public ArrayList<Government> getGovernments() {
		return governments;
	}

	/**
	 * @param governments the governments to set
	 */
	public void setGovernments(ArrayList<Government> governments) {
		this.governments = governments;
	}

	/**
	 * @return the offices
	 */
	public ArrayList<Office> getOffices() {
		return offices;
	}

	/**
	 * @param offices the offices to set
	 */
	public void setOffices(ArrayList<Office> offices) {
		this.offices = offices;
	}

	/**
	 * @return the leadershipOffices
	 */
	public ArrayList<Office> getLeadershipOffices() {
		return leadershipOffices;
	}

	/**
	 * @param leadershipOffices the leadershipOffices to set
	 */
	public void setLeadershipOffices(ArrayList<Office> leadershipOffices) {
		this.leadershipOffices = leadershipOffices;
	}
	
}
