/** A class to represent non-governmental organizations, such as corporations, charities, and fellowships. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class NGO extends PeopleGroup {
	
	public NGO(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	/** A list of people who hold high-ranking positions in this organization. */
	private ArrayList<Person> leaders;
	
	/** A list of notable people who are members of this organization, either as leaders or as rank-and-file members. */
	private ArrayList<Person> notableMembers;
	
}
