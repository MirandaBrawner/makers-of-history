package models.actors;

import systemState.Game;

public class AgeGroup extends PeopleGroup {
	
	private int lowerBoundInclusive;
	private int upperBoundInclusive;

	public AgeGroup(Game game, int lowerBoundInclusive, int upperBoundInclusive) {
		super(game);
		this.lowerBoundInclusive = lowerBoundInclusive;
		this.upperBoundInclusive = upperBoundInclusive;
	}

	public int getLowerBoundInclusive() {
		return lowerBoundInclusive;
	}

	public int getUpperBoundInclusive() {
		return upperBoundInclusive;
	}
	
	
}
