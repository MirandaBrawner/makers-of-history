/** A group of people, whether formal or informal. */

package models.actors;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

import systemState.Game;

public class PeopleGroup extends Actor {

    public PeopleGroup(Game game) {
		super(game);
		subgroups = Lists.mutable.empty();
		supergroups = Lists.mutable.empty();
		ultimateComponents = Lists.mutable.empty();
	}

	/** Plural Noun */
	private String autonym = "";
	
	private String singularAutonym = "";
	
	/** Plural Noun */
	private String exonym = "";
	
	private String singularExonym = "";
	
	private String pluralAdjective = "";
	
	private String adjective = "";

	/** The number of people in this group. */
	private long population;

	/** A list of more specific groups that collectively make up this group. */
	private MutableList<Long> subgroups;

	/** A list of broader groups that include this group. */
	private MutableList<Long> supergroups;

	private MutableList<Long> ultimateComponents;

	private String nameVarRule = "";

	/**
	 * @return the autonym
	 */
	public String getAutonym() {
		return autonym;
	}

	/**
	 * @param autonym the autonym to set
	 */
	public void setAutonym(String autonym) {
		this.autonym = autonym;
	}

	/**
	 * @return the exonym
	 */
	public String getExonym() {
		return exonym;
	}

	/**
	 * @param exonym the exonym to set
	 */
	public void setExonym(String exonym) {
		this.exonym = exonym;
	}

	public String getSingularAutonym() {
		return singularAutonym;
	}

	public void setSingularAutonym(String singularAutonym) {
		this.singularAutonym = singularAutonym;
	}

	public String getSingularExonym() {
		return singularExonym;
	}

	public void setSingularExonym(String singularExonym) {
		this.singularExonym = singularExonym;
	}

	public String getPluralAdjective() {
		return pluralAdjective;
	}

	public void setPluralAdjective(String pluralAdjective) {
		this.pluralAdjective = pluralAdjective;
	}

	/**
	 * @return the adjective
	 */
	public String getAdjective() {
		return adjective;
	}

	/**
	 * @param adjective the adjective to set
	 */
	public void setAdjective(String adjective) {
		this.adjective = adjective;
		this.pluralAdjective = adjective;
	}



    
	/**
	 * @return the population
	 */
	public long getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(long population) {
		this.population = population;
	}

	/**
	 * @return the subgroups
	 */
	public MutableList<Long> getSubgroups() {
		return subgroups;
	}

	/**
	 * @param subgroups the subgroups to set
	 */
	public void setSubgroups(MutableList<Long> subgroups) {
		this.subgroups = subgroups;
	}

	/**
	 * @return the supergroups
	 */
	public MutableList<Long> getSupergroups() {
		return supergroups;
	}

	/**
	 * @param supergroups the supergroups to set
	 */
	public void setSupergroups(MutableList<Long> supergroups) {
		this.supergroups = supergroups;
	}

	/**
	 * @return the ultimateComponents
	 */
	public MutableList<Long> getUltimateComponents() {
		return ultimateComponents;
	}

	/**
	 * @param ultimateComponents the ultimateComponents to set
	 */
	public void setUltimateComponents(MutableList<Long> ultimateComponents) {
		this.ultimateComponents = ultimateComponents;
	}
	
	public void updateUltimates() {
		if (subgroups.isEmpty()) {
			ultimateComponents.removeAll(ultimateComponents);
			population = 10000;
		} else {
			for (long childID: subgroups) {
				PeopleGroup subgroup = (PeopleGroup) this.getGame().lookupActorByID(childID);
				if (subgroup.subgroups.isEmpty()) {
					if (!ultimateComponents.contains(childID)) {
						ultimateComponents.add(childID);
						population += 10000;
						subgroup.population = 10000;
					} 
				} else {
					subgroup.updateUltimates();
					for (long grandchildID: subgroup.ultimateComponents) {
						if (!ultimateComponents.contains(grandchildID)) {
							ultimateComponents.add(grandchildID);
						}
					}
					population += subgroup.population;
				}
			}
		}
	}
	
	public boolean isTopLevel() {
		return supergroups == null || supergroups.isEmpty();
	}
	
	public boolean isSecondLevel() {
		if (isTopLevel()) return false;
		if (supergroups.size() > 1) return false;
		for (Long parentID: supergroups) {
			PeopleGroup parent = (PeopleGroup) this.getGame().lookupActorByID(parentID);
			return parent.isTopLevel();
		}
		return false;
	}

	public String getNameVarRule() {
		return nameVarRule;
	}

	public void setNameVarRule(String nameVarRule) {
		this.nameVarRule = nameVarRule;
	}
    
}
