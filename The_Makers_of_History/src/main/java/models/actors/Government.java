/** This class represents any organizations that either exercise sovereign authority, 
 either as independent states or subjects, or are recognized by a significant number 
 of people as legitimate governments. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Government extends PeopleGroup {
	
	public Government(Game game) {
		super(game);
		overlords = new ArrayList<>();
		subjects = new ArrayList<>();
		branches = new ArrayList<>();
	}

	/** The long form of this government's name, used in certain formal contexts. */
	private String fullName;
	
	/** The short form of this government's name, if it has one. */
	private String shortName;
	
	/** The adjective used to describe this government and its people. */
	private String adjective;
	
	/** A list of any governments that wield power over this one. */
	private ArrayList<Government> overlords;
	
	/** A list of any governments that answer to this one, such as constituents,
	 colonies, puppets, or vassals. */
	private ArrayList<Government> subjects;
	
	/** A list of branches of this government. */
	private ArrayList<Branch> branches;
	
	/** The total number of people living in this country and all of its dependencies. */
	private long population;

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the adjective
	 */
	public String getAdjective() {
		return adjective;
	}

	/**
	 * @param adjective the adjective to set
	 */
	public void setAdjective(String adjective) {
		this.adjective = adjective;
	}

	/**
	 * @return the overlords
	 */
	public ArrayList<Government> getOverlords() {
		return overlords;
	}

	/**
	 * @param overlords the overlords to set
	 */
	public void setOverlords(ArrayList<Government> overlords) {
		this.overlords = overlords;
	}

	/**
	 * @return the subjects
	 */
	public ArrayList<Government> getSubjects() {
		return subjects;
	}

	/**
	 * @param subjects the subjects to set
	 */
	public void setSubjects(ArrayList<Government> subjects) {
		this.subjects = subjects;
	}

	/**
	 * @return the branches
	 */
	public ArrayList<Branch> getBranches() {
		return branches;
	}

	/**
	 * @param branches the branches to set
	 */
	public void setBranches(ArrayList<Branch> branches) {
		this.branches = branches;
	}

	/**
	 * @return the population
	 */
	public long getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(long population) {
		this.population = population;
	}

}
