/** A collection of variables relating to the person's gender, 
    sexuality, and fertility. */
package models.actors;

import java.util.ArrayList;

import systemState.Game;
import systemState.Utilities;

public class Gender extends PeopleGroup {
    
    /** Gender identity. Ranges from 0 (male) through 0.5 (nonbinary) 
    to 1 (female). */
    private double gender;
    
    /** Assigned gender. Ranges from 0 (assigned male) through
    0.5 (assigned nonbinary or no gender assigned) to 1 (assigned female). */
    private double assignedGender;
    
    /** Apparent gender. Ranges from 0 (masculine) through 
    0.5 (androgynous) to 1 (feminine). */
    private double apparentGender;
    
    /** Intensity of sexual attraction and energy. Ranges from 0 (asexual) 
    through 0.5 (typical libido) to 1 (high libido). */
    private double libido;
    
    /** Gender that the person is most attracted to. Ranges from 0 (men only) 
     through 0.5 (pansexual) to 1 (women only). */
    private double preferredGender;
    
    /** Ability to conceive children as the mother. Ranges from 
    (no ability) to 1 (likely to conceive). This is the base value.
    It is modified by other factors. */
    private double baseMaternalFertility;
    
    /** Ability to conceive children as the father. Ranges from 
    (no ability) to 1 (likely to conceive). This is the base value. 
    It is modified by other factors. */
    private double basePaternalFertility;
    
    /** Likelihood of a miscarriage if the character becomes pregnant. 
     Ranges from 0 (very unlikely) to 1 (very likely). This is the 
     base value. It is modified by other factors. */
    private double baseMiscarriageChance;
    
	public Gender(Game game, double gender, double assignedGender, double apparentGender, double libido, 
			double preferredGender, double baseMaternalFertility, double basePaternalFertility, 
			double baseMiscarriageChance) {
		super(game);
		this.gender = gender;
		this.assignedGender = assignedGender;
		this.apparentGender = apparentGender;
		this.libido = libido;
		this.preferredGender = preferredGender;
		this.baseMaternalFertility = baseMaternalFertility;
		this.basePaternalFertility = basePaternalFertility;
		this.baseMiscarriageChance = baseMiscarriageChance;
	}
	
	public Gender(Game game, double gender, double assignedGender, double apparentGender, 
			double libido, double preferredGender) {
		super(game);
		this.gender = gender;
		this.assignedGender = assignedGender;
		this.apparentGender = apparentGender;
		this.libido = libido;
		this.preferredGender = preferredGender;
		this.baseMaternalFertility = assignedGender;
		this.basePaternalFertility = 1 - assignedGender;
		this.baseMiscarriageChance = 0.5;
	}
	
	public Gender(Game game) {
		super(game);
		gender = Utilities.randomBimodal();
		apparentGender = Utilities.mostlyCoupledVariables(gender, 25);
		assignedGender = Utilities.extremifyThree(apparentGender);
		preferredGender = 1 - Utilities.mostlyCoupledVariables(gender, 9);
		libido = Utilities.randomWeightedTowardOne(7);
		this.baseMaternalFertility = assignedGender;
		this.basePaternalFertility = 1 - assignedGender;
		this.baseMiscarriageChance = 0.5;
	}

	/** Retrieve gender-specific terms from the database. */
    public String getTermFromDatabase(String language, String term, char gender) {
        
        return null;
    }
    
    public double getGenderValue(String field) {
        double value;
        if (field.equalsIgnoreCase("assigned")) {
            value = assignedGender;
        } else if (field.equalsIgnoreCase("apparent")) {
            value = apparentGender;
        } else {
            value = gender;
        }
        return value;
    }
    
    /** Compares a numeric value to a set of thresholds to produce
     one of several possible strings.
     * @param field The name of the numeric variable being converted
     to a String.
     * @param thresholds An array of numbers between 0 and 1 that are
     used to convert a numeric value into a string. Must be in 
     ascending order. 
     * @param options An array of strings, one of which will be selected.
     Must have one more element than the thresholds.
     * @return  A text representation of some numerical value. */
    public String getString(String field, double[] thresholds, 
            String[] options) {
        double value = getGenderValue(field);
        for (int i = 0; i < thresholds.length; i++) {
            if (value < thresholds[i]) return options[i];
        }
        return options[thresholds.length];
    }
    
    public String getMainAdjective(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"male", "nonbinary", "female"};
        return getString(field, thresholds, options);
    }
    
    public String getAppearanceAdjective(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"masculine", "androgynous", "feminine"};
        return getString(field, thresholds, options);
    }
    
    public String getAppearanceNoun(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"masculinity", "androgyny", "femininity"};
        return getString(field, thresholds, options);
    }
    
    public String getAdultNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"man", "person", "woman"};
        return getString(field, thresholds, options);
    }
    
    public static String getAdultNounPlural(String field, 
            ArrayList<Person> people) {
        int numberOfWomen = 0;
        int numberOfMen = 0;
        for (Person person: people) {
            String singular = person.getGender().getAdultNounSingular(field);
            if (singular.equals("woman")) {
                numberOfWomen++;
            } else if (singular.equals("man")) {
                numberOfMen++;
            }
            if (numberOfWomen > 0 && numberOfMen > 0) {
                return "people";
            }
        }
        if (numberOfWomen > 0 && numberOfMen == 0) {
            return "women";
        } else if (numberOfMen > 0 && numberOfWomen == 0) {
            return "men";
        }
        return "people";
    }
    
    public String getYouthNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"boy", "child", "girl"};
        return getString(field, thresholds, options);
    }
    
    public String getBabyNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"a boy", "intersex", "a girl"};
        return getString(field, thresholds, options);
    }
    
    public String getParentNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"father", "parent", "mother"};
        return getString(field, thresholds, options);
    }
    
    public String getGrandparentNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"grandfather", "grandparent", "grandmother"};
        return getString(field, thresholds, options);
    }
    
    public String getOffspringNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"son", "child", "daughter"};
        return getString(field, thresholds, options);
    }
    
    public String getGrandchildNounSingular(String field) {
        double[] thresholds = {0.3, 0.7};
        String[] options = {"Grandson", "Grandchild", "Granddaughter"};
        return getString(field, thresholds, options);
    }

	public double getGender() {
		return gender;
	}

	public void setGender(double gender) {
		this.gender = gender;
	}

	public double getAssignedGender() {
		return assignedGender;
	}

	public void setAssignedGender(double assignedGender) {
		this.assignedGender = assignedGender;
	}

	public double getApparentGender() {
		return apparentGender;
	}

	public void setApparentGender(double apparentGender) {
		this.apparentGender = apparentGender;
	}

	public double getLibido() {
		return libido;
	}

	public void setLibido(double libido) {
		this.libido = libido;
	}

	public double getPreferredGender() {
		return preferredGender;
	}

	public void setPreferredGender(double preferredGender) {
		this.preferredGender = preferredGender;
	}

	public double getBaseMaternalFertility() {
		return baseMaternalFertility;
	}

	public void setBaseMaternalFertility(double baseMaternalFertility) {
		this.baseMaternalFertility = baseMaternalFertility;
	}

	public double getBasePaternalFertility() {
		return basePaternalFertility;
	}

	public void setBasePaternalFertility(double basePaternalFertility) {
		this.basePaternalFertility = basePaternalFertility;
	}

	public double getBaseMiscarriageChance() {
		return baseMiscarriageChance;
	}

	public void setBaseMiscarriageChance(double baseMiscarriageChance) {
		this.baseMiscarriageChance = baseMiscarriageChance;
	}
    
}
