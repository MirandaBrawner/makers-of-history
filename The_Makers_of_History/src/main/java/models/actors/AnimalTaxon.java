/** This class represents a taxon, or grouping, of wild or domesticated animals.
 It can be broad or narrow. */

package models.actors;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.TreeMap;

import models.resources.Nutrient;
import models.resources.Resource;
import systemState.Game;

public class AnimalTaxon extends Actor {
    
    public AnimalTaxon(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	/** The total number of individual animals of this taxon. */
    private BigInteger population;

    /** The average length, in years, of this animal's life. */
    private double lifeSpan;

    /** The average time it takes this animal to reach maturity, in years */
    private double childhoodLength;

    /** The average time in this animal's life between fertilization and birth, in years */
    private double gestationLength;

    /** The average number of babies that are born together. */
    private double litterSize;

    /** The average mass of one of these animals, in kilograms. */
    private double mass;

    /** A measure of how tightly-knit the communities of this animal tend to be.
     High values indicate a more communal lifestyle. 
     Low values indicate a more individualistic lifestyle. */
    private int sociality;

    /** A measure of how the animal's typical willingness to attack. */
    private int aggression;

    /** A measure how easily the animal can learn new skills, pursue 
     complex objectives, manipulate its environment, and understand 
     abstract ideas. */
    private int cleverness;

    /** A measure of how easily the animal can travel undetected, 
     either to sneak up on prey or to evade predators. */
    private int stealth;

    /** A measure of to what extent the animal is of the right size, 
     shape, strength, and temperament to carry a human rider. */
    private int ridability;

    /** A list of any broader, more general groups of animals that this 
     group of animals is a part of. */
    private ArrayList<AnimalTaxon> supertaxa;

    /** A list of any specific varieties of this group of animals. */
    private ArrayList<AnimalTaxon> subtaxa;

    /** A list of the nutrients, and how much of them (in grams per day) this 
     animal needs to be in optimal health. */
    private TreeMap<Nutrient, Double> dietaryNeeds;

    /** A list of any animals that this animal typically eats. */
    private ArrayList<AnimalTaxon> prey;

    /** A list of any animals that typically hunt this animal. */
    private ArrayList<AnimalTaxon> predators;

    /** A list of raw and processed materials, other than animals, 
     that this animal typically eats, whether in the wild or
     in captivity. */
    private ArrayList<Resource> nonAnimalDiet;

    public BigInteger getPopulation() {
        return population;
    }

    public void setPopulation(BigInteger population) {
        this.population = population;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(double lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public double getChildhoodLength() {
        return childhoodLength;
    }

    public void setChildhoodLength(double childhoodLength) {
        this.childhoodLength = childhoodLength;
    }

    public double getGestationLength() {
        return gestationLength;
    }

    public void setGestationLength(double gestationLength) {
        this.gestationLength = gestationLength;
    }

    public double getLitterSize() {
        return litterSize;
    }

    public void setLitterSize(double litterSize) {
        this.litterSize = litterSize;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public int getSociality() {
        return sociality;
    }

    public void setSociality(int sociality) {
        this.sociality = sociality;
    }

    public int getAggression() {
        return aggression;
    }

    public void setAggression(int aggression) {
        this.aggression = aggression;
    }

    public int getCleverness() {
        return cleverness;
    }

    public void setCleverness(int cleverness) {
        this.cleverness = cleverness;
    }

    public int getStealth() {
        return stealth;
    }

    public void setStealth(int stealth) {
        this.stealth = stealth;
    }

    public int getRidability() {
        return ridability;
    }

    public void setRidability(int ridability) {
        this.ridability = ridability;
    }

    public ArrayList<AnimalTaxon> getSupertaxa() {
        return supertaxa;
    }

    public void setSupertaxa(ArrayList<AnimalTaxon> supertaxa) {
        this.supertaxa = supertaxa;
    }

    public ArrayList<AnimalTaxon> getSubtaxa() {
        return subtaxa;
    }

    public void setSubtaxa(ArrayList<AnimalTaxon> subtaxa) {
        this.subtaxa = subtaxa;
    }

    public TreeMap<Nutrient, Double> getDietaryNeeds() {
        return dietaryNeeds;
    }

    public void setDietaryNeeds(TreeMap<Nutrient, Double> dietaryNeeds) {
        this.dietaryNeeds = dietaryNeeds;
    }

    public ArrayList<AnimalTaxon> getPrey() {
        return prey;
    }

    public void setPrey(ArrayList<AnimalTaxon> prey) {
        this.prey = prey;
    }

    public ArrayList<AnimalTaxon> getPredators() {
        return predators;
    }

    public void setPredators(ArrayList<AnimalTaxon> predators) {
        this.predators = predators;
    }

    public ArrayList<Resource> getNonAnimalDiet() {
        return nonAnimalDiet;
    }

    public void setNonAnimalDiet(ArrayList<Resource> nonAnimalDiet) {
        this.nonAnimalDiet = nonAnimalDiet;
    }
	
    
}
