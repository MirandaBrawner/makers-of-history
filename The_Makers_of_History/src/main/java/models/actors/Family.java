/** The second-smallest unit of society, larger than the individual but
 smaller than most other groups. This class represents a small collection 
 of people who see themselves as close relatives. */

package models.actors;

import java.util.ArrayList;

import systemState.Game;

public class Family extends PeopleGroup {
	
	public Family(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	/** A list of known individuals who are considered part of this family. */
	private ArrayList<Person> members;

}
