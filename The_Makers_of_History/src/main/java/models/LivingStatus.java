package models;

public enum LivingStatus {
	NOT_YET_BORN,
	ALIVE,
	DEAD
}
