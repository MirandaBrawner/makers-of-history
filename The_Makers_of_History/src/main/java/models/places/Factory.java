/** A place to convert raw materials into processed goods. */

package models.places;

import java.util.TreeMap;

import models.resources.Resource;

public class Factory {
    
    /** A table of the name and quantity of each resource the factory 
     takes in each day. */
    private TreeMap<Resource, Double> input;
    
    /** A table of the name and quantity of each resource the factory
     produces each day. */
    private TreeMap<Resource, Double> output;

}
