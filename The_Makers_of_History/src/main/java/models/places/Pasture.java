/** An area of land for raising domestic animals. */

package models.places;

import java.util.TreeMap;

import models.actors.AnimalTaxon;

public class Pasture extends Settlement{
    
    /** A table of the animals raised here, and the area devoted to 
     each animal, in square km. */
    private TreeMap<AnimalTaxon, Double> animals;
    
    /** The total area occupied by this pasture, in square km.
     * @return  The pasture's total area. */
    public double getTotalArea() {
        double sum = 0;
        for (double partialArea: animals.values()) {
            sum += partialArea;
        }
        return sum;
    }
    
    /** Given a category of animal, returns the area of this pasture that is
     devoted to that animal. 
     * @param animal A category of animals.
     * @return The area, in square km. */
    public double getAreaForAnimal(AnimalTaxon animal) {
        double sum = 0;
        for (AnimalTaxon livestock: animals.keySet()) {
            if (livestock.equals(animal)) {
                sum += animals.get(livestock);
            } else if (!livestock.getSupertaxa().isEmpty()) {
                for (AnimalTaxon ancestor: livestock.getSupertaxa()) {
                    if (ancestor.equals(animal)) {
                        sum += animals.get(livestock);
                    }
                }
            }
        }
        return sum;
    }
    
}
