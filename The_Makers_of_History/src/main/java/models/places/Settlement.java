/** An inhabited site, such as a city, fort, or farm. */

package models.places;

public class Settlement {
    
    /** The name of the settlement that is most commonly used by the
     people inhabiting it. */
    private String name;
    
    /** The number of people currently at this settlement. */
    private long population;

}
