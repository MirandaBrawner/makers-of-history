/** A place for education and research. */

package models.places;

public class School extends Settlement {

    /** The number of people enrolled in this school. */
    private long numberOfStudents;
    
    /** The number of instructors employed at this school. */
    private long numberOfTeachers;
    
    /** The number of non-instructor laborers working for this school. */
    private long numberOfWorkers;
}
