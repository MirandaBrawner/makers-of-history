package models.places;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

public class Region implements Comparable<Region> {
	private long id;
	private String name;
	private long population;
	private MutableList<Long> parents;
	private MutableList<Long> children;
	public Region(long id, String name) {
		this.id = id;
		this.name = name;
		population = 0;
		parents = Lists.mutable.empty();
		children = Lists.mutable.empty();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPopulation() {
		return population;
	}
	public void setPopulation(long population) {
		this.population = population;
	}
	public MutableList<Long> getParents() {
		return parents;
	}
	public void setParents(MutableList<Long> parents) {
		this.parents = parents;
	}
	public MutableList<Long> getChildren() {
		return children;
	}
	public void setChildren(MutableList<Long> children) {
		this.children = children;
	}
	@Override
	public int compareTo(Region otherRegion) {
		if (otherRegion == null) return 1;
		int nameCompare = name.compareTo(otherRegion.name);
		if (nameCompare == 0) {
			return (int) (id - otherRegion.id);
		}
		return nameCompare;
	}
}
