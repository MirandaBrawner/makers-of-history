/** A place for gathering rocks and minerals. */

package models.places;

import java.util.TreeMap;

import models.resources.Mineral;

public class Mine extends Settlement {

    /** A table of minerals unearthed in this mine, with the amount 
     gathered daily. */
    private TreeMap<Mineral, Double> output;
    
}
