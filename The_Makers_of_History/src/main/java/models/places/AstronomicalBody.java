/** This class represents stars, planets and moons, whether inhabited or barren. */

package models.places;

import java.util.ArrayList;

import javafx.geometry.Point3D;

public class AstronomicalBody {
	
	/** The name of the star, planet, or moon. */
	private String name;

	/** The average distance from a point on the surface to the body's center. */
	private long radius;
	
	/** The total number of people living on or around this astronomical object. */
	private long population;
	
	/** A list of astronomical objects that this object orbits. */
	private ArrayList<AstronomicalBody> orbitParent;
	
	/** A list of astronomical objects that orbit this object. */
	private ArrayList<AstronomicalBody> orbitChildren;
	
	/** The object's location in the galaxy, using a rectangular coordinate system. */
	private Point3D location;
	
}
