/** MapLocations are the smallest regions represented on the map. */

package models.places;

import java.util.ArrayList;

import models.actors.Government;

public class MapLocation {
	
	/** An integer to uniquely identify the location */
	public long id;
	
	/** The name of the location in the language used by the majority or plurality of its inhabitants. */
	private String name = "Unkown Location";
	
	/** The planet, moon, asteroid, star, or space station where this location can be found. */
	private AstronomicalBody world;
	
	/** The location's latitude, in degrees, north of the equator. */
	private double latitude;
	
	/** The location's longitude, in degrees, east of the prime meridian in Greenwich. */
	private double longitude;
	
	/** The total number of people currently in this area.*/
	private long population;
	
	/** A list of governments who exercise de facto control over this location. */
	private ArrayList<Government> controllers;
	
	/** A list of countries who claim this location as their own, but do not have de facto control. */
	private ArrayList<Government> claimants;
	
	public MapLocation() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AstronomicalBody getWorld() {
		return world;
	}

	public void setWorld(AstronomicalBody world) {
		this.world = world;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public long getPopulation() {
		return population;
	}

	public void setPopulation(long population) {
		this.population = population;
	}

	public ArrayList<Government> getControllers() {
		return controllers;
	}

	public void setControllers(ArrayList<Government> controllers) {
		this.controllers = controllers;
	}

	public ArrayList<Government> getClaimants() {
		return claimants;
	}

	public void setClaimants(ArrayList<Government> claimants) {
		this.claimants = claimants;
	}
	
	
}
