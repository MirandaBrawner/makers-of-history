/** A place used for growing crops. */

package models.places;

import java.util.TreeMap;

import models.resources.PlantTaxon;

public class Farm extends Settlement {
    
    /** A measure of how close this farm comes to meeting its optimal 
     output. */
    private double efficiency;
    
    /** A table of the crops grown on this farm, and the area devoted to 
     each crop, in square km. */
    private TreeMap<PlantTaxon, Double> crops;
    
    /** The total area occupied by this farm, in square km.
     * @return  The farm's total area. */
    public double getTotalArea() {
        double sum = 0;
        for (double partialArea: crops.values()) {
            sum += partialArea;
        }
        return sum;
    }
    
    /** Given a category of plants, returns the area of this farm that is
     devoted to that crop. 
     * @param crop A category of plants.
     * @return The area, in square km. */
    public double getAreaForCrop(PlantTaxon crop) {
        double sum = 0;
        for (PlantTaxon plant: crops.keySet()) {
            if (plant.equals(crop)) {
                sum += crops.get(plant);
            } else if (!plant.supertaxa.isEmpty()) {
                for (PlantTaxon ancestor: plant.supertaxa) {
                    if (ancestor.equals(crop)) {
                        sum += crops.get(plant);
                    }
                }
            }
        }
        return sum;
    }
    
}
