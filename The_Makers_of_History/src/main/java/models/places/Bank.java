package models.places;

public class Bank extends Settlement {
    
    /** The total amount of money deposited in the bank, measured in the 
     game's generic behind-the-scenes currency. */
    private double sumOfAccounts;
    
    /** The amount of money the bank has on hand, measured in the game's 
     generic behind-the-scenes currency. */
    private double reserves;

}
