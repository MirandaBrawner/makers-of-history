package systemState;

public class GameRuleOption implements Comparable<GameRuleOption> {

	private final GameRule gameRule;
	private final int id;
	private final String name;
	private final boolean achieveSafe;
	
	public GameRuleOption(GameRule gameRule, int id, String name, boolean achieveSafe) {
		this.gameRule = gameRule;
		this.id = id;
		this.name = name;
		this.achieveSafe = achieveSafe;
	}
	
	public GameRuleOption deepCopy(GameRule newGameRule) {
		return new GameRuleOption(newGameRule, id, name, achieveSafe);
	}
	
	public boolean isAllowed() {
		if (gameRule == null) return false;
		if (gameRule.getAllowedChoices() == null) return false;
		return gameRule.getAllowedChoices().contains(this);
	}
	
	public GameRule getGameRule() {
		return gameRule;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public boolean isAchieveSafe() {
		return achieveSafe;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof GameRuleOption)) return false;
		GameRuleOption otherOption = (GameRuleOption) obj;
		return compareTo(otherOption) == 0;
	}

	@Override
	public int compareTo(GameRuleOption otherOption) {
		if (otherOption == null) return 1;
		if (gameRule == null && otherOption.gameRule != null) return -1;
		if (gameRule != null && otherOption.gameRule == null) return 1;
		if ((gameRule == null && otherOption.gameRule == null) || gameRule.equals(otherOption.gameRule)) {
			return id - otherOption.id;
		} else return gameRule.compareTo(otherOption.gameRule);
	}
}
