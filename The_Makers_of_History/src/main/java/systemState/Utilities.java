package systemState;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

import javafx.scene.control.ChoiceBox;
import javafx.scene.text.Font;

public class Utilities {
	
	public static void main(String... args) {
		/*
		 * String[] vars = {"x"}; double[] values = {0}; String eq = "x ^ 3";
		 * MathFunction func = new MathFunction(vars, eq); double area = integrate(func,
		 * 0, 1, 0.00001, "x", values); System.out.println(area);
		 * 
		 * GregorianCalendar gc = new GregorianCalendar();
		 * gc.set(GregorianCalendar.YEAR, -5); for (int i = 0; i < 5; i++) {
		 * gc.roll(GregorianCalendar.YEAR, false); int year =
		 * gc.get(GregorianCalendar.YEAR); String era = gc.get(GregorianCalendar.ERA) ==
		 * 1 ? "CE" : "BCE"; System.out.printf("The year is %d %s.\n", year, era); }
		 */
		
		//expandTable("src/main/resources/history/actors/peopleGroups/religions.txt", 4000);
		// expandTable("src/main/resources/history/actors/peopleGroups/governments.txt",
		// 1000);
		// expandTable("src/main/resources/history/actors/peopleGroups/branches.txt",
		// 2000);
		// expandTable("src/main/resources/history/actors/offices.txt", 4000);
		// expandTable("src/main/resources/history/actors/individuals.txt", 9000);
		// expandTable("src/main/resources/history/actors/peopleGroups/factions.txt",
		// 4000);
		//expandTable("src/main/resources/history/policies.txt", 200);
		
		// testFonts();
		
		testWeightedTowardOne();
	}
	
	public static double bimodalDist(double seed) {
		double base = (seed - 0.5);
		double sign = Math.signum(base);
		double absBase = Math.abs(base);
		double baseAndExp = sign * Math.pow(absBase, 1.0 / 3.0);
		double coeff = Math.pow(0.5, 2.0 / 3.0);
		return 0.5 + (baseAndExp * coeff);
	}
	
	public static double randomBimodal() {
		return bimodalDist(Math.random());
	}
	
	public static double mostlyCoupledVariables(double input, int exp) {
		double seed = Math.random() * 2 - 1;
		double offset = Math.pow(seed, exp);
		double sum = input + offset;
		if (sum > 1) return 1;
		if (sum < 0) return 0;
		return sum;
	}
	
	public static void testBimodal() {
		int buckets = 20;
		int[] results = new int[buckets];
		for (int i = 0; i < 10000; i++) {
			double bimodal = randomBimodal();
			int category = (int)(bimodal * buckets);
			results[category]++;
		}
		for (int i = 0; i < results.length; i++) {
			double lower = i / (1.0 * buckets);
			double upper = (i + 1.0) / buckets;
			System.out.printf("%.2f to %.2f: %d\n", lower, upper, results[i]);
		}
	}
	
	public static double randomWeightedTowardOne(int exp) {
		return 1 - Math.pow(Math.random(), exp);
	}
	
	public static void testWeightedTowardOne() {
		int buckets = 20;
		int[] results = new int[buckets];
		for (int i = 0; i < 10000; i++) {
			double output = randomWeightedTowardOne(7);
			int category = (int)(output * buckets);
			if (category > 19) category = 19;
			results[category]++;
		}
		for (int i = 0; i < results.length; i++) {
			double lower = i / (1.0 * buckets);
			double upper = (i + 1.0) / buckets;
			System.out.printf("%.2f to %.2f: %d\n", lower, upper, results[i]);
		}
	}
	
	public static double extremifyTwo(double input) {
		if (input > 0.6) {
			return 1;
		} else if (input >= 0.4) {
			return (int)(Math.random() * 2);
		} else {
			return 0;
		}
	}
	
	public static double extremifyThree(double input) {
		if (input > 0.6) {
			return 1;
		} else if (input >= 0.4) {
			return 0.5;
		} else {
			return 0;
		}
	}
	
	public static String numberToLetter(double number) {
		if (number > 0.6) {
			return "F";
		} else if (number >= 0.4) {
			return "N";
		} else {
			return "M";
		}
	}
	
	public static void testCoupledVars() {
		Map<String, Integer> resultMap = new HashMap<>();
		for (int i = 0; i < 10000; i++) {
			double firstValue = randomBimodal();
			double secondValue = mostlyCoupledVariables(firstValue, 7);
			String resultString = numberToLetter(firstValue) + numberToLetter(secondValue);
			if (resultMap.containsKey(resultString)) {
				resultMap.put(resultString, resultMap.get(resultString) + 1);
			} else {
				resultMap.put(resultString, 1);
			}
		}
		for (String key: resultMap.keySet()) {
			System.out.printf("%s: %d\n", key, resultMap.get(key));
		}
	}
	
	/** Folder name is relative to src/main. It does not need slashes at the beginning and end, 
	 only slashes in the middle. localPathName should not have any slashes, but should have 
	 the file format like .jpg if there is one. */
	public static String getFullPathName(String folder, String localPathName) { 
		String currentDirectory = System.getProperty("user.dir"); // [2]
		return String.format("file:///%s/src/main/%s/%s",
				currentDirectory, folder, localPathName);
	}
	
	public static void testChoiceBoxPrimVsWrap() {
		com.sun.javafx.application.PlatformImpl.startup(()->{}); // [1]
		ChoiceBox<Integer> box = new ChoiceBox<>();
		box.getItems().addAll(1, 2);
		box.getSelectionModel().select(1);
		int newValue =  box.getSelectionModel().getSelectedItem();
		System.out.printf("Using primitive input: %d\n", newValue);
		if (newValue == 2) {
			System.out.println("Param was interpreted as a primitive holding the index.");
		} else if (newValue == 1) {
			System.out.println("Param was interpreted as a wrapper holding the list element.");
		} else {
			System.out.println("Something went wrong.");
		}
		System.out.println();
		box.getSelectionModel().select(Integer.valueOf(1));
		newValue =  box.getSelectionModel().getSelectedItem();
		System.out.printf("Using wrapper input: %d\n", newValue);
		if (newValue == 2) {
			System.out.println("Param was interpreted as a primitive holding the index.");
		} else if (newValue == 1) {
			System.out.println("Param was interpreted as a wrapper holding the list element.");
		} else {
			System.out.println("Something went wrong.");
		}
		com.sun.javafx.application.PlatformImpl.exit(); // [1]
	}

	public static void fixGroupNameEndings() {
		String inputFileName = "src/main/resources/history/actors/peopleGroups/cultures.txt";
		File inputFile = new File(inputFileName);
		String outputFileName = "src/main/resources/history/actors/peopleGroups/revisedCultures.txt";
		File outputFile = new File(outputFileName);
		try {
			Scanner scanner = new Scanner(inputFile);
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile, true));
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = cultureNameTransform(scanner.nextLine());
				bw.newLine();
				bw.write(line);
			}
			scanner.close();
			bw.flush();
			bw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static String cultureNameTransform(String input) {
		if (input.contains(";")) {
			String[] data = input.split(";");
			if (data.length >= 4) {
				String id = data[0];
				String noun = data[1];
				if (noun.endsWith("Criollos") || noun.endsWith("Mestizos")) {
					String parents = data[3];
					String base = noun.substring(noun.length() - 8, noun.length() - 1);
					String subgroup = "";
					if (noun.length() > 8) {
						subgroup = noun.substring(0, noun.length() - 8);
					}
					input = String.format("%s;%s%ss/as;%s%s/a;%s", id, subgroup, base, subgroup, base, parents);
				}
			}
		} 
		return input;
	}

	public static String estimatedYearToString(int inputYear, int cutoff) {
		int roundedYear = inputYear;
		String circa = "";
		if (inputYear < cutoff) {
			circa = "c. ";
			if (inputYear <= -100) {
				roundedYear = (int) roundToSigDigitsWhole(inputYear, 2);
			} else if (inputYear < -15) {
				roundedYear = (int) roundToSigDigitsWhole(inputYear, 1);
			} else if (inputYear < -5) {
				roundedYear = -10;
			} else if (inputYear < 5) {
				roundedYear = 1;
			} else if (inputYear < 15) {
				roundedYear = 10;
			} else if (inputYear < 100) {
				roundedYear = (int) roundToSigDigitsWhole(inputYear, 1);
			} else if (inputYear < 1000) {
				roundedYear = (int) roundToSigDigitsWhole(inputYear, 2);
			} else {
				roundedYear = (int) roundToSigDigitsWhole(inputYear, 3);
			}
			if (roundedYear < 1) {
				roundedYear++;
			}
		}
		return circa + exactYearToString(roundedYear);
	}

	public static String exactYearToString(int year) {
		String era = "";
		int displayedYear = year;
		if (year < 1) {
			era = " BCE";
			displayedYear = 1 - year;
		} else if (year < 1000) {
			era = " CE";
		}
		return String.format("%d%s", displayedYear, era);
	}

	public static String titleCase(String input) {
		String[] smallWords = { "a", "am", "are", "and", "an", "the", "of", "is", "in", "on", "for", "at", "out", "be",
				"was", "were", "has", "have", "had", "to", "with" };
		if (input.isEmpty())
			return "";
		StringBuffer sb = new StringBuffer();
		for (String word : input.split(" ")) {
			if (word.isEmpty())
				continue;
			if (!Arrays.asList(smallWords).contains(word)) {
				sb.append("" + Character.toUpperCase(word.charAt(0)));
				if (word.length() > 1) {
					sb.append(word.substring(1));
				}
			} else {
				sb.append(word);
			}
			sb.append(" ");
		}
		return sb.substring(0, sb.length() - 1);
	}

	public static String plainPathName(String pathName) {
		String[] array = pathName.split("/");
		if (array.length > 0) {
			return array[array.length - 1];
		} else
			return "";
	}

	public static String unCamelCase(String input) {
		StringBuffer sb = new StringBuffer();
		if (input.isEmpty())
			return "";
		sb.append(input.charAt(0));
		for (int i = 1; i < input.length(); i++) {
			char nextChar = input.charAt(i);
			if (Character.isUpperCase(nextChar)) {
				sb.append(" ");
				sb.append(Character.toLowerCase(nextChar));
			} else {
				sb.append(nextChar);
			}
		}
		return sb.toString();
	}
	
	public static int countDecimalPlaces(double input, int sigDigits) {
		if (sigDigits <= 0) return 0;
		if (input < 0) return countDecimalPlaces(-input, sigDigits);
		if (input == 0) return 0;
		double logTen = Math.floor(Math.log10(input));
		int baseDigitCount = (int)(-logTen);
		if (sigDigits <= 1) return baseDigitCount;
		return baseDigitCount + sigDigits - 1;
	}
	
	public static double roundToSigDigitsFraction(double input, int sigDigits) {
		if (sigDigits <= 0) return 0;
		if (input < 0) return -1 * roundToSigDigitsFraction(-input, sigDigits);
		if (input == 0) return 0;
		if (Double.isNaN(input)) return Double.NaN;
		if (Double.isInfinite(input)) return Double.POSITIVE_INFINITY;
		if (input > Long.MAX_VALUE) return Double.POSITIVE_INFINITY;
		int places = countDecimalPlaces(input, sigDigits);
		if (places <= 0) {
			return roundToSigDigitsWhole(input, sigDigits);
		} else {
			long multiplier = (long) Math.pow(10, places);
			long scaledInput = (long) (input * multiplier + 0.5);
			if (scaledInput < input) return Double.POSITIVE_INFINITY;
			return (scaledInput * 1.0) / (multiplier * 1.0);
		}
	}
	

	public static long roundToSigDigitsWhole(double input, int digits) {
		if (digits <= 0) return 0;
		if (input < 0) return -1 * roundToSigDigitsWhole(-input, digits);
		if (input == 0) return 0;
		if (Double.isNaN(input)) return 0;
		if (Double.isInfinite(input)) return Long.MAX_VALUE;
		if (input > Long.MAX_VALUE) return Long.MAX_VALUE;
		double logTen = Math.floor(Math.log10(input));
		double smallestExp = logTen - digits + 1;
		long smallestMagnitude = (long) (Math.pow(10, smallestExp));
		return ((((long) input) + (long) (0.5 * smallestMagnitude)) / smallestMagnitude) * smallestMagnitude;
	}

	public static String estimateNumber(double number) {
		if (Double.isNaN(number)) return "Undefined";
		double fracEstimate = roundToSigDigitsFraction(number, 2);
		if (fracEstimate == 0) return "0";
		if (fracEstimate < 0) {
			return "-" + estimateNumber(-fracEstimate);
		} else if (Double.isInfinite(fracEstimate)) {
			return "Infinity";
		}
		long estimate = 0;
		if (fracEstimate >= 10) {
			estimate = (long) fracEstimate;
		} else {
			int places = countDecimalPlaces(number, 2);
			String formatPattern = "%." + places + "f";
			return String.format(formatPattern, fracEstimate);
		}
		if (estimate < 10000) {
			return "" + estimate;
		} else if (estimate < 1_000_000) {
			return (estimate / 1000) + ",000";
		} else if (estimate < 10_000_000) {
			return String.format("%.1f Million", estimate / 1_000_000.0);
		} else if (estimate < 1_000_000_000) {
			return (estimate / 1_000_000) + " Million";
		} else if (estimate < 10_000_000_000L) {
			return String.format("%.1f Billion", estimate / 1_000_000_000.0);
		} else if (estimate < 1_000_000_000_000L) {
			return (estimate / 1_000_000_000) + " Billion";
		} else if (estimate < 10_000_000_000_000L) {
			return String.format("%.1f Trillion", estimate / 1_000_000_000_000.0);
		} else if (estimate < 1_000_000_000_000_000L) {
			return (estimate / 1000000000000L) + " Trillion";
		} else if (estimate < 10_000_000_000_000_000L) {
			return String.format("%.1f Quadrillion", estimate / 1_000_000_000_000_000.0);
		} else if (estimate < 1_000_000_000_000_000_000L) {
			return (estimate / 1_000_000_000_000_000L) + " Quadrillion";
		} else {
			return String.format("%.1f Quintillion", estimate / 1_000_000_000_000_000_000.0);
		}
	}

	/** Gets the name of a month in a language. Months are numbered starting from 0. */
	public static String gregorianMonthNames(String lang, int monthNum) {
		String[] names = {};
		if (lang.equals("eng")) {
			names = new String[] { "January", "February", "March", "April", "May", "June", "July", "August",
					"September", "October", "November", "December" };
		}
		if (names.length <= monthNum) {
			return "" + (monthNum + 1);
		} else {
			return names[monthNum];
		}
	};
	
	/** Gets the number of a month in a language. Months are numbered starting from 0. */
	public static int gregorianMonthNumber(String lang, String monthName) {
		String lower = monthName.toLowerCase();
		String[] names = {};
		String[] abbr = {};
		if (lang.equals("eng")) {
			names = new String[] { "january", "february", "march", "april", "may", "june", "july", "august",
					"september", "october", "november", "december" };
			abbr = new String[] { "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug",
					"sep", "oct", "nov", "dec" };
		}
		if (Arrays.asList(names).contains(lower)) {
			return Arrays.asList(names).indexOf(lower);
		} else if (Arrays.asList(abbr).contains(lower)) {
			return Arrays.asList(abbr).indexOf(lower);
		} else {
			return 0;
		}
	}
	
	public static int daysInMonth(int year, int month) {
		int[] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (month == 1 && isGregorianLeapYear(year)) {
			return 29;
		} else {
			return days[month];
		}
	}
	
	public static int clipDayOfMonth(int year, int month, int day) {
		if (day < 1) return 1;
		while (day > daysInMonth(year, month)) {
			day--;
		}
		return day;
	}

	public static void expandTable(String filePath, int rows) {
		File file = new File(filePath);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			int highestNumber = 0;
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length > 0) {
					highestNumber = Integer.parseInt(data[0]);
				}
			}
			scanner.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(filePath, true));
			for (int i = 1; i < rows; i++) {
				bw.newLine();
				int id = i + highestNumber;
				bw.write(id + ";");
			}
			bw.flush();
			bw.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", filePath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void testFonts() {
		for (String fontName : Font.getFontNames()) {
			System.out.println(fontName);
		}
	}

	public static String generatePassword(int length) {
		StringBuffer buffer = new StringBuffer();
		for (int index = 0; index < length; index++) {
			char nextChar = (char) (33 + (int) (Math.random() * 94));
			buffer.append(nextChar);
		}
		return buffer.toString();
	}

	/**
	 * Test whether a number is approximately equal to another number, within a
	 * specified level of error. The error is not proportional to the values, it is
	 * absolute.
	 */
	public static boolean isCloseToAbsolute(double firstValue, double secondValue, double absoluteError) {
		return Math.abs(firstValue - secondValue) <= absoluteError;

	}

	/**
	 * Test whether a number is approximately equal to another number, within a
	 * specified level of error. The error is interprested as a multiple of the
	 * first value specified. For example, if you compare 5 and 6 using a relative
	 * error value of 0.5, then the absolute error would be 5 * 0.5 = 2.5
	 */
	public static boolean isCloseToRelative(double firstValue, double secondValue, double relativeError) {
		return isCloseToAbsolute(firstValue, secondValue, relativeError * firstValue);
	}

	/**
	 * "Normal Distribution". Wikipedia. Retrieved from
	 * https://en.wikipedia.org/wiki/Normal_distribution, 23 November 2019.
	 */
	public static double normalCDF(double endPoint) {
		double coeff = 1.0 / (Math.sqrt(2 * Math.PI));
		double sum = 0;
		double intervalSize = 0.001;
		for (double z = -10; z < endPoint; z += intervalSize) {
			double exponent = -0.5 * Math.pow(z, 2);
			double density = Math.pow(Math.E, exponent);
			sum += (intervalSize * density);
		}
		return sum * coeff;
	}

	/**
	 * "Log-normal Distribution". Wikipedia. Retrieved from
	 * https://en.wikipedia.org/wiki/Log-normal_distribution, 24 November 2019.
	 */
	public static double logNormalCDF(double mean, double stdev, double endPoint) {
		double log = Math.log(endPoint);
		double z = (log - mean) / stdev;
		return normalCDF(z);
	}

	/**
	 * "Integral". Wikipedia. Retrieved from https://en.wikipedia.org/wiki/Integral,
	 * 23 November 2019.
	 */
	public static double integrate(MathFunction function, double startPoint, double endPoint, double intervalSize,
			String withRespectTo, double variables[]) {
		double sum = 0;
		int index = Arrays.asList(function.getParameters()).indexOf(withRespectTo);
		if (index < 0)
			return 0;
		for (double current = startPoint; current < endPoint; current += intervalSize) {
			variables[index] = current;
			double density = function.calculate(variables, true);
			sum += (intervalSize * density);
		}
		return sum;
	}

	/**
	 * Determine whether a year would be a leap year according to the Gregorian
	 * rules, applied rectroactively to years before the rules took effect
	 * historically.
	 */
	public static boolean isGregorianLeapYear(int year) {
		if (year % 4 == 0) {
			if (year % 100 == 0) {
				return year % 400 == 0;
			} else
				return true;
		} else
			return false;
	}

	/** Calculate the duration of time between two calendar dates. */
	public static double getDuration(GregorianCalendar start, GregorianCalendar end, String field) {
		if (start.equals(end))
			return 0;
		int startEra = start.get(GregorianCalendar.ERA);
		int startYear = start.get(GregorianCalendar.YEAR);
		startYear = (startEra == 1) ? startYear : 1 - startYear;
		int startDay = start.get(GregorianCalendar.DAY_OF_YEAR);
		int endEra = end.get(GregorianCalendar.ERA);
		int endYear = end.get(GregorianCalendar.YEAR);
		endYear = (endEra == 1) ? endYear : 1 - endYear;
		int endDay = end.get(GregorianCalendar.DAY_OF_YEAR);
		int totalDayCount = 0;
		int yearStep = start.compareTo(end) < 0 ? 1 : -1;
		for (int year = startYear; year < endYear; year += yearStep) {
			totalDayCount += (isGregorianLeapYear(year) ? 366 : 365);
		}
		if (isGregorianLeapYear(startYear) && start.get(GregorianCalendar.MONTH) >= GregorianCalendar.MARCH) {
			totalDayCount--;
		}
		if (isGregorianLeapYear(endYear) && end.get(GregorianCalendar.MONTH) >= GregorianCalendar.MARCH) {
			totalDayCount++;
		}
		totalDayCount += (endDay - startDay);
		switch (field.toLowerCase()) {
		case "day":
			return totalDayCount;
		case "month":
			return (totalDayCount / 365.2425) * 12;
		case "year":
			return totalDayCount / 365.2425;
		default:
			return totalDayCount / 365.2425;
		}
	}
	
	public static MutableList<String> splitByNewLine(String input) {
		MutableList<String> output = Lists.mutable.empty();
		StringBuffer sb = new StringBuffer();
		// int localIndex = 0;
		for (int i = 0; i < input.length(); i++) {
			//System.out.printf("Global index: %d. Local Index: %d. Content of Last Line: \"%s\".\n",
					//i, localIndex, sb.toString());
			if (input.charAt(i) == '\\') {
				//System.out.println("Backslash character found.");
				if (input.charAt(i + 1) == 'n') {
					//System.out.println("Lower case n found. Line break detected.");
					output.add(sb.toString());
					//System.out.printf("Line %d has been added. Content: \"%s\".\n", output.size() - 1, sb.toString());
					sb = new StringBuffer();
					// localIndex = 0;
					i++;
				}
			} else if (input.charAt(i) == '\n') {
				//System.out.println("Line break character found.");
				output.add(sb.toString());
				//System.out.printf("Line %d has been added. Content: \"%s\".\n", output.size() - 1, sb.toString());
				sb = new StringBuffer();
				// localIndex = 0;
			} else {
				sb.append(input.charAt(i));
				// localIndex++;
			}
		}
		output.add(sb.toString());
		//System.out.printf("Line %d has been added. Content: \"%s\".\n", output.size() - 1, sb.toString());
		return output;
	}
	
	/* Sources Used:
	 * [1] Dan Vulpe and user28775. "JavaFX 2.1: Toolkit not initialized". StackOverflow, 2012-2017. Retrieved from
	 https://stackoverflow.com/questions/11273773/javafx-2-1-toolkit-not-initialized, 6 Feb 2020.
	 * [2] Mykong. "How to Get the Current Working Directory in Java." Mykong.com, 2010-2019.
	 Retrieved from https://mkyong.com/java/how-to-get-the-current-working-directory-in-java/,
	 22 February 2020.
	 * */
}
