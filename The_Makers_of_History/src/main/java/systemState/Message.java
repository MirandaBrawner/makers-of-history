package systemState;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import view.menus.ControlElement;

public class Message implements Comparable<Message> {
	
	private Server server;
	private long id;
	private String title;
	private String fullText;
	private Player author;
	private ControlElement location;
	private GregorianCalendar originalPostDate;
	private GregorianCalendar lastEditDate;
	private ArrayList<String> tags;
	
	public Message(Server server, String title, String fullText, Player author, ControlElement location, String... tags) {
		this.server = server;
		this.title = title;
		this.fullText = fullText;
		this.author = author;
		this.location = location;
		this.originalPostDate = new GregorianCalendar();
		this.lastEditDate = new GregorianCalendar();
		this.tags = new ArrayList<>();
		for (String tag: tags) {
			this.tags.add(tag);
		}
		server.add(this);
	}

	public Message(Server server, String fullText, Player author, ControlElement location) {
		this(server, "", fullText, author, location);
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Player getAuthor() {
		return author;
	}

	public void setAuthor(Player author) {
		this.author = author;
	}

	public ControlElement getLocation() {
		return location;
	}

	public void setLocation(ControlElement location) {
		this.location = location;
	}

	public GregorianCalendar getOriginalPostDate() {
		return originalPostDate;
	}

	public void setOriginalPostDate(GregorianCalendar originalPostDate) {
		this.originalPostDate = originalPostDate;
	}

	public GregorianCalendar getLastEditDate() {
		return lastEditDate;
	}

	public void setLastEditDate(GregorianCalendar lastEditDate) {
		this.lastEditDate = lastEditDate;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	@Override
	public int compareTo(Message otherMessage) {
		if (id < otherMessage.getId()) {
			return -1;
		} else if (id == otherMessage.getId()) {
			return 0;
		} else {
			return 1;
		}
	}
	
	
}
