package systemState.aop;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class ExitAspect {

	@AfterThrowing("execution(* *(..))")
	public void quit() throws Throwable {
		System.exit(1);
	}
}
