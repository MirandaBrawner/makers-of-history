package systemState;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import javafx.application.Platform;
import models.LivingStatus;
import models.actors.Actor;
import models.actors.AgeGroup;
import models.actors.Branch;
import models.actors.Caste;
import models.actors.Culture;
import models.actors.Faction;
import models.actors.Gender;
import models.actors.Government;
import models.actors.Office;
import models.actors.PeopleGroup;
import models.actors.Person;
import models.actors.Profession;
import models.actors.Religion;
import models.actors.Sexuality;
import models.ideas.Decision;
import models.ideas.Policy;
import models.ideas.Technology;
import models.places.AstronomicalBody;
import models.places.MapLocation;
import models.places.Region;
import models.resources.Resource;
import models.worldEvents.Modifier;
import models.worldEvents.WorldEvent;
import view.Launch;
import view.images.ExternalAvatar;
import view.images.ImageElement;
import view.labels.LabelElement;
import view.menus.InternalControlElement;
import view.menus.Popup;
import view.sound.SoundElement;
import view.world.WorldDisplayElement;

@SuppressWarnings("restriction")
public class Game implements Comparable<Game> {

	public static final double SMALL_NUMBER = Math.pow(2, -62);
	public static final double LARGE_NUMBER = Math.pow(2, 62);

	private Server server;
	private long id;
	private String name;
	private String password;
	private String description;
	private ExternalAvatar avatar;
	private GameStatus status;
	private HashMap<Player, Person> players;
	private HashMap<Player, Launch> playerConsoles;
	private MutableList<Player> achieveSafePlayers;
	private MutableList<GameRule> gameRules;

	private GregorianCalendar proposalDate;
	private GregorianCalendar stagingDate;
	private GregorianCalendar firstLaunchDate;
	private GregorianCalendar latestLaunchDate;
	private GregorianCalendar latestPauseDate;
	private GregorianCalendar cancellationDate;
	private GregorianCalendar completionDate;
	private GregorianCalendar ingameDate;

	private final int EARLIEST_YEAR = -100_000;
	private MutableList<Policy> policies;
	private HashMap<String, MutableList<Policy>> lawCategories;
	private MutableList<Technology> technologies;
	private MutableList<AstronomicalBody> astronomicalBodies;
	private MutableList<Region> regions;
	private MutableList<MapLocation> locations;
	private MutableList<Actor> actors;
	private String[] groupFilterNames = { "Religions", "Cultures", "Age Groups", "Sexualities", "Genders", "Castes",
			"Professions" };
	private String[] groupClassNames = { "Religion", "Culture", "AgeGroup", "Sexuality", "Gender", "Caste",
			"Profession" };
	private MutableList<Gender> genders;
	private MutableList<Sexuality> sexualities;
	private MutableList<AgeGroup> ages;
	private MutableList<Culture> cultures;
	private MutableList<Religion> religions;
	private MutableList<Government> governments;
	private MutableList<Branch> branches;
	private MutableList<Office> offices;
	private MutableList<Profession> professions;
	private MutableList<Caste> castes;
	private MutableList<Faction> factions;
	private MutableList<Resource> resources;
	private MutableList<Decision> decisions;
	private MutableList<WorldEvent> events;
	private MutableList<Modifier> modifiers;
	private MutableList<Person> individuals;
	private MutableList<Person> livingPeople;
	private Map<Integer, MutableList<String>> nameLists;
	private NameHandler nameHandler = new NameHandler(this);
	public final int NAME_LIST_NUMBER = 5;
	public final int NAME_LIST_BLOCK_SIZE = 10;
	public final int NEXT_ID_BLOCK_SIZE = 500;

	private MutableList<WorldDisplayElement> worldDisplays;
	private MutableList<ImageElement> imageElements;
	private MutableList<InternalControlElement> internalControlElements;
	private MutableList<LabelElement> labelElements;
	private MutableList<SoundElement> soundElements;

	private String[] resourceCategories = { "foodAndDrugs/candies", "foodAndDrugs/dairy", "foodAndDrugs/drinks",
			"foodAndDrugs/drugs", "foodAndDrugs/fruits", "foodAndDrugs/greenVegetables", "foodAndDrugs/meat",
			"foodAndDrugs/mixedFoods", "foodAndDrugs/ointments", "foodAndDrugs/proteinVegetables",
			"foodAndDrugs/spices", "foodAndDrugs/starches", "minerals", "plants", "liveAnimals", "tools/appliances",
			"tools/audio", "tools/clothing", "tools/computers", "tools/engines", "tools/extractionEquipment",
			"tools/generators", "tools/infrastructure", "tools/lights", "tools/manufacturingEquipment",
			"tools/materials", "tools/medicalEquipment", "tools/photography", "tools/vehicles", "tools/weapons" };

	public Game(Server server, String name, String password, String description, ExternalAvatar avatar,
			Player proposer) {
		this.server = server;
		this.name = name;
		this.password = password;
		this.description = description;
		this.avatar = avatar;
		this.status = GameStatus.PROPOSED;
		this.players = new HashMap<>();
		this.players.put(proposer, null);
		this.playerConsoles = new HashMap<>();
		this.playerConsoles.put(proposer, null);
		proposalDate = new GregorianCalendar();
		server.add(this);
	}

	public Game(Server server, String password, Player proposer) {
		this(server, "Game", password, "Feel free to add a description of your game here.", new ExternalAvatar(),
				proposer);
	}

	public void joinWithConsole(Launch console) {
		if (console != null && console.player != null) {
			playerConsoles.put(console.player, console);
			players.put(console.player, console.playerChar);
		}
	}
	
	public boolean hasConsoles() {
		for (Launch console: playerConsoles.values()) {
			if (console != null) return true;
		}
		return false;
	}

	public void updatePlayerChar(Launch console) {
		if (console != null && console.player != null) {
			players.put(console.player, console.playerChar);
		}
	}

	public Player getPlayerByCharacter(Person character) {
		if (players == null) return null;
		if (character == null)
			return null;
		for (Player player : players.keySet()) {
			if (player == null)
				return null;
			if (Objects.equals(players.get(player), character)) {
				return player;
			}
		}
		return null;
	}

	public int getIngameYear() {
		int dispYear = ingameDate.get(GregorianCalendar.YEAR);
		if (ingameDate.get(GregorianCalendar.ERA) == 0) {
			return 1 - dispYear;
		} else
			return dispYear;
	}

	public void enterStaging() {
		status = GameStatus.STAGING;
		stagingDate = new GregorianCalendar();
	}

	public void launch() {
		if (achieveSafePlayers == null) {
			achieveSafePlayers = Lists.mutable.empty();
			achieveSafePlayers.addAll(players.keySet());
		}
		latestLaunchDate = new GregorianCalendar();
		if (status == GameStatus.PROPOSED || status == GameStatus.STAGING) {
			firstLaunchDate = new GregorianCalendar();
		}
		loadGameRules();
		loadTechnology();
		loadRegions();
		/* loadLocations(); */
		loadCultures();
		loadReligions();
		// loadFamilies();
		loadAgeGroups();
		loadGenders();
		loadSexualities();
		loadFactions();
		loadNames();
		loadIndividuals();
		loadGovernments();
		loadBranches();
		loadOffices();
		for (String res : resourceCategories) {
			loadResources(res);
		}
		loadProfessions();
		loadCastes();
		/*
		 * loadIdeologies(); loadNewsAgencies(); loadSchools(); loadAnimals();
		 * loadDiseases(); loadGods();
		 */
		loadPolicies();
		status = GameStatus.ACTIVE;
	}

	public void pause() {
		status = GameStatus.PAUSED;
		latestPauseDate = new GregorianCalendar();
	}

	public void cancel() {
		status = GameStatus.CANCELED;
		cancellationDate = new GregorianCalendar();
	}

	public void conclude() {
		status = GameStatus.COMPLETE;
		completionDate = new GregorianCalendar();
	}

	public long nextActorId() {
		if (actors == null) {
			actors = Lists.mutable.empty();
			return 0;
		}
		long max = Long.MIN_VALUE;
		for (Actor actor : actors) {
			if (actor.getId() > max) {
				max = actor.getId();
			}
		}
		return max + 1 + (int) (Math.random() * NEXT_ID_BLOCK_SIZE);
	}

	public void loadGameRules() {
		if (gameRules == null) {
			gameRules = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/options/gameMechanicOptions.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(";");
				if (data.length < 5)
					continue;
				int ruleID = Integer.parseInt(data[0]);
				String ruleScope = data[1];
				String ruleName = data[2];
				int defaultRuleOptionID = Integer.parseInt(data[3]);
				String[] ruleOptionStringArray = data[4].split(",");
				MutableList<GameRuleOption> ruleOptionMutableList = Lists.mutable.empty();
				for (int ruleOptionID = 0; ruleOptionID < ruleOptionStringArray.length; ruleOptionID++) {
					String ruleOptionString = ruleOptionStringArray[ruleOptionID];
					boolean achieveSafe = true;
					if (ruleOptionString.startsWith("[x]")) {
						achieveSafe = false;
						ruleOptionString = ruleOptionString.substring(3);
					}
					ruleOptionMutableList.add(new GameRuleOption(null, ruleOptionID, ruleOptionString, achieveSafe));
				}
				GameRule gameRule = new GameRule(this, ruleID, ruleScope, ruleName, ruleOptionMutableList,
						defaultRuleOptionID);
				gameRule.allowAll();
				gameRules.add(gameRule);
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public GameRule getGameRuleById(int gameRuleId) {
		for (GameRule nextRule : gameRules) {
			if (nextRule.getId() == gameRuleId) {
				return nextRule;
			}
		}
		return null;
	}

	public void loadPolicies() {
		if (policies == null) {
			policies = Lists.mutable.empty();
		}
		if (lawCategories == null) {
			lawCategories = new HashMap<String, MutableList<Policy>>();
		}
		String fileName = "src/main/resources/history/policies.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				boolean isNumeric = false;
				boolean isLinear = true;
				String line = scanner.nextLine();
				if (line.isEmpty() || !line.contains(";"))
					break;
				String[] data = line.split(";");
				if (data.length < 4)
					continue;
				long policyID = Long.parseLong(data[0]);
				String policyName = data[1];
				String categoryString = data[2];
				MutableList<String> policyCategories = Lists.mutable.empty();
				for (String cat : categoryString.split(",")) {
					policyCategories.add(cat);
				}
				String optionString = data[3];
				MutableList<String> optionNames = Lists.mutable.empty();
				String[] optionArray = optionString.split(",");
				double min = 0;
				double numericValue = min;
				double max = 100;
				int optionCount = 101;
				if (optionArray[0].equals("NUMERIC")) {
					isNumeric = true;
					String units = "";
					if (optionArray.length >= 2 && !optionArray[1].isEmpty()) {
						isLinear = !optionArray[1].equals("LOG");
					}
					if (optionArray.length >= 3 && !optionArray[2].isEmpty()) {
						min = Double.parseDouble(optionArray[2]);
					}
					if (optionArray.length >= 4 && !optionArray[3].isEmpty()) {
						max = Double.parseDouble(optionArray[3]);
						if (max < min)
							continue;
					}
					if (optionArray.length >= 5 && !optionArray[4].isEmpty()) {
						optionCount = Integer.parseInt(optionArray[4]);
					}
					if (optionArray.length >= 6 && !optionArray[5].isEmpty()) {
						units = " " + optionArray[5];
					}
					if (max == min || optionCount <= 1) {
						optionNames.add(Utilities.estimateNumber(min));
					} else if (isLinear) {
						double step = (max - min) / (optionCount - 1);
						for (int optionIndex = 0; optionIndex < optionCount; optionIndex++) {
							optionNames.add(Utilities.estimateNumber(numericValue) + units);
							numericValue += step;
						}
					} else {
						boolean startsAtZero = false;
						boolean startsAtMinusInfinity = false;
						boolean endsAtZero = false;
						boolean endsAtInfinity = false;
						if (min < SMALL_NUMBER && min > -SMALL_NUMBER) {
							min = SMALL_NUMBER;
							startsAtZero = true;
						}
						if (max < SMALL_NUMBER && max > -SMALL_NUMBER) {
							max = -SMALL_NUMBER;
							endsAtZero = true;
						}
						if (min < -LARGE_NUMBER) {
							min = -LARGE_NUMBER;
							startsAtMinusInfinity = true;
						}
						if (max > LARGE_NUMBER) {
							max = LARGE_NUMBER;
							endsAtInfinity = true;
						}
						double overallRatio = max / min;
						if (overallRatio <= 0)
							continue;
						double stepRatio = Math.pow(overallRatio, 1.0 / optionCount);
						if (startsAtZero) {
							optionNames.add("None");
						} else if (startsAtMinusInfinity) {
							optionNames.add("No Lower Limit");
						}
						numericValue = min;
						for (int optionIndex = 0; optionIndex < optionCount && numericValue <= max; optionIndex++) {
							optionNames.add(Utilities.estimateNumber(numericValue) + units);
							numericValue *= stepRatio;
						}
						if (endsAtZero) {
							optionNames.add("None");
						} else if (endsAtInfinity) {
							optionNames.add("No Upper Limit");
						}
					}
				} else {
					for (String option : optionArray) {
						optionNames.add(option);
					}
				}
				Policy policy = new Policy(this, policyID, policyName, optionNames, policyCategories);
				if (data.length > 4 && !(data[4].isEmpty())) {
					double defaultValue = Double.parseDouble(data[4]);
					double adjDefaultValue = defaultValue;
					if (isNumeric) {
						if (isLinear) {
							adjDefaultValue = (defaultValue - min) / (max - min);
						} else {
							double defaultRatio = defaultValue / min;
							double base = max / min;
							adjDefaultValue = Math.log(defaultRatio) / Math.log(base);
						}
					}
					policy.setDefaultValue(adjDefaultValue);
				}
				if (data.length > 5 && !(data[5].isEmpty())) {
					MutableList<Double> thresholds = Lists.mutable.empty();
					String[] threshArray = data[5].split(",");
					for (int threshCount = 0; threshCount < threshArray.length
							&& threshCount < optionNames.size() - 1; threshCount++) {
						thresholds.add(Double.parseDouble(threshArray[threshCount]));
					}
					policy.setThresholds(thresholds);
				}
				for (Government govt : governments) {
					policy.getCurrentValues().put(govt, policy.getDefaultValue());
				}
				policies.add(policy);
				for (String cat : policy.getCategories()) {
					if (lawCategories.containsKey(cat)) {
						if (!lawCategories.get(cat).contains(policy)) {
							lawCategories.get(cat).add(policy);
						}
					} else {
						MutableList<Policy> policySubMutableList = Lists.mutable.empty();
						policySubMutableList.add(policy);
						lawCategories.put(cat, policySubMutableList);
					}
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public Policy getPolicyByName(String policyName) {
		if (policies == null) {
			policies = Lists.mutable.empty();
			return null;
		}
		for (Policy law : policies) {
			if (law.getName().toLowerCase().equals(policyName.toLowerCase())) {
				return law;
			}
		}
		return null;
	}

	public void loadTechnology() {
		String fileName = "src/main/resources/history/technology.txt";
		File file = new File(fileName);
		if (technologies == null) {
			technologies = Lists.mutable.empty();
		}
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			long nextTechID = 0;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.isEmpty() || !line.contains(";"))
					break;
				String[] data = line.split(";");
				if (data.length < 2)
					continue;
				long currentTechID = nextTechID;
				String techName = data[0];
				int techYear = Integer.parseInt(data[1]);
				Technology tech = new Technology(currentTechID, techName, techYear);
				technologies.add(tech);
				nextTechID++;
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadAstronomy() {
		String fileName = "src/main/resources/history/places/astronomicalBodies.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadRegions() {
		if (regions == null) {
			regions = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/places/regions.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(";");
				if (data.length >= 2) {
					long regionID = Long.parseLong(data[0]);
					String regionName = data[1];
					Region region = new Region(regionID, regionName);
					if (data.length >= 3) {
						String parentString = data[2];
						if (!parentString.isEmpty()) {
							String[] parents = parentString.split(",");
							for (String parent : parents) {
								long parentID = Long.parseLong(parent);
								region.getParents().add(parentID);
							}
						}
					}
					regions.add(region);
				}
			}
			scanner.close();
			for (Region child : regions) {
				if (child.getParents() == null) {
					child.setParents(Lists.mutable.empty());
					continue;
				}
				if (!child.getParents().isEmpty()) {
					for (long parentID : child.getParents()) {
						Region parent = lookupRegionByID(parentID);
						if (parent != null) {
							if (parent.getChildren() == null) {
								parent.setChildren(Lists.mutable.empty());
							}
							if (!parent.getChildren().contains(child.getId())) {
								parent.getChildren().add(child.getId());
							}
						}
					}
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public Region lookupRegionByName(String name) {
		if (regions == null) {
			regions = Lists.mutable.empty();
			return null;
		}
		for (Region reg : regions) {
			if (reg.getName().equalsIgnoreCase(name)) {
				return reg;
			}
		}
		return null;
	}

	public Region lookupRegionByID(long regionID) {
		if (regions == null) {
			regions = Lists.mutable.empty();
			return null;
		}
		for (Region reg : regions) {
			if (reg.getId() == regionID) {
				return reg;
			}
		}
		return null;
	}

	public void loadLocations() {
		String fileName = "src/main/resources/history/places/locations.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadIndividuals() {
		individuals = Lists.mutable.empty();
		livingPeople = Lists.mutable.empty();
		String fileName = "src/main/resources/history/actors/individuals.txt";
		File file = new File(fileName);
		int gameYear = this.ingameDate.get(GregorianCalendar.YEAR);
		int gameMonth = this.ingameDate.get(GregorianCalendar.MONTH) + 1;
		int gameDate = this.ingameDate.get(GregorianCalendar.DAY_OF_MONTH);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 11) {
					// [0]ID;[1]CanonicalName;[2]GivenName;[3]ShortName;
					// [4]FamilyName;[5]PortraitPath;[6]BirthYear;[7]BirthMonth;
					// [8]BirthDate;[9]Culture;[10]Religion;[11]Gender(Identity);
					// [12]Factions;[13]DeathYear;[14]DeathMonth;[15]DeathDay
					int id = Integer.parseInt(data[0]);
					String canonicalName = data[1];
					Person person = new Person(this, canonicalName);
					person.setId(id);
					person.setCanonicalName(canonicalName);
					person.setFullName(canonicalName);
					person.setGivenName(data[2]);
					person.setShortName(data[3]);
					person.setFamilyName(data[4]);
					person.setPortraitPath(data[5]);
					int birthYear = Integer.parseInt(data[6]);
					int birthMonth = Integer.parseInt(data[7]);
					int birthDate = Integer.parseInt(data[8]);
					if (birthYear > gameYear)
						continue;
					if (birthYear == gameYear) {
						if (birthMonth > gameMonth)
							continue;
						if (birthMonth == gameMonth) {
							if (birthDate > gameDate)
								continue;
						}
					}
					String[] personCultures = data[9].split(",");
					for (String cultureString : personCultures) {
						int cultureID = Integer.parseInt(cultureString);
						Culture culture = (Culture) lookupActorByID(cultureID);
						if (culture != null) {
							person.getCultures().add(culture);
						}
					}
					String religionString = data[10];
					if (religionString.matches("[\\d]+")) {
						int relID = Integer.parseInt(religionString);
						Religion rel = (Religion) lookupActorByID(relID + 1_000_000);
						if (rel != null) {
							person.getReligions().add(rel);
						}
					}
					person.setDateOfBirth(new GregorianCalendar(birthYear, birthMonth - 1, birthDate));
					double genderNumber = Double.parseDouble(data[11]);
					Gender gender = new Gender(this, genderNumber, genderNumber, genderNumber, 1, 1 - genderNumber,
							genderNumber, 1 - genderNumber, 0);
					person.setGender(gender);
					// [0]ID;[1]CanonicalName;[2]GivenName;[3]ShortName;
					// [4]FamilyName;[5]PortraitPath;[6]BirthYear;[7]BirthMonth;
					// [8]BirthDate;[9]Culture;[10]Religion;[11]Gender(Identity);
					// [12]Factions;[13]DeathYear;[14]DeathMonth;[15]DeathDay
					Culture namingCulture = nameHandler.getNamingCulture(person.getCultures());
					String namingRule = namingCulture.getNameVarRule();
					if (namingRule.isEmpty()) {
						namingRule = Math.random() < 0.5 ? "gs" : "sg";
					}
					String customSurname = nameHandler.transformName(person.getFamilyName(), namingRule, person, true);
					person.setCustomFamilyName(customSurname);
					if (data.length >= 13 && !data[12].isEmpty()) {
						long factionID = Long.parseLong(data[12]);
						for (Faction fac : factions) {
							if (fac.getId() == factionID) {
								person.getParties().add(fac);
							}
						}
					}
					if (data.length >= 16) {
						int deathYear = Integer.parseInt(data[13]);
						int deathMonth = Integer.parseInt(data[14]);
						int deathDate = Integer.parseInt(data[15]);
						person.setDateOfDeath(new GregorianCalendar(deathYear, deathMonth - 1, deathDate));
						if (ingameDate.compareTo(person.getDateOfDeath()) < 0) {
							person.setDateOfDeath(null);
						}
					}
					if (data.length >= 17 && !data[16].isEmpty()) {
						String[] parentIdList = data[16].split(",");
						if (person.getLegalParents() == null) {
							person.setLegalParents(Lists.mutable.empty());
						}
						for (String parentId: parentIdList) {
							if (parentId.matches("[\\d]+")) {
								person.getLegalParents().add(Long.parseLong(parentId));
							}
						}
					}
					actors.add(person);
					individuals.add(person);
				}
			}
			for (Person person: individuals) {
				for (Long parentId: person.getLegalParents()) {
					Actor actor = lookupActorByID(parentId);
					if (actor instanceof Person) {
						Person parent = (Person) actor;
						if (!parent.getLegalChildren().contains(person.getId())) {
							parent.getLegalChildren().add(person.getId());
						}
					}
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadFamilies() {
		String fileName = "src/main/resources/history/actors/peopleGroups/families.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	/**
	 * This method must run before all the other loadABC methods where ABC is a
	 * subclass of PeopleGroup.
	 */
	public void loadCultures() {
		String fileName = "src/main/resources/history/actors/peopleGroups/cultures.txt";
		File file = new File(fileName);
		if (cultures == null) {
			cultures = Lists.mutable.empty();
		}
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.isEmpty()) {
					break;
				}
				String[] data = line.split(";");
				if (data.length >= 3) {
					Culture culture = new Culture(this);
					culture.setId(Long.parseLong(data[0]));
					culture.setExonym(data[1]);
					String adj = data[2];
					if (adj.isEmpty()) {
						adj = data[1];
					} else if (adj.equals("s") && adj.endsWith("s")) {
						adj = data[1].substring(0, data[1].length() - 1);
					}
					culture.setAdjective(adj);
					culture.setSingularAutonym(adj);
					culture.setSingularExonym(adj);
					if (data.length >= 4) {
						String groupString = data[3];
						if (!groupString.isEmpty()) {
							String[] groups = groupString.split(",");
							for (String group : groups) {
								long parentID = Long.parseLong(group);
								culture.getSupergroups().add(parentID);
								Actor parentActor = lookupActorByID(parentID);
								if (parentActor != null && parentActor instanceof PeopleGroup) {
									PeopleGroup parentGroup = (PeopleGroup) parentActor;
									if (parentGroup.getSubgroups() != null
											&& !parentGroup.getSubgroups().contains(culture.getId())) {
										parentGroup.getSubgroups().add(culture.getId());
									}
								}
							}
						}
					}
					if (data.length >= 5) {
						culture.setNameVarRule(data[4]);
					}
					cultures.add(culture);
					actors.add(culture);
				}
			}
			scanner.close();
			for (Culture culture : cultures) {
				for (long parentID : culture.getSupergroups()) {
					Actor parentActor = lookupActorByID(parentID);
					if (parentActor != null && parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup.getSubgroups() != null
								&& !parentGroup.getSubgroups().contains(culture.getId())) {
							parentGroup.getSubgroups().add(culture.getId());
						}
					}
				}
			}
			PeopleGroup people = (PeopleGroup) lookupActorByID(50000);
			people.updateUltimates();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public Culture getCultureByName(String input) {
		if (cultures == null) {
			cultures = Lists.mutable.empty();
		}
		for (Culture cul : cultures) {
			if (cul == null || cul.getExonym() == null || cul.getAdjective() == null) {
				continue;
			}
			if (cul.getExonym().equalsIgnoreCase(input) || cul.getAdjective().equalsIgnoreCase(input)) {
				return cul;
			}
		}
		return null;
	}

	public void loadLanguages() {
		String fileName = "src/main/resources/history/actors/peopleGroups/languages.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadReligions() {
		if (religions == null) {
			religions = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/religions.txt";
		File file = new File(fileName);
		religions = Lists.mutable.empty();
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.isEmpty() || !line.contains(";"))
					continue;
				String[] data = line.split(";");
				if (data.length < 3)
					continue;
				Integer relID = Integer.parseInt(data[0]) + 1_000_000;
				String relName = data[1];
				String relAdj = data[2];
				Religion rel = new Religion(this);
				rel.setId(relID);
				rel.setName(relName);
				rel.setExonym(relName);
				rel.setAutonym(relName);
				if (relAdj.isEmpty()) {
					rel.setAdjective(relName);
				} else if (relAdj.equals("s")) {
					rel.setAdjective(relName.substring(0, relName.length() - 1));
				} else {
					rel.setAdjective(relAdj);
				}
				rel.setSingularAutonym(rel.getAdjective());
				rel.setSingularExonym(rel.getAdjective());
				rel.setSupergroups(Lists.mutable.empty());
				if (data.length >= 4 && !data[3].isEmpty()) {
					String[] parentStringArray = data[3].split(",");
					for (String parentString : parentStringArray) {
						long parentID = Integer.parseInt(parentString) + 1_000_000;
						rel.getSupergroups().add(parentID);
					}
				} else {
					rel.getSupergroups().add(50000L);
				}
				if (data.length >= 5 && !data[4].isEmpty()) {
					int foundingYear = Integer.parseInt(data[4]);
					if (foundingYear > getIngameYear())
						continue;
					rel.setFoundingDate(new GregorianCalendar(foundingYear, 0, 1));
				} else {
					rel.setFoundingDate(new GregorianCalendar(EARLIEST_YEAR, 0, 1));
				}
				religions.add(rel);
				actors.add(rel);
			}
			for (Religion rel : religions) {
				if (rel.getSubgroups() == null) {
					rel.setSubgroups(Lists.mutable.empty());
				}
				for (long parentID : rel.getSupergroups()) {
					Actor parentActor = this.lookupActorByID(parentID);
					if (parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup != null) {
							if (parentGroup.getSubgroups() == null) {
								parentGroup.setSubgroups(Lists.mutable.empty());
							}
							if (!parentGroup.getSubgroups().contains(rel.getId())) {
								parentGroup.getSubgroups().add(rel.getId());
							}
						}
					}
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadAgeGroups() {
		if (ages == null) {
			ages = Lists.mutable.empty();
		}
		int lastThreshold = 120;
		int groupSize = 10;
		int topLevelGroups = 1 + (lastThreshold / groupSize);
		for (int index = 0; index < topLevelGroups; index++) {
			int lower = index * groupSize;
			int upper = (index < topLevelGroups - 1) ? index * groupSize + groupSize - 1 : Integer.MAX_VALUE;
			AgeGroup ageGroup = new AgeGroup(this, lower, upper);
			ageGroup.setId(950_000 + index);
			ageGroup.getSupergroups().add(50_000L);
			if (index == topLevelGroups - 1) {
				ageGroup.setAdjective(String.format("%s+ Year Old", lower, upper));
			} else {
				ageGroup.setAdjective(String.format("%s to %s Year Old", lower, upper));
			}
			ageGroup.setExonym(ageGroup.getAdjective() + "s");
			ageGroup.setAutonym(ageGroup.getExonym());
			ageGroup.setSingularExonym(ageGroup.getAdjective());
			ageGroup.setSingularAutonym(ageGroup.getAdjective());
			ages.add(ageGroup);
			actors.add(ageGroup);
		}
		for (int single = 0; single < lastThreshold; single++) {
			AgeGroup ageGroup = new AgeGroup(this, single, single);
			ageGroup.setId(950_000 + topLevelGroups + single);
			long largerGroup = 950_000 + (single / groupSize);
			ageGroup.getSupergroups().add(largerGroup);
			ageGroup.setAdjective(String.format("%s Year Old", single));
			ageGroup.setExonym(ageGroup.getAdjective() + "s");
			ageGroup.setAutonym(ageGroup.getExonym());
			ageGroup.setSingularExonym(ageGroup.getAdjective());
			ageGroup.setSingularAutonym(ageGroup.getAdjective());
			ages.add(ageGroup);
			actors.add(ageGroup);
		}
		for (AgeGroup subgroup : ages) {
			if (subgroup.getSupergroups() == null || subgroup.getSupergroups().isEmpty()) {
				subgroup.setSupergroups(Lists.mutable.empty());
				subgroup.getSupergroups().add(50_000L);
			}
			for (long parentID : subgroup.getSupergroups()) {
				Actor parentActor = lookupActorByID(parentID);
				if (parentActor != null && parentActor instanceof PeopleGroup) {
					PeopleGroup parentGroup = (PeopleGroup) parentActor;
					if (parentGroup.getSubgroups() != null && !parentGroup.getSubgroups().contains(subgroup.getId())) {
						parentGroup.getSubgroups().add(subgroup.getId());
					}
				}
			}
		}
	}

	public Religion getReligionByName(String input) {
		if (religions == null) {
			religions = Lists.mutable.empty();
		}
		for (Religion rel : religions) {
			if (rel == null || rel.getAutonym() == null) {
				continue;
			}
			if (rel.getAutonym().equalsIgnoreCase(input)) {
				return rel;
			}
		}
		return null;
	}

	public void loadGenders() {
		if (genders == null) {
			genders = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/genders.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length < 8)
					continue;
				long genderId = 500_000 + Long.parseLong(data[0]);
				double genderNumber = Double.parseDouble(data[5]);
				double assignedGender = Double.parseDouble(data[6]);
				double apparentGender = Double.parseDouble(data[7]);
				Gender gender = new Gender(this, genderNumber, assignedGender, apparentGender, -1, -1);
				gender.setId(genderId);
				gender.setName(data[1]);
				gender.setAutonym(data[1]);
				gender.setExonym(data[1]);
				String singularString = data[2];
				if (singularString.isEmpty())
					singularString = data[1];
				else if (singularString.equals("s")) {
					singularString = data[1].substring(0, data[1].length() - 1);
				}
				gender.setSingularAutonym(singularString);
				gender.setSingularExonym(singularString);
				String pluralAdj = data[3];
				String singAdj = data[4];
				if (singAdj.isEmpty()) {
					singAdj = pluralAdj;
				}
				gender.setAdjective(singAdj);
				gender.setPluralAdjective(pluralAdj);
				if (gender.getSupergroups() == null) {
					gender.setSupergroups(Lists.mutable.empty());
				}
				if (data.length >= 9 && !(data[8].trim().isEmpty())) {
					String[] parentArray = data[8].split(",");
					for (String parentString : parentArray) {
						long parentId = 500_000L + Long.parseLong(parentString);
						gender.getSupergroups().add(parentId);
					}
				}
				genders.add(gender);
				actors.add(gender);
			}
			scanner.close();
			for (Gender subgroup : genders) {
				if (subgroup.getSupergroups() == null || subgroup.getSupergroups().isEmpty()) {
					subgroup.setSupergroups(Lists.mutable.empty());
					subgroup.getSupergroups().add(50_000L);
				}
				for (long parentID : subgroup.getSupergroups()) {
					Actor parentActor = lookupActorByID(parentID);
					if (parentActor != null && parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup.getSubgroups() != null
								&& !parentGroup.getSubgroups().contains(subgroup.getId())) {
							parentGroup.getSubgroups().add(subgroup.getId());
						}
					}
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadSexualities() {
		if (sexualities == null) {
			sexualities = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/sexualities.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length < 6)
					continue;
				long sexualityId = 900_000 + Long.parseLong(data[0]);
				double genderNumber = Double.parseDouble(data[5]);
				double asexualNumber = Double.parseDouble(data[6]);
				Sexuality sexuality = new Sexuality(this, genderNumber, asexualNumber);
				sexuality.setId(sexualityId);
				sexuality.setName(data[1]);
				sexuality.setAutonym(data[1]);
				sexuality.setExonym(data[1]);
				String singularString = data[2];
				if (singularString.isEmpty())
					singularString = data[1];
				else if (singularString.equals("s")) {
					singularString = data[1].substring(0, data[1].length() - 1);
				}
				sexuality.setSingularAutonym(singularString);
				sexuality.setSingularExonym(singularString);
				String pluralAdj = data[3];
				String singAdj = data[4];
				if (singAdj.isEmpty()) {
					singAdj = pluralAdj;
				}
				sexuality.setAdjective(singAdj);
				sexuality.setPluralAdjective(pluralAdj);
				if (sexuality.getSupergroups() == null) {
					sexuality.setSupergroups(Lists.mutable.empty());
				}
				PeopleGroup everyone = (PeopleGroup) lookupActorByID(50_000L);
				if (sexuality.getSupergroups() == null) {
					sexuality.setSupergroups(Lists.mutable.empty());
				}
				if (sexuality.getSupergroups().isEmpty()) {
					sexuality.getSupergroups().add(50_000L);
					if (everyone != null) {
						everyone.getSubgroups().add(sexuality.getId());
					}
				}
				sexualities.add(sexuality);
				actors.add(sexuality);
			}
			scanner.close();
			for (Sexuality subgroup : sexualities) {
				if (subgroup.getSupergroups() == null || subgroup.getSupergroups().isEmpty()) {
					subgroup.setSupergroups(Lists.mutable.empty());
					subgroup.getSupergroups().add(50_000L);
				}
				for (long parentID : subgroup.getSupergroups()) {
					Actor parentActor = lookupActorByID(parentID);
					if (parentActor != null && parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup.getSubgroups() != null
								&& !parentGroup.getSubgroups().contains(subgroup.getId())) {
							parentGroup.getSubgroups().add(subgroup.getId());
						}
					}
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadGovernments() {
		String fileName = "src/main/resources/history/actors/peopleGroups/governments.txt";
		File file = new File(fileName);
		if (governments == null) {
			governments = Lists.mutable.empty();
		}
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 5) {
					Government govt = new Government(this);
					long id = Long.parseLong(data[0]);
					govt.setId(id);
					govt.setFullName(data[1]);
					govt.setShortName(data[2]);
					govt.setAdjective(data[3]);
					govt.setPortraitPath(data[4]);
					govt.setPopulation(Long.parseLong(data[5]));
					governments.add(govt);
					actors.add(govt);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public Government getGovernmentByName(String input) {
		if (governments == null) {
			governments = Lists.mutable.empty();
		}
		for (Government govt : governments) {
			if (govt.getFullName().equalsIgnoreCase(input) || govt.getShortName().equalsIgnoreCase(input)) {
				return govt;
			}
		}
		return null;
	}

	public void loadIdeologies() {
		String fileName = "src/main/resources/history/actors/peopleGroups/ideologies.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadFactions() {
		String fileName = "src/main/resources/history/actors/peopleGroups/factions.txt";
		File file = new File(fileName);
		if (factions == null) {
			factions = Lists.mutable.empty();
		}
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 3) {
					Faction faction = new Faction(this);
					faction.setId(Long.parseLong(data[0]));
					faction.setFullName(data[1]);
					faction.setShortName(data[2]);
					factions.add(faction);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadBranches() {
		if (branches == null) {
			branches = Lists.mutable.empty();
		}
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/branches.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 4) {
					Branch branch = new Branch(this);
					branch.setId(Long.parseLong(data[0]));
					branch.setFullName(data[1]);
					branch.setShortName(data[2]);
					long govtID = Long.parseLong(data[3]);
					for (Government govt : governments) {
						if (govt.getId() == govtID) {
							branch.getGovernments().add(govt);
							govt.getBranches().add(branch);
						}
					}
					branches.add(branch);
					actors.add(branch);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadCastes() {
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		if (castes == null) {
			castes = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/castes.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(";");
				if (data.length < 4)
					continue;
				long casteId = 1_500_000L + Long.parseLong(data[0]);
				String castePlural = data[1];
				String casteSing = data[2];
				if (casteSing.equals("s")) {
					casteSing = castePlural.substring(0, castePlural.length() - 1);
				} else if (casteSing.isEmpty()) {
					casteSing = castePlural;
				}
				String casteAdj = data[3];
				Caste caste = new Caste(this);
				caste.setId(casteId);
				caste.setAutonym(castePlural);
				caste.setExonym(castePlural);
				caste.setName(castePlural);
				caste.setSingularAutonym(casteSing);
				caste.setSingularExonym(casteSing);
				caste.setAdjective(casteAdj);
				if (data.length >= 5 && !(data[4].trim().isEmpty())) {
					String[] parentArray = data[4].split(",");
					for (String parentString : parentArray) {
						long parentId = 1_500_000L + Long.parseLong(parentString);
						caste.getSupergroups().add(parentId);
					}
				}
				castes.add(caste);
				actors.add(caste);
			}
			scanner.close();
			long highestId = 0;
			for (Caste caste : castes) {
				if (caste.getId() > highestId) {
					highestId = caste.getId();
				}
			}
			for (Caste caste : castes) {
				if (caste.getSupergroups() == null || caste.getSupergroups().isEmpty()) {
					caste.setSupergroups(Lists.mutable.empty());
					caste.getSupergroups().add(50_000L);
				}
				for (long parentID : caste.getSupergroups()) {
					Actor parentActor = lookupActorByID(parentID);
					if (parentActor != null && parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup.getSubgroups() != null && !parentGroup.getSubgroups().contains(caste.getId())) {
							parentGroup.getSubgroups().add(caste.getId());
						}
					}
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadProfessions() {
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		if (professions == null) {
			professions = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/peopleGroups/professions.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(";");
				if (data.length < 3)
					continue;
				long profId = 2_000_000L + Long.parseLong(data[0]);
				String profPlural = data[1];
				String profSing = data[2];
				if (profSing.equals("s")) {
					profSing = profPlural.substring(0, profPlural.length() - 1);
				} else if (profSing.isEmpty()) {
					profSing = profPlural;
				}
				Profession prof = new Profession(this);
				prof.setId(profId);
				prof.setAutonym(profPlural);
				prof.setExonym(profPlural);
				prof.setName(profPlural);
				prof.setAdjective(profSing);
				prof.setSingularAutonym(profSing);
				prof.setSingularExonym(profSing);
				if (data.length >= 4 && !(data[3].trim().isEmpty())) {
					String[] parentArray = data[3].split(",");
					for (String parentString : parentArray) {
						long parentId = 2_000_000L + Long.parseLong(parentString);
						prof.getSupergroups().add(parentId);
					}
				}
				professions.add(prof);
				actors.add(prof);
			}
			scanner.close();
			long highestId = 0;
			for (Profession prof : professions) {
				if (prof.getId() > highestId) {
					highestId = prof.getId();
				}
			}
			for (Profession prof : professions) {
				if (prof.getSupergroups() == null || prof.getSupergroups().isEmpty()) {
					prof.setSupergroups(Lists.mutable.empty());
					prof.getSupergroups().add(50_000L);
				}
				for (long parentID : prof.getSupergroups()) {
					Actor parentActor = lookupActorByID(parentID);
					if (parentActor != null && parentActor instanceof PeopleGroup) {
						PeopleGroup parentGroup = (PeopleGroup) parentActor;
						if (parentGroup.getSubgroups() != null && !parentGroup.getSubgroups().contains(prof.getId())) {
							parentGroup.getSubgroups().add(prof.getId());
						}
					}
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadOrganizations() {
		String fileName = "src/main/resources/history/actors/peopleGroups/nonGovernmentalOrganizations.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadNewsAgencies() {
		String fileName = "src/main/resources/history/actors/peopleGroups/newsAgencies.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadSchools() {
		String fileName = "src/main/resources/history/actors/peopleGroups/schools.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadOffices() {
		if (offices == null) {
			offices = Lists.mutable.empty();
		}
		if (actors == null) {
			actors = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/actors/offices.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 5) {
					Office office = new Office(this);
					office.setId(Long.parseLong(data[0]));
					office.setFullName(data[1]);
					office.setShortName(data[2]);
					long branchID = Long.parseLong(data[3]);
					for (Branch branch : branches) {
						if (branch.getId() == branchID) {
							branch.getLeadershipOffices().add(office);
						}
					}
					long personID = Long.parseLong(data[4]);
					for (Actor actor : actors) {
						if (actor instanceof Person) {
							Person person = (Person) actor;
							if (person.getId() == personID) {
								office.getIncumbents().add(person);
								person.getOffices().add(office);
							}
						}
					}
					if (data.length >= 9) {
						office.setFeminineFullName(data[5]);
						office.setFeminineShortName(data[6]);
						office.setMasculineFullName(data[7]);
						office.setMasculineShortName(data[8]);
					}
					offices.add(office);
					actors.add(office);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadResources(String category) {
		if (resources == null) {
			resources = Lists.mutable.empty();
		}
		String fileName = "src/main/resources/history/resources/" + category + ".txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 1) {
					String resName = data[0];
					Resource res = new Resource();
					resName = Utilities.unCamelCase(resName);
					resName = Utilities.titleCase(resName);
					res.setName(resName);
					String catName = Utilities.plainPathName(category);
					catName = Utilities.unCamelCase(catName);
					catName = Utilities.titleCase(catName);
					res.setCategory(catName);
					resources.add(res);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public Resource getResourceByName(String resName) {
		if (resources == null) {
			resources = Lists.mutable.empty();
			return null;
		}
		for (Resource res : resources) {
			if (res.getName().equalsIgnoreCase(resName)) {
				return res;
			}
		}
		return null;
	}

	public void loadAnimals() {
		String fileName = "src/main/resources/history/actors/animals.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadDiseases() {
		String fileName = "src/main/resources/history/actors/diseases.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadGods() {
		String fileName = "src/main/resources/history/actors/godsAndSpirits.txt";
		File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", fileName);
		}
	}

	public void loadNames() {
		nameLists = new HashMap<>();
		File nameListFolder = new File("src/main/resources/history/actors/nameLists");
		if (nameListFolder == null || !nameListFolder.isDirectory())
			return;
		File[] nameListFiles = nameListFolder.listFiles();
		if (nameListFiles == null)
			return;
		for (File nextNameFile : nameListFiles) {
			int offset = -1;
			MutableList<MutableList<String>> listOfLists = Lists.mutable.empty();
			for (int i = 0; i < NAME_LIST_NUMBER; i++) {
				listOfLists.add(Lists.mutable.empty());
			}
			try {
				Scanner scanner = new Scanner(nextNameFile, "utf-8");
				if (!scanner.hasNextLine()) {
					System.out.printf("There was a problem reading the file %s.\n", nextNameFile);
				}
				String idString = scanner.nextLine().trim();
				int nameListId = Integer.parseInt(idString);
				int baseIndex = nameListId * NAME_LIST_BLOCK_SIZE;
				while (scanner.hasNextLine()) {
					String nameLine = scanner.nextLine();
					if (nameLine.isEmpty())
						continue;
					if (nameLine.charAt(0) == '#') {
						if (nameLine.charAt(1) == '#' || offset >= NAME_LIST_NUMBER) {
							break;
						} else {
							offset++;
						}
					} else {
						if (offset >= NAME_LIST_NUMBER) break;
						String nextName = nameLine.trim();
						listOfLists.get(offset).add(nextName);
					}
				}
				scanner.close();
				for (int i = 0; i < NAME_LIST_NUMBER; i++) {
					nameLists.put(baseIndex + i, listOfLists.get(i));
				}
			} catch (FileNotFoundException ex) {
				System.out.printf("A problem has occurred. The file %s was not found.", nextNameFile.getName());
			}
		}
	}

	public Actor lookupActorByID(long actorID) {
		if (actors == null) {
			actors = Lists.mutable.empty();
			return null;
		}
		for (Actor a : actors) {
			if (a.getId() == actorID) {
				return a;
			}
		}
		return null;
	}

	public Actor lookupActorByName(String input) {
		for (Actor a : actors) {
			if (a == null)
				continue;
			if (a instanceof Person) {
				Person p = (Person) a;
				if (p.getFullName() == null)
					continue;
				if (p.getFullName().equalsIgnoreCase(input) || p.getShortName().equalsIgnoreCase(input)
						|| p.getFamilyName().equalsIgnoreCase(input)) {
					return p;
				}
			} else if (a instanceof Government) {
				Government g = (Government) a;
				if (g.getFullName() == null)
					continue;
				if (g.getFullName().equalsIgnoreCase(input) || g.getShortName().equalsIgnoreCase(input)) {
					return g;
				}
			} else if (a instanceof PeopleGroup) {
				PeopleGroup group = (PeopleGroup) a;
				if (group.getExonym() == null)
					continue;
				if (group.getExonym().equalsIgnoreCase(input) || group.getAdjective().equalsIgnoreCase(input)) {
					return group;
				}
			} else if (a.getName() != null && a.getName().equalsIgnoreCase(input)) {
				return a;
			}
		}
		return null;
	}

	public void updateAchieveSafePlayer(Player player) {
		boolean soFarSoGood = true;
		for (GameRule gameRule : gameRules) {
			if (!gameRule.getForPlayer(player).isAchieveSafe()) {
				soFarSoGood = false;
				break;
			}
		}
		if (soFarSoGood) {
			achieveSafePlayers.add(player);
		} else {
			achieveSafePlayers.remove(player);
		}
	}

	public void updateAchieveSafeAll() {
		for (Player player : players.keySet()) {
			updateAchieveSafePlayer(player);
		}
	}

	public boolean isEveryone(MutableList<PeopleGroup> groupList) {
		for (PeopleGroup nextGroup : groupList) {
			if (nextGroup.getId() != 50_000) {
				return false;
			}
		}
		return true;
	}

	public boolean hasSingleCriterion(MutableList<PeopleGroup> groupList) {
		int componentCount = 0;
		for (PeopleGroup nextGroup : groupList) {
			if (nextGroup.getId() != 50_000) {
				componentCount++;
			}
		}
		return componentCount == 1;
	}

	public boolean isSecondLevel(MutableList<PeopleGroup> groupList) {
		if (!hasSingleCriterion(groupList))
			return false;
		if (isEveryone(groupList))
			return false;
		PeopleGroup single = getSingleGroup(groupList);
		if (single.getSupergroups().size() != 1)
			return false;
		for (long parentId : single.getSupergroups()) {
			if (parentId != 50_000)
				return false;
		}
		return true;
	}

	public PeopleGroup getSingleGroup(MutableList<PeopleGroup> groupList) {
		for (PeopleGroup nextGroup : groupList) {
			if (nextGroup.getId() != 50_000) {
				return nextGroup;
			}
		}
		return (PeopleGroup) this.lookupActorByID(50_000);
	}

	public MutableList<PeopleGroup> getShortList(MutableList<PeopleGroup> groupList) {
		MutableList<PeopleGroup> shortList = Lists.mutable.empty();
		for (int groupIndex = 0; groupIndex < groupList.size(); groupIndex++) {
			PeopleGroup nextGroup = groupList.get(groupIndex);
			if (nextGroup.getId() != 50_000) {
				shortList.add(nextGroup);
			}
		}
		return shortList;
	}

	public String getCompoundName(MutableList<PeopleGroup> groupList) {
		StringBuffer output = new StringBuffer();
		MutableList<PeopleGroup> shortList = getShortList(groupList);
		if (shortList.isEmpty())
			return "People";
		for (int groupIndex = 0; groupIndex < shortList.size(); groupIndex++) {
			PeopleGroup nextGroup = shortList.get(groupIndex);
			if (groupIndex < shortList.size() - 1) {
				output.append(nextGroup.getPluralAdjective() + " ");
			} else {
				output.append(nextGroup.getExonym());
			}
		}
		return output.toString();
	}

	public MutableList<Long> getSupergroupsFromList(MutableList<PeopleGroup> groupList) {
		if (isEveryone(groupList)) {
			return Lists.mutable.empty();
		} else if (hasSingleCriterion(groupList)) {
			return getSingleGroup(groupList).getSupergroups();
		}
		MutableList<Long> combinedSupergroups = Lists.mutable.empty();
		MutableList<PeopleGroup> shortList = getShortList(groupList);
		for (PeopleGroup shortListGroup : shortList) {
			combinedSupergroups.add(shortListGroup.getId());
		}
		return combinedSupergroups;
	}

	public int getGroupClassNameIndex(PeopleGroup peopleGroup) {
		String currentClassName = peopleGroup.getClass().getSimpleName();
		for (int classNameIndex = 0; classNameIndex < groupClassNames.length; classNameIndex++) {
			if (groupClassNames[classNameIndex].equals(currentClassName)) {
				return classNameIndex;
			}
		}
		return -1;
	}

	public MutableList<Long> groupToIdList(PeopleGroup peopleGroup) {
		MutableList<Long> groupIdList = Lists.mutable.empty();
		Arrays.asList(groupClassNames).forEach(groupClassName -> groupIdList.add(50_000L));
		int classNameIndex = getGroupClassNameIndex(peopleGroup);
		if (classNameIndex >= 0 && classNameIndex < groupClassNames.length) {
			groupIdList.set(classNameIndex, peopleGroup.getId());
		}
		return groupIdList;
	}

	public MutableList<PeopleGroup> singleGroupToGroupList(PeopleGroup inputGroup) {
		MutableList<PeopleGroup> newGroupList = Lists.mutable.empty();
		int notableIndex = getGroupClassNameIndex(inputGroup);
		for (int classNameIndex = 0; classNameIndex < groupClassNames.length; classNameIndex++) {
			if (classNameIndex == notableIndex) {
				newGroupList.add(inputGroup);
			} else {
				newGroupList.add((PeopleGroup) lookupActorByID(50_000L));
			}
		}
		return newGroupList;
	}

	public MutableList<Long> getSubgroupAsNumberList(MutableList<PeopleGroup> parentList, PeopleGroup child,
			int classNameIndex) {
		MutableList<Long> groupIdList = Lists.mutable.empty();
		for (int nextClassIndex = 0; nextClassIndex < groupClassNames.length; nextClassIndex++) {
			if (nextClassIndex == classNameIndex) {
				groupIdList.add(child.getId());
			} else {
				groupIdList.add(parentList.get(nextClassIndex).getId());
			}
		}
		return groupIdList;
	}

	public MutableList<PeopleGroup> getSubgroupAsGroupList(MutableList<PeopleGroup> parentList, PeopleGroup child,
			int classNameIndex) {
		MutableList<PeopleGroup> groupList = Lists.mutable.empty();
		for (int nextClassIndex = 0; nextClassIndex < groupClassNames.length; nextClassIndex++) {
			if (nextClassIndex == classNameIndex) {
				groupList.add(child);
			} else {
				groupList.add(parentList.get(nextClassIndex));
			}
		}
		return groupList;
	}

	public MutableList<MutableList<Long>> getSubgroupsFromList(MutableList<PeopleGroup> groupList) {
		MutableList<MutableList<Long>> combinedSubgroups = Lists.mutable.empty();
		if (isEveryone(groupList)) {
			PeopleGroup everyone = (PeopleGroup) lookupActorByID(50_000);
			for (long childId : everyone.getSubgroups()) {
				Actor nextActor = lookupActorByID(childId);
				if (nextActor == null || !(nextActor instanceof PeopleGroup))
					continue;
				PeopleGroup nextGroup = (PeopleGroup) nextActor;
				combinedSubgroups.add(groupToIdList(nextGroup));
			}
		} else {
			for (int groupClassIndex = 0; groupClassIndex < groupList.size(); groupClassIndex++) {
				PeopleGroup nextGroup = groupList.get(groupClassIndex);
				MutableList<Long> nextSubgroupMutableList = nextGroup.getSubgroups();
				for (long childId : nextSubgroupMutableList) {
					Actor nextActor = lookupActorByID(childId);
					if (nextActor == null || !(nextActor instanceof PeopleGroup))
						continue;
					PeopleGroup nextSubgroup = (PeopleGroup) nextActor;
					if (getGroupClassNameIndex(nextSubgroup) != groupClassIndex)
						continue;
					combinedSubgroups.add(getSubgroupAsNumberList(groupList, nextSubgroup, groupClassIndex));
				}
			}
		}
		return combinedSubgroups;
	}

	public long getPopulationFromList(MutableList<PeopleGroup> groupList) {
		if (isEveryone(groupList) || hasSingleCriterion(groupList)) {
			return getSingleGroup(groupList).getPopulation();
		} else {
			// TODO: Population
			return 0;
		}
	}

	public void handleDecision(Decision decision, Launch launch, int selectedIndex) {
		launch.closeNextPopup();
		if (decision == null)
			return;
		switch (decision.getInternalName()) {
		case "player_char_death": {
			int deathRuleMutableListString = getGameRuleById(0).getForPlayer(launch.player).getId();
			if (deathRuleMutableListString == 3 && !livingPeople.isEmpty()) {
				String eventTitle = "The Next Chapter";
				String eventText = "Choose who you would like to play as next.";
				MutableList<String> choices = Lists.mutable.empty();
				livingPeople.forEach(p -> choices.add(p.getFullName()));
				Decision chooseCharDec = new Decision(this, "choose_new_char", eventTitle, eventText,
						Lists.immutable.empty(), choices, Lists.mutable.empty(), Lists.mutable.empty(), 0,
						Double.POSITIVE_INFINITY, true);
				Popup deathPopup = new Popup(launch, chooseCharDec);
				launch.popupWindow = deathPopup.display();
				launch.ingameCanvas.getChildren().add(launch.popupWindow);
			} else if (livingPeople.isEmpty() || launch.nextPlayerChar == null) {
				updatePlayerChar(launch);
				System.exit(0);
			} else {
				launch.playerChar = launch.nextPlayerChar;
				updatePlayerChar(launch);
				launch.nextPlayerChar = null;
				launch.playerCharDeathEventFired = false;
				launch.viewActor(launch.playerChar.getId());
			}
			break;
		}
		case "choose_new_char": {
			launch.playerChar = livingPeople.get(selectedIndex);
			updatePlayerChar(launch);
			launch.nextPlayerChar = null;
			launch.playerCharDeathEventFired = false;
			launch.viewActor(launch.playerChar.getId());
			launch.closeNextPopup();
			break;
		}
		case "player_has_child": {
			if (decision.getParticipants() == null || decision.getInputStrings() == null)
				return;
			for (int i = 0; i < decision.getParticipants().size() && i < decision.getInputStrings().size(); i++) {
				Object participant = decision.getParticipants().get(i);
				if (participant instanceof Person) {
					Person child = (Person) participant;
					String childGivenName = decision.getInputStrings().get(i);
					child.setGivenName(childGivenName);
				}
			}
			launch.tempChildList.removeAll(launch.tempChildList);
			launch.closeNextPopup();
			break;
		}
		case "interact_char": {
			if (decision.getParticipants() != null && !decision.getParticipants().isEmpty()) {
				Object obj = decision.getParticipants().get(0);
				if (obj instanceof Person) {
					Person person = (Person) obj;
					if (selectedIndex == 1) {
						String eventTitle = String.format("A Public Statement About %s", person.getFullName());
						String eventText = String.format("It is time to let the world know what you think of %s."
								+ " What kind of statement will you offer?", 
								person.shortDisplayName(launch.playerChar));
						MutableList<String> choices = Lists.mutable.of("Enthusiastic praise.", "Mild praise.", 
								"Polite criticism.", "Strong criticism.", "Furious condemnation.",
								"On second thought, maybe this isn't such a good idea.");
						Decision statementDec = new Decision(this, "praise_or_criticize", eventTitle, eventText,
								decision.getPicturePaths(), choices, Lists.mutable.empty(), decision.getParticipants(), 
								choices.size() - 1, Double.POSITIVE_INFINITY, false);
						Popup statementPopup = new Popup(launch, statementDec);
						launch.popupWindow = statementPopup.display();
						launch.ingameCanvas.getChildren().add(launch.popupWindow);
					}
				}
			}
			break;
		}
		case "praise_or_criticize": {
			if (decision.getParticipants() != null && !decision.getParticipants().isEmpty()) {
				Object obj = decision.getParticipants().get(0);
				if (obj instanceof Person) {
					Person person = (Person) obj;
					if (selectedIndex == 0) {
						if (person.getLivingStatus() == LivingStatus.ALIVE) {
							person.changeOpinion(launch.playerChar, 10);
						} 
					} else if (selectedIndex == 1) {
						if (person.getLivingStatus() == LivingStatus.ALIVE) {
							person.changeOpinion(launch.playerChar, 5);
						} 
					} else if (selectedIndex == 2) {
						if (person.getLivingStatus() == LivingStatus.ALIVE) {
							person.changeOpinion(launch.playerChar, -5);
						} 
					} else if (selectedIndex == 3) {
						if (person.getLivingStatus() == LivingStatus.ALIVE) {
							person.changeOpinion(launch.playerChar, -15);
						} 
					}  else if (selectedIndex == 4) {
						if (person.getLivingStatus() == LivingStatus.ALIVE) {
							person.changeOpinion(launch.playerChar, -40);
						} 
					}
				}
			}
			launch.closeNextPopup();
			break;
		}
		}
		launch.unpause();
	}

	public void clearUpMemory(int max) {
		if (individuals == null)
			return;
		int years = 200;
		while (individuals.size() > max && years >= 0) {
			for (Person p : individuals) {
				if (p.getLivingStatus() != LivingStatus.ALIVE) {
					if (Utilities.getDuration(p.getDateOfDeath(), ingameDate, "year") >= years) {
						Platform.runLater(() -> {
							individuals.remove(p);
							actors.remove(p);
						});
					}
				}
			}
			years -= 50;
		}
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ExternalAvatar getAvatar() {
		return avatar;
	}

	public void setAvatar(ExternalAvatar avatar) {
		this.avatar = avatar;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	public HashMap<Player, Person> getPlayers() {
		return players;
	}

	public void setPlayers(HashMap<Player, Person> players) {
		this.players = players;
	}

	public HashMap<Player, Launch> getPlayerConsoles() {
		return playerConsoles;
	}

	public void setPlayerConsoles(HashMap<Player, Launch> playerConsoles) {
		this.playerConsoles = playerConsoles;
	}

	public MutableList<Player> getAchieveSafePlayers() {
		return achieveSafePlayers;
	}

	public void setAchieveSafePlayers(MutableList<Player> achieveSafePlayers) {
		this.achieveSafePlayers = achieveSafePlayers;
	}

	public MutableList<GameRule> getGameRules() {
		return gameRules;
	}

	public void setGameRules(MutableList<GameRule> gameRules) {
		this.gameRules = gameRules;
	}

	public GregorianCalendar getProposalDate() {
		return proposalDate;
	}

	public void setProposalDate(GregorianCalendar proposalDate) {
		this.proposalDate = proposalDate;
	}

	public GregorianCalendar getStagingDate() {
		return stagingDate;
	}

	public void setStagingDate(GregorianCalendar stagingDate) {
		this.stagingDate = stagingDate;
	}

	public GregorianCalendar getFirstLaunchDate() {
		return firstLaunchDate;
	}

	public void setFirstLaunchDate(GregorianCalendar firstLaunchDate) {
		this.firstLaunchDate = firstLaunchDate;
	}

	public GregorianCalendar getLatestLaunchDate() {
		return latestLaunchDate;
	}

	public void setLatestLaunchDate(GregorianCalendar latestLaunchDate) {
		this.latestLaunchDate = latestLaunchDate;
	}

	public GregorianCalendar getLatestPauseDate() {
		return latestPauseDate;
	}

	public void setLatestPauseDate(GregorianCalendar latestPauseDate) {
		this.latestPauseDate = latestPauseDate;
	}

	public GregorianCalendar getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(GregorianCalendar cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public GregorianCalendar getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(GregorianCalendar completionDate) {
		this.completionDate = completionDate;
	}

	public GregorianCalendar getIngameDate() {
		return ingameDate;
	}

	public void setIngameDate(GregorianCalendar ingameDate) {
		this.ingameDate = ingameDate;
	}

	public int getEarliestYear() {
		return EARLIEST_YEAR;
	}

	public MutableList<Policy> getPolicies() {
		return policies;
	}

	public void setPolicies(MutableList<Policy> policies) {
		this.policies = policies;
	}

	public HashMap<String, MutableList<Policy>> getLawCategories() {
		return lawCategories;
	}

	public void setLawCategories(HashMap<String, MutableList<Policy>> lawCategories) {
		this.lawCategories = lawCategories;
	}

	public MutableList<Technology> getTechnologies() {
		return technologies;
	}

	public void setTechnologies(MutableList<Technology> technologies) {
		this.technologies = technologies;
	}

	public MutableList<AstronomicalBody> getAstronomicalBodies() {
		return astronomicalBodies;
	}

	public void setAstronomicalBodies(MutableList<AstronomicalBody> astronomicalBodies) {
		this.astronomicalBodies = astronomicalBodies;
	}

	public MutableList<Region> getRegions() {
		return regions;
	}

	public void setRegions(MutableList<Region> regions) {
		this.regions = regions;
	}

	public MutableList<MapLocation> getLocations() {
		return locations;
	}

	public void setLocations(MutableList<MapLocation> locations) {
		this.locations = locations;
	}

	public MutableList<Actor> getActors() {
		return actors;
	}

	public void setActors(MutableList<Actor> actors) {
		this.actors = actors;
	}

	public String[] getGroupFilterNames() {
		return groupFilterNames;
	}

	public void setGroupFilterNames(String[] groupFilterNames) {
		this.groupFilterNames = groupFilterNames;
	}

	public String[] getGroupClassNames() {
		return groupClassNames;
	}

	public void setGroupClassNames(String[] groupClassNames) {
		this.groupClassNames = groupClassNames;
	}

	public MutableList<Gender> getGenders() {
		return genders;
	}

	public void setGenders(MutableList<Gender> genders) {
		this.genders = genders;
	}

	public MutableList<Sexuality> getSexualities() {
		return sexualities;
	}

	public void setSexualities(MutableList<Sexuality> sexualities) {
		this.sexualities = sexualities;
	}

	public MutableList<AgeGroup> getAges() {
		return ages;
	}

	/**
	 * @return the cultures
	 */
	public MutableList<Culture> getCultures() {
		return cultures;
	}

	/**
	 * @param cultures the cultures to set
	 */
	public void setCultures(MutableList<Culture> cultures) {
		this.cultures = cultures;
	}

	/**
	 * @return the religions
	 */
	public MutableList<Religion> getReligions() {
		return religions;
	}

	/**
	 * @param religions the religions to set
	 */
	public void setReligions(MutableList<Religion> religions) {
		this.religions = religions;
	}

	/**
	 * @return the governments
	 */
	public MutableList<Government> getGovernments() {
		return governments;
	}

	/**
	 * @param governments the governments to set
	 */
	public void setGovernments(MutableList<Government> governments) {
		this.governments = governments;
	}

	/**
	 * @return the branches
	 */
	public MutableList<Branch> getBranches() {
		return branches;
	}

	/**
	 * @param branches the branches to set
	 */
	public void setBranches(MutableList<Branch> branches) {
		this.branches = branches;
	}

	/**
	 * @return the offices
	 */
	public MutableList<Office> getOffices() {
		return offices;
	}

	/**
	 * @param offices the offices to set
	 */
	public void setOffices(MutableList<Office> offices) {
		this.offices = offices;
	}

	public MutableList<Profession> getProfessions() {
		return professions;
	}

	public void setProfessions(MutableList<Profession> professions) {
		this.professions = professions;
	}

	public MutableList<Caste> getCastes() {
		return castes;
	}

	public void setCastes(MutableList<Caste> castes) {
		this.castes = castes;
	}

	/**
	 * @return the factions
	 */
	public MutableList<Faction> getFactions() {
		return factions;
	}

	/**
	 * @param factions the factions to set
	 */
	public void setFactions(MutableList<Faction> factions) {
		this.factions = factions;
	}

	public MutableList<Resource> getResources() {
		return resources;
	}

	public void setResources(MutableList<Resource> resources) {
		this.resources = resources;
	}

	public MutableList<Decision> getDecisions() {
		return decisions;
	}

	public void setDecisions(MutableList<Decision> decisions) {
		this.decisions = decisions;
	}

	public MutableList<WorldEvent> getEvents() {
		return events;
	}

	public void setEvents(MutableList<WorldEvent> events) {
		this.events = events;
	}

	public MutableList<Modifier> getModifiers() {
		return modifiers;
	}

	/**
	 * @return the individuals
	 */
	public MutableList<Person> getIndividuals() {
		return individuals;
	}

	/**
	 * @param individuals the individuals to set
	 */
	public void setIndividuals(MutableList<Person> individuals) {
		this.individuals = individuals;
	}

	public MutableList<Person> getLivingPeople() {
		return livingPeople;
	}

	public void setLivingPeople(MutableList<Person> livingPeople) {
		this.livingPeople = livingPeople;
	}

	public void setModifiers(MutableList<Modifier> modifiers) {
		this.modifiers = modifiers;
	}

	public MutableList<WorldDisplayElement> getWorldDisplays() {
		return worldDisplays;
	}

	public void setWorldDisplays(MutableList<WorldDisplayElement> worldDisplays) {
		this.worldDisplays = worldDisplays;
	}

	public MutableList<ImageElement> getImageElements() {
		return imageElements;
	}

	public void setImageElements(MutableList<ImageElement> imageElements) {
		this.imageElements = imageElements;
	}

	public MutableList<InternalControlElement> getInternalControlElements() {
		return internalControlElements;
	}

	public void setInternalControlElements(MutableList<InternalControlElement> internalControlElements) {
		this.internalControlElements = internalControlElements;
	}

	public MutableList<LabelElement> getLabelElements() {
		return labelElements;
	}

	public void setLabelElements(MutableList<LabelElement> labelElements) {
		this.labelElements = labelElements;
	}

	public MutableList<SoundElement> getSoundElements() {
		return soundElements;
	}

	public void setSoundElements(MutableList<SoundElement> soundElements) {
		this.soundElements = soundElements;
	}

	public Map<Integer, MutableList<String>> getNameLists() {
		return nameLists;
	}

	public NameHandler getNameHandler() {
		return nameHandler;
	}

	@Override
	public int compareTo(Game otherGame) {
		if (id < otherGame.getId()) {
			return -1;
		} else if (id == otherGame.getId()) {
			return 0;
		} else {
			return 1;
		}
	}
}