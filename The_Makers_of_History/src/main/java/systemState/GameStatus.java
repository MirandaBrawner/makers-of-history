package systemState;

public enum GameStatus {
	PROPOSED,
	STAGING,
	ACTIVE,
	PAUSED,
	CANCELED,
	COMPLETE
}
