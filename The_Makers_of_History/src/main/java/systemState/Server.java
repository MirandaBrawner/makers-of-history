package systemState;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Server {
	
	private ArrayList<Player> players;
	private ArrayList<PlayerGroup> groups;
	private ArrayList<Game> games;
	private ArrayList<Achievement> achievements;
	private ArrayList<Message> messages;
	private ArrayList<Scenario> scenarios;
	
	public Server() {
		File file;
		String fileName;
		players = new ArrayList<>();
		groups = new ArrayList<>();
		games = new ArrayList<>();
		achievements = new ArrayList<>();
		messages = new ArrayList<>();
		scenarios = new ArrayList<>();
		fileName = "src/main/resources/history/scenarios.txt";
		file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				String[] data = scanner.nextLine().split(";");
				if (data.length >= 6) {
					Scenario scenario = new Scenario();
					int year = Integer.parseInt(data[0]);
					int month = Integer.parseInt(data[1]);
					int day = Integer.parseInt(data[2]);
					GregorianCalendar scenDate = new GregorianCalendar(year, month - 1, day);
					scenario.setDate(scenDate);
					scenario.setTitle(data[3]);
					scenario.setImage(data[4]);
					String peopleIDstring = data[5];
					if (!peopleIDstring.isEmpty()) {
						for (String personID: peopleIDstring.split(",")) {
							if (!personID.isEmpty()) {
								long idNumber = Long.parseLong(personID);
								scenario.getKeyCharacters().add(idNumber);
							}
						}
					}
					scenario.setDescription(data[6]);
					scenarios.add(scenario);
				}
			}
			scanner.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.\n", fileName);
		}
	}
	
	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}

	public ArrayList<PlayerGroup> getGroups() {
		return groups;
	}

	public void setGroups(ArrayList<PlayerGroup> groups) {
		this.groups = groups;
	}

	public ArrayList<Game> getGames() {
		return games;
	}

	public void setGames(ArrayList<Game> games) {
		this.games = games;
	}

	public ArrayList<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(ArrayList<Achievement> achievements) {
		this.achievements = achievements;
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	/**
	 * @return the scenarios
	 */
	public ArrayList<Scenario> getScenarios() {
		return scenarios;
	}

	/**
	 * @param scenarios the scenarios to set
	 */
	public void setScenarios(ArrayList<Scenario> scenarios) {
		this.scenarios = scenarios;
	}

	public void add(Object obj) {
		switch (obj.getClass().getName()) {
		case "Player": {
			Collections.sort(players);
			long nextId = players.get(players.size() - 1).getId() + 1;
			Player p = (Player) obj;
			p.setId(nextId);
			players.add(p);
			break;
		}
		case "PlayerGroup": {
			Collections.sort(groups);
			long nextId = groups.get(groups.size() - 1).getId() + 1;
			PlayerGroup pg = (PlayerGroup) obj;
			pg.setId(nextId);
			groups.add(pg);
			break;
		}
		case "Game": {
			Collections.sort(games);
			long nextId = games.get(games.size() - 1).getId() + 1;
			Game g = (Game) obj;
			g.setId(nextId);
			games.add(g);
			break;
		}
		case "Achievement": {
			Collections.sort(achievements);
			long nextId = achievements.get(achievements.size() - 1).getId() + 1;
			Achievement a = (Achievement) obj;
			a.setId(nextId);
			achievements.add(a);
			break;
		}
		case "Message": {
			Collections.sort(messages);
			long nextId = messages.get(messages.size() - 1).getId() + 1;
			Message m = (Message) obj;
			m.setId(nextId);
			messages.add(m);
			break;
		}
		default: break;
		}
	}
}
