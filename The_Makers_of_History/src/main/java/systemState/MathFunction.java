package systemState;

import java.util.Arrays;
import java.util.LinkedList;

public class MathFunction {
	
	private String[] parameters;
	private String equation;
	private static double realPart = 0;
	private static double imaginaryPart = 0;
	
	public MathFunction(String[] parameters, String equation) {
		this.parameters = parameters;
		this.equation = equation;
	}
	
	public double calculate(double[] inputs, boolean negativeSplit) {
		StringBuffer inputDescription = new StringBuffer();
		for (int i = 0; i < parameters.length; i++) {
			inputDescription.append(parameters[i] + "=");
			if (i < inputs.length) {
				inputDescription.append(inputs[i]);
			} else {
				inputDescription.append("[NULL]");
			}
			if (i < parameters.length - 1) {
				inputDescription.append(", ");
			}
		}
		if (inputs[0] > -0.1) {
			System.out.printf("Calculating %s for inputs %s...\n", equation, inputDescription.toString());
		}
		StringBuffer buffer = new StringBuffer();
		String punctuation = "^!#$%&'()*+,/:'<=>?[]{}|~`\"\\";
		equation = equation.replace("- ", "-"); 
		equation = equation.replace("divided by", "/");
		equation = equation.replace("multiplied by", "*");
		equation = equation.replace("to the power of", "^");
		equation = equation.replace("raised to the", "^");
		equation = equation.replace("to the", "^");
		String sciNotation = "[0-9]+[.]?[0-9]+[eE][-]?[0-9]+";
		for (int index = 0; index < equation.length(); index++) {
			char symbol = equation.charAt(index);
			if (punctuation.contains("" + symbol) /* || (symbol == '-' && negativeSplit) */ ) {
				StringBuffer spacedToken = new StringBuffer();
				if (index > 0) {
					spacedToken.append(" ");
				} 
				spacedToken.append(symbol);
				if (index < equation.length() - 1) {
					spacedToken.append(" ");
				}
				buffer.append(spacedToken.toString());
			} else {
				buffer.append(symbol);
			}
			for (int endIndex = index + 1; endIndex < buffer.length(); endIndex++) {
				if (buffer.substring(index, endIndex).matches(sciNotation) && 
						(endIndex == buffer.length() - 1 || 
						!buffer.substring(index, endIndex + 1).matches(sciNotation))) {
					int eLocation = buffer.toString().indexOf('e', index);
					double mantissa = Double.parseDouble(
							buffer.substring(index, eLocation));
					double exponent = Double.parseDouble(
							buffer.substring(eLocation + 1, endIndex));
					double newValue = mantissa * Math.pow(10, exponent);
					StringBuffer newBuffer = new StringBuffer(buffer.substring(0, index));
					if (index > 0) {
						newBuffer.append(" ");
					}
					newBuffer.append(newValue);
					if (endIndex < buffer.length() - 1) {
						newBuffer.append(" ");
					}
					buffer = newBuffer;
				}
			}
		}
		LinkedList<String> tokens = new LinkedList<>();
		Arrays.stream(buffer.toString().split(" ")).filter(t -> !t.isEmpty()).forEach(t -> tokens.add(t.toLowerCase()));
		StringBuffer tokenDisplay = new StringBuffer("Tokens: ");
		tokens.stream().forEach(t -> tokenDisplay.append("[" + t + "] "));
		if (inputs[0] > -0.1) {
			System.out.println(tokenDisplay.toString());
		}
		if (tokens.isEmpty()) {
			return Double.NaN;
		} else if (tokens.size() == 1) {
			String token = tokens.get(0);	
			int position = Arrays.asList(parameters).indexOf(token);
			if (position >= 0) {
				return inputs[position];
			}
			try {
				return Double.parseDouble(token);
			} catch (NumberFormatException ex) {
				if (token.equals("e")) {
					return Math.E;
				} else if (token.equals("pi")) {
					return Math.PI;
				} else if (token.equals("infinity")) {
					return Double.POSITIVE_INFINITY; 
				} else if (token.equals("i")) {
					imaginaryPart = 1;
					return 0;
				} else if (token.matches("[0-9]*[.]?[0-9]+[eE][-]?[0-9]+")) {
					int eLocation = token.toString().indexOf('e');
					double mantissa = Double.parseDouble(
							token.substring(0, eLocation));
					double exponent = Double.parseDouble(
							buffer.substring(eLocation + 1, token.length()));
					return mantissa * Math.pow(10, exponent);
				} else { /* System.out.printf("%d-token expression not recognized: %s -> NaN\n",
						tokens.size(), equation); */ return Double.NaN; }
			}
		} else if (tokens.size() == 2) {
			String first = tokens.get(0);
			MathFunction firstSubFunction = new MathFunction(parameters, first);
			double firstSubValue = firstSubFunction.calculate(inputs, false);
			String second = tokens.get(1);
			MathFunction secondSubFunction = new MathFunction(parameters, second);
			double secondSubValue = secondSubFunction.calculate(inputs, false);
			boolean negativeSign = first.equals("-") || first.equals("negative");
			boolean squareRoot = first.equals("sqrt") || first.equals("squareroot");
			boolean sine = first.equals("sin") || first.equals("sine");
			boolean cosine = first.equals("cos") || first.equals("cosine");
			boolean tangent = first.equals("tan") || first.equals("tangent");
			boolean arcsine = first.equals("asin") ||first.equals("arcsin") || first.equals("arcsine");
			boolean arccosine = first.equals("acos") ||first.equals("arccos") || first.equals("arccosine");
			boolean arctangent = first.equals("atan") ||first.equals("arctan") || first.equals("arctangent");
			boolean prime = first.equals("prime");
			boolean fib = first.equals("fib") || first.equals("fibonacci");
			if (negativeSign) return -secondSubValue;
			else if (squareRoot) {
				if (secondSubValue < 0) {
					imaginaryPart = Math.sqrt(-secondSubValue);
					return 0;
				} else return Math.sqrt(secondSubValue);
			}
			else if (sine) return Math.sin(secondSubValue);
			else if (cosine) return Math.cos(secondSubValue);
			else if (tangent) return Math.tan(secondSubValue);
			else if (arcsine) return Math.asin(secondSubValue);
			else if (arccosine) return Math.acos(secondSubValue);
			else if (arctangent) return Math.atan(secondSubValue);
			else if (prime) {
				if (secondSubValue < 1) return Double.NaN;
				int current = 2;
				int primeCount = 0;
				double highestPrime = Double.NaN;
				while (primeCount < (int) secondSubValue) {
					boolean isPrime = true;
					for (int factor = 2; factor * factor <= current; factor++) {
						if (current % factor == 0) {
							isPrime = false;
							break;
						}
					}
					if (isPrime) { 
						primeCount++; 
						highestPrime = current;
					}
					current++;
				}
				return highestPrime;
			}
			else if (fib) {
				if (secondSubValue < 0) return Double.NaN;
				else if (secondSubValue < 1) return 0;
				else if (secondSubValue < 2) return 1;
				int howMany = ((int) secondSubValue) + 1;
				int[] sequence = new int[howMany];
				sequence[0] = 0;
				sequence[1] = 1;
				for (int index = 2; index < howMany; index++) {
					sequence[index] = sequence[index - 2] + sequence[index - 1];
				}
				return sequence[howMany - 1];
			} else if (second.equals("!") || second.equals("factorial")) {
				if (firstSubValue < 0) return Double.NaN;
				else {
					int max = (int) firstSubValue;
					if (max < 2) return 1;
					int product = 1;
					for (int factor = 2; factor <= max; factor++) {
						product *= factor;
					}
					return product;
				}
			} else { /* System.out.printf("%d-token expression not recognized: %s -> NaN\n",
					tokens.size(), equation); */ return Double.NaN; }
		} else if (tokens.size() == 3) {
			String first = tokens.get(0);
			MathFunction firstSubFunction = new MathFunction(parameters, first);
			double firstValue = firstSubFunction.calculate(inputs, false);
			String second = tokens.get(1);
			MathFunction secondSubFunction = new MathFunction(parameters, second);
			double secondValue = secondSubFunction.calculate(inputs, false);
			String third = tokens.get(2);
			MathFunction thirdSubFunction = new MathFunction(parameters, third);
			double thirdValue = thirdSubFunction.calculate(inputs, false);
			String firstSecond = first + " " + second;
			MathFunction firstSecondSubFunction = new MathFunction(parameters, firstSecond);
			double firstSecondValue = firstSecondSubFunction.calculate(inputs, false);
			String secondThird = second + " " + third;
			MathFunction secondThirdSubFunction = new MathFunction(parameters, secondThird);
			double secondThirdValue = secondThirdSubFunction.calculate(inputs, false);
			String[] unaryOperators = {"-", "negative", "sqrt", "squareroot", "sin", "sine", "cos", "cosine",
					"tan", "tangent", "asin", "arcsin", "arcsine", "acos", "arccos", "arccosine", "atan",
					"arctan", "arctangent", "prime", "fib"};
			if (Arrays.asList(unaryOperators).contains(first)) {
				String reducedEquation = String.format("%s %s", first, secondThirdSubFunction.calculate(inputs, false));
				return new MathFunction(parameters, reducedEquation).calculate(inputs, false);
			} else if (second.equals("+") || second.equals("plus")) {
				return firstValue + thirdValue;
			} else if (second.equals("-") || second.equals("minus")) {
				if (firstValue == Double.NaN) {
					String reduced = first + " " + (-1 * thirdValue);
					return new MathFunction (parameters, reduced).calculate(inputs, false);
				} else return firstValue - thirdValue;
			} else if (second.equals("*") || second.equals("times") || second.equals("multipliedby")) {
				return firstValue * thirdValue;
			} else if (second.equals("/") || second.equals("dividedby")) {
				return firstValue / thirdValue;
			} else if (second.equals("%") || second.equals("mod") || second.equals("modulo") || second.equals("remainder")) {
				return firstValue % thirdValue;
			} else if (second.equals("^")) {
				return Math.pow(firstValue, thirdValue);
			} else { /* System.out.printf("%d-token expression not recognized: %s -> NaN\n",
					tokens.size(), equation); */ return Double.NaN; }
		} else {
			int latestOpen = -1;
			for (int index = 0; index < tokens.size(); index++) {
				if (tokens.get(index).equals("(")) {
					latestOpen = index;
				} else if (tokens.get(index).equals(")")) {
					StringBuffer before = new StringBuffer();
					for (int subindex = 0; subindex < latestOpen; subindex++) {
						before.append(tokens.get(subindex));
						if (subindex < latestOpen - 1) {
							before.append(" ");
						}
					}
					StringBuffer subequation = new StringBuffer();
					for (int subindex = latestOpen + 1; subindex < index; subindex++) {
						subequation.append(tokens.get(subindex));
						if (subindex < index - 1) {
							subequation.append(" ");
						}
					}
					MathFunction subfunc = new MathFunction(parameters, subequation.toString());
					double innerValue = subfunc.calculate(inputs, true);
					StringBuffer after = new StringBuffer();
					for (int subindex = index + 1; subindex < tokens.size(); subindex++) {
						after.append(tokens.get(subindex));
						if (subindex < tokens.size() - 1) {
							after.append(" ");
						}
					}
					StringBuffer reduced = new StringBuffer(before.toString());
					if (reduced.length() > 0) {
						reduced.append(" ");
					}
					reduced.append(innerValue);
					if (after.length() > 0) {
						reduced.append(" ");
						reduced.append(after.toString());
					}
					return new MathFunction(parameters, reduced.toString()).calculate(inputs, true);
				}
			}
			for (int index = 0; index < tokens.size() - 1; index++) {
				String t = tokens.get(index);
				if (t.equals("-") || t.equals("negative") || t.equals("minus")) {
					boolean unary = false;
					if (index > 0) {
						double preceding = new MathFunction(parameters, tokens.get(index - 1)).calculate(inputs, false);
						if (Double.isNaN(preceding)) {
							unary = true;
						}
					} else unary = true;
					if (unary) {
						if (index < tokens.size() - 1) {
							double following = new MathFunction(parameters, tokens.get(index + 1)).calculate(inputs, false);
							if (following != Double.NaN) {
								double negative = following * -1;
								StringBuffer reduced = new StringBuffer();
								for (int subindex = 0; subindex < index; subindex++) {
									reduced.append(tokens.get(subindex));
								}
								if (index != 0) {
									reduced.append(" ");
								}
								reduced.append(negative);
								if (index + 1 <= tokens.size() - 1) {
									reduced.append(" ");
								}
								for (int subindex = index + 2; subindex < tokens.size(); subindex++) {
									reduced.append(tokens.get(subindex));
								}
								return new MathFunction(parameters, reduced.toString()).calculate(inputs, false);
							}
						} 
					}
				}
			}
		}
		/* System.out.printf("%d-token expression not recognized: %s -> NaN\n",
				tokens.size(), equation); */ return Double.NaN; 
	}
	
	public String getResult(double[] inputs, boolean negativeSplit, double precision) {
		realPart = 0;
		imaginaryPart = 0;
		realPart = calculate(inputs, negativeSplit);
		if (realPart < precision && realPart > -precision) {
			if (imaginaryPart < precision && imaginaryPart > -precision) return "0";
			else if (imaginaryPart == 1) return "i";
			else return String.format("%si", imaginaryPart);
		} else {
			if (imaginaryPart < precision && imaginaryPart > -precision) {
				return "" + realPart;
			} else if (imaginaryPart > 0) {
				return String.format("%s+%si", realPart, imaginaryPart);
			} else return String.format("%s%si", realPart, imaginaryPart);
		}
	}
	
	public static void main(String[] args) {
		String[] functions = {/* "", "a", "e", "InFiNiTy", "E", "Pi", "26.2", "-25",
				"cosine pi", "sine pi", "negative e", "sqrt 2", "fib 5", "fib 10",
				"fibonacci 12.1", "prime 3", "prime 4.2", "prime -1", "arccos 1",
				"sqrt 49", "sqrt -1", "negative -10", "a", "b", "c", "- a", "prime c",
				"sqrt a", "fib b", "-c", "a * b", "c - 7", "c % 4", "1.5 / 2.0", "2 to the 3",
				"10 plus 10", "3 * (5 + 5)", "a + (1 / 2)", "c factorial", "b!", 
				"a to the power of (1 divided by 2)", "a ^ (1 / 2)", "a to the power of (1 / 2)",
				"a ^ (1 divided by 2)", "a ^ b", "c / b", "(1 / 2) ^ 2", */ "e ^ (c * (a ^ 2))"};
		for (String f: functions) {
			String[] variables = {"a", "b", "c"};
			MathFunction m = new MathFunction(variables, f);
			double[] inputs = {3, 4, 5};
			System.out.printf("%s -> %s\n", f, m.getResult(inputs, false, 0.0000000000000001));
		}
	}

	public String[] getParameters() {
		return parameters;
	}

	public void setParameters(String[] parameters) {
		this.parameters = parameters;
	}

	public String getEquation() {
		return equation;
	}

	public void setEquation(String equation) {
		this.equation = equation;
	}

	public static double getRealPart() {
		return realPart;
	}

	public static void setRealPart(double realPart) {
		MathFunction.realPart = realPart;
	}

	public static double getImaginaryPart() {
		return imaginaryPart;
	}

	public static void setImaginaryPart(double imaginaryPart) {
		MathFunction.imaginaryPart = imaginaryPart;
	}
	
}
