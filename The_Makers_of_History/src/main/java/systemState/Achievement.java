package systemState;

import java.util.ArrayList;

import view.images.ExternalAvatar;

public class Achievement implements Comparable<Achievement> {
	
	private Server server;
	private long id;
	private String name;
	private String description;
	private ExternalAvatar avatar;
	private ArrayList<Player> achievers;
	
	public Achievement(Server server, String name, String description) {
		this.server = server;
		this.name = name;
		this.description = description;
		this.achievers = new ArrayList<>();
		server.add(this);
	}

	public Achievement(Server server) {
		this(server, "Achievement", "This achievement needs a description.");
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ExternalAvatar getAvatar() {
		return avatar;
	}

	public void setAvatar(ExternalAvatar avatar) {
		this.avatar = avatar;
	}

	public ArrayList<Player> getAchievers() {
		return achievers;
	}

	public void setAchievers(ArrayList<Player> achievers) {
		this.achievers = achievers;
	}

	@Override
	public int compareTo(Achievement otherAchievement) {
		if (id < otherAchievement.getId()) {
			return -1;
		} else if (id == otherAchievement.getId()) {
			return 0;
		} else {
			return 1;
		}
	}
	
}
