package systemState;

import java.util.TimerTask;
import javafx.scene.control.Button;
import view.Launch;

@SuppressWarnings("restriction")
public class CustomCharTimerTask extends TimerTask {

	public Launch app;
	public Button button;
	
	public CustomCharTimerTask(Launch app, Button button) {
		this.app = app;
		this.button = button;
	}
	
	@Override
	public void run() {
		app.checkCustomChar(button);
	}

}
