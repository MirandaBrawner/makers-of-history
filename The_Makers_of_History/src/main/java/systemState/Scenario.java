package systemState;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Scenario {
	private String title;
	private String description;
	private GregorianCalendar date;
	private String image;
	private ArrayList<Long> keyCharacters;
	
	public Scenario() {
		keyCharacters = new ArrayList<>();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the date
	 */
	public GregorianCalendar getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(GregorianCalendar date) {
		this.date = date;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the keyCharacters
	 */
	public ArrayList<Long> getKeyCharacters() {
		return keyCharacters;
	}

	/**
	 * @param keyCharacters the keyCharacters to set
	 */
	public void setKeyCharacters(ArrayList<Long> keyCharacters) {
		this.keyCharacters = keyCharacters;
	}
}
