package systemState;

import java.util.TimerTask;

import view.Launch;

public class GameTimerTask extends TimerTask {
	
	public Launch app;
	
	public GameTimerTask(Launch launch) {
		app = launch;
	}

	@Override
	public synchronized void run() {
		app.advanceTime();
	}

}
