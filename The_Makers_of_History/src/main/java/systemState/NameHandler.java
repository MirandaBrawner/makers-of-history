package systemState;

import java.util.Arrays;
import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import models.actors.Actor;
import models.actors.Culture;
import models.actors.Person;

public class NameHandler {

	public Game game;

	public static final String[] POSSIBLE_RULES = "gs sg gps gp".split(" ");

	public NameHandler(Game game) {
		this.game = game;
	}

	public Culture getNamingCulture(MutableList<Culture> startingCultures) {
		MutableList<Culture> currentCultures = Lists.mutable.ofAll(startingCultures);
		for (int round = 0; round < 50; round++) {
			MutableList<Culture> parentList = Lists.mutable.empty();
			for (Culture culture : currentCultures) {
				if (!culture.getNameVarRule().isEmpty()) {
					return culture;
				}
				culture.getSupergroups().collect(parId -> parentList.add((Culture) game.lookupActorByID(parId)));
			}
			if (parentList.isEmpty())
				break;
			currentCultures = parentList;
		}
		return (Culture) game.lookupActorByID(50_000L);
	}

	public MutableList<MutableList<String>> getNameListsForSeveralCultures(MutableList<Culture> cultures) {
		 //System.out.println("Starting name list search...");
		MutableList<MutableList<String>> results = Lists.mutable.empty();
		for (int offset = 0; offset < game.NAME_LIST_NUMBER; offset++) {
			results.add(Lists.mutable.empty());
		}
		MutableList<Culture> currentCultures = Lists.mutable.ofAll(cultures);
		for (int round = 0; round < 50; round++) {
			for (Culture culture : currentCultures) {
				//System.out.printf("Searching for name lists for the %s culture...\n",
				 //culture.getAdjective());
				int listBaseId = (int) (game.NAME_LIST_BLOCK_SIZE * (culture.getId() - 50000));
				for (int offset = 0; offset < game.NAME_LIST_NUMBER; offset++) {
					int key = listBaseId + offset;
					 //System.out.printf("Searching for name list with key %d...\n", key);
					if (game.getNameLists().containsKey(key)) {
						 //System.out.printf("Name list found. Key: %d. Number of Names: %d.\n", key,
						 //game.getNameLists().get(key).size());
						results.get(offset).addAll(game.getNameLists().get(key));
					}
				}
			}
			if (currentCultures.anySatisfy(cul -> cul.getId() == 50_000L)
					|| results.allSatisfy(nameList -> !nameList.isEmpty())) {
				 //System.out.println("Name list search complete.\n");
				break;
			} else {
				MutableList<Culture> parentList = Lists.mutable.empty();
				for (Culture culture : currentCultures) {
					for (Long parentId : culture.getSupergroups()) {
						Culture parentCulture = (Culture) game.lookupActorByID(parentId);
						if (!parentList.contains(parentCulture)) {
							parentList.add(parentCulture);
						}
					}
				}
				currentCultures = parentList;
			}
		}
		return results;
	}
	
	public String transformName(String genericName, String nameVariation, Person person, boolean trueGender) {
		return transformName(genericName, nameVariation, person, trueGender, 1);
	}

	public String transformName(String genericName, String nameVariation, Person person, boolean trueGender,
			double chanceOfParentName) {
		if (nameVariation == null || nameVariation.isEmpty()) {
			System.out.println("Error: No naming rule found.");
			return genericName;
		}
		String tempName = genericName;
		String[] rules = nameVariation.split(",");
		List<String> ruleList = Arrays.asList(rules);
		if (ruleList.contains("gp") || ruleList.contains("gm")) {
			if (Math.random() < chanceOfParentName) {
				MutableList<Culture> cultures = person.getCultures();
				MutableList<MutableList<String>> nameMatrix = getNameListsForSeveralCultures(cultures);
				MutableList<String> surnameList = Lists.mutable.ofAll(nameMatrix.get(4));
				if (surnameList.size() == 0) {
					System.out.println("Error: No patronyms or matronyms found.");
					cultures.forEach(cul -> System.out.println(cul.getExonym()));
				}
				int seed = (int) (Math.random() * surnameList.size());
				tempName = surnameList.get(seed);
			}
		}
		if (tempName.contains("[FATHER]") || tempName.contains("[MOTHER]")) {
			if (!person.getLegalParents().isEmpty()) {
				for (long parentId : person.getLegalParents()) {
					Actor actor = game.lookupActorByID(parentId);
					if (actor != null && actor instanceof Person) {
						Person parent = (Person) actor;
						tempName = tempName.replaceAll("\\[", "");
						tempName = tempName.replaceAll("\\]", "");
						if (parent.getGender().getGender() < 0.5) {
							tempName = tempName.replaceAll("FATHER", parent.getGivenName());
						} else {
							tempName = tempName.replaceAll("MOTHER", parent.getGivenName());
						}
					}
				}
			}
		}
		if (nameVariation.contains("[FATHER]") || nameVariation.contains("[MOTHER]")) {
			return "";
		}
		double number = person.getGender().getAssignedGender();
		if (trueGender) {
			number = person.getGender().getGender();
		}
		if (ruleList.contains("(f/m)") && tempName.contains("/")) {
			String newName = "";
			String section = "before";
			for (int charIndex = 0; charIndex < tempName.length(); charIndex++) {
				char nextChar = tempName.charAt(charIndex);
				if (nextChar == '(') {
					section = "f";
				} else if (nextChar == '/') {
					section = "m";
				} else if (nextChar == ')') {
					section = "after";
				} else if (section == "before" || section == "after") {
					newName += nextChar;
				} else if (section == "f" && number >= 0.5) {
					newName += nextChar;
				} else if (section == "m" && number < 0.5) {
					newName += nextChar;
				}
			}
			return newName;
		}
		for (String rule : rules) {
			if (!rule.contains("="))
				continue;
			if ((number >= 0.5 && rule.charAt(0) == 'f') || (number < 0.5 && rule.charAt(0) == 'm')) {
				String restOfRule = rule.substring(2);
				String expression = "x(x)";
				if (restOfRule.equals(expression) && tempName.contains("(") && tempName.contains(")")) {
					String newName = "";
					for (int charIndex = 0; charIndex < tempName.length(); charIndex++) {
						char nextChar = tempName.charAt(charIndex);
						if (nextChar != '(' && nextChar != ')') {
							newName += nextChar;
						}
					}
					return newName;
				}
				expression = "x()";
				if (restOfRule.equals(expression) && tempName.contains("(") && tempName.contains(")")) {
					String newName = "";
					int depth = 0;
					for (int charIndex = 0; charIndex < tempName.length(); charIndex++) {
						char nextChar = tempName.charAt(charIndex);
						if (nextChar == '(') {
							depth++;
						} else if (nextChar == ')') {
							depth--;
						} else if (depth == 0) {
							newName += nextChar;
						}
					}
					return newName;
				}
			}
		}
		return tempName;
	}

	public String getOrderedNameByCulture(Culture culture, String givenName, String customSurname, String parental) {
		String nameRule = "";
		if (culture != null) {
			Culture currentCulture = culture;
			for (int i = 0; i < 100; i++) {
				if (!currentCulture.getNameVarRule().isEmpty()) {
					nameRule = currentCulture.getNameVarRule();
					break;
				} else if (currentCulture.getSupergroups().isEmpty()) {
					break;
				} else {
					currentCulture = (Culture) (game.lookupActorByID(currentCulture.getSupergroups().get(0)));
				}
			}
		}
		return getOrderedName(nameRule, givenName, customSurname, parental);
	}

	public String getOrderedName(String nameVariation, String givenName, String customSurname, String parental) {
		if (!nameVariation.isEmpty()) {
			String[] rules = nameVariation.split(",");
			for (String rule : rules) {
				if (rule.equals("gs") || rule.equals("gp") || rule.equals("gm")) {
					return String.format("%s %s", givenName, customSurname);
				} else if (rule.equals("sg")) {
					return String.format("%s %s", customSurname, givenName);
				}
			}
		}
		return (Math.random() < 0.5) ? String.format("%s %s", givenName, customSurname)
				: String.format("%s %s", customSurname, givenName);
	}

	public void nameChild(MutableList<Person> parents, Person child) {
		String nameVariation = "";
		String parentName = "";
		if (!parents.isEmpty()) {
			for (Person parent : parents) {
				if (parent != null && parent.getFamilyName() != null && !parent.getFamilyName().isEmpty()) {
					child.setFamilyName(parent.getFamilyName());
					nameVariation = getNamingCulture(parent.getCultures()).getNameVarRule();
					String customFamilyName = transformName(parent.getFamilyName(), nameVariation, child, false);
					child.setCustomFamilyName(customFamilyName);
					child.setShortName(customFamilyName);
					parentName = parent.getFullName().replace(parent.getCustomFamilyName(), "").trim();
					break;
				}
			}
		}
		MutableList<Culture> parentCultures = parents.flatCollect(parent -> parent.getCultures());
		MutableList<MutableList<String>> nameMatrix = getNameListsForSeveralCultures(parentCultures);
		MutableList<String> nameList = Lists.mutable.ofAll(nameMatrix.get(0));
		if (child.getGender().getAssignedGender() <= 0.6) {
			nameList.addAll(nameMatrix.get(2));
		}
		if (child.getGender().getAssignedGender() >= 0.4) {
			nameList.addAll(nameMatrix.get(1));
		}
		int seed = (int) (Math.random() * nameList.size());
		String givenName = nameList.get(seed);
		child.setGivenName(givenName);
		child.setFullName(getOrderedName(nameVariation, givenName, child.getCustomFamilyName(), parentName));
	}
}
