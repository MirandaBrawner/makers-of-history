package systemState.comparators;

import java.util.Comparator;

import models.ideas.Policy;

public class PolicyComparator implements Comparator<Policy> {

	@Override
	public int compare(Policy first, Policy second) {
		models.ideas.Policy firstPolicy = (models.ideas.Policy) first;
		models.ideas.Policy secondPolicy = (models.ideas.Policy) second;
		return firstPolicy.getName().compareTo(secondPolicy.getName());
	}

}
