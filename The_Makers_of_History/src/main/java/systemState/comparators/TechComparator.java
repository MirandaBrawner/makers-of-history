package systemState.comparators;

import java.util.Comparator;

import models.ideas.Technology;

/* Sorts by discovery date in reverse chronological order, from latest to earliest. */
public class TechComparator implements Comparator<Technology> {
	
	public TechComparator() {}

	@Override
	public int compare(Technology firstTech, Technology secondTech) {
		int firstYear = firstTech.getYear();
		int secondYear = secondTech.getYear();
		if (firstYear > secondYear) {
			return -1;
		} else return firstYear == secondYear ? 0 : 1;
	}

}
