package systemState.comparators;

import java.util.Comparator;

import models.places.Region;
import systemState.Game;

public class RegionComparator implements Comparator<Long> {
	
	public Game game;
	
	public RegionComparator(Game game) {
		this.game = game;
	}

	@Override
	public int compare(Long firstID, Long secondID) {
		long id1 = (long) firstID;
		long id2 = (long) secondID;
		Region firstRegion = game.lookupRegionByID(id1);
		Region secondRegion = game.lookupRegionByID(id2);
		if (firstRegion == null || !(firstRegion instanceof Region)) {
			if (secondRegion == null || !(secondRegion instanceof Region)) {
				return 0;
			} else {
				return -1;
			}
		} else {
			if (secondRegion == null || !(secondRegion instanceof Region)) {
				return 1;
			} else {
				Region r1 = (Region) firstRegion;
				Region r2 = (Region) secondRegion;
				return r1.getName().compareToIgnoreCase(r2.getName());
			}
		}
	}
	

}
