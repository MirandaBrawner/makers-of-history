package systemState.comparators;

import java.util.Comparator;

import models.actors.Actor;
import models.actors.AgeGroup;
import models.actors.PeopleGroup;
import systemState.Game;

public class PeopleGroupComparator implements Comparator<Long> {
	
	public Game game;
	
	public PeopleGroupComparator(Game game) {
		this.game = game;
	}

	@Override
	public int compare(Long firstID, Long secondID) {
		long id1 = (long) firstID;
		long id2 = (long) secondID;
		Actor firstActor = game.lookupActorByID(id1);
		Actor secondActor = game.lookupActorByID(id2);
		if (firstActor == null || !(firstActor instanceof PeopleGroup)) {
			if (secondActor == null || !(secondActor instanceof PeopleGroup)) {
				return 0;
			} else {
				return -1;
			}
		} else {
			if (secondActor == null || !(secondActor instanceof PeopleGroup)) {
				return 1;
			} else {
				PeopleGroup g1 = (PeopleGroup) firstActor;
				PeopleGroup g2 = (PeopleGroup) secondActor;
				if (g1 instanceof AgeGroup && g2 instanceof AgeGroup) {
					AgeGroup ag1 = (AgeGroup) g1;
					AgeGroup ag2 = (AgeGroup) g2;
					return Integer.valueOf(ag1.getLowerBoundInclusive()).compareTo(
							Integer.valueOf(ag2.getLowerBoundInclusive()));
				}
				return g1.getExonym().compareToIgnoreCase(g2.getExonym());
			}
		}
	}
}
