package systemState;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import view.images.ExternalAvatar;

public class PlayerGroup implements Comparable<PlayerGroup> {
	
	private Server server;
	private long id;
	private String name;
	private String password;
	private String description;
	private ExternalAvatar avatar;
	private boolean active;
	private ArrayList<Player> allMembers;
	private ArrayList<Player> moderators;
	private GregorianCalendar dateFounded;
	private GregorianCalendar dateDeactivated;
	
	public PlayerGroup(Player founder, String name, String password, 
			String description, ExternalAvatar avatar, Server server) {
		this.name = name;
		this.password = password;
		this.description = description;
		this.avatar = avatar;
		this.server = server;
		active = true;
		allMembers = new ArrayList<>();
		allMembers.add(founder);
		moderators = new ArrayList<>();
		moderators.add(founder);
		founder.getGroups().add(this);
		dateFounded = new GregorianCalendar();
		server.add(this);
	}
	
	public PlayerGroup(Player founder, String password, Server server) {
		this(founder, "Group", password, "Feel free to add a description of your group here.", 
				new ExternalAvatar(), server);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ExternalAvatar getAvatar() {
		return avatar;
	}

	public void setAvatar(ExternalAvatar avatar) {
		this.avatar = avatar;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ArrayList<Player> getAllMembers() {
		return allMembers;
	}

	public void setAllMembers(ArrayList<Player> allMembers) {
		this.allMembers = allMembers;
	}

	public ArrayList<Player> getModerators() {
		return moderators;
	}

	public void setModerators(ArrayList<Player> moderators) {
		this.moderators = moderators;
	}

	public GregorianCalendar getDateFounded() {
		return dateFounded;
	}

	public void setDateFounded(GregorianCalendar dateFounded) {
		this.dateFounded = dateFounded;
	}

	public GregorianCalendar getDateDeactivated() {
		return dateDeactivated;
	}

	public void setDateDeactivated(GregorianCalendar dateDeactivated) {
		this.dateDeactivated = dateDeactivated;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	@Override
	public int compareTo(PlayerGroup otherGroup) {
		if (id < otherGroup.getId()) {
			return -1;
		} else if (id == otherGroup.getId()) {
			return 0;
		} else {
			return 1;
		}
	}
}