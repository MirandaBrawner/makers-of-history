package systemState;

import view.images.ExternalAvatar;

public class Admin extends Player {
	public Admin(Server server, String name, String password, String email, 
			String gender, String description, String language, ExternalAvatar avatar) {
		super(server, name, password, email, gender, description, language, avatar);
	}

	public Admin(Server server, String password, String email) {
		this(server, String.format("Admin"), password, email, "Not Specified",
				"Feel free to use this space to tell us about yourself.", "eng", new ExternalAvatar());
	}
	
	public void ban(Player player, String message) {
		player.deactivateAccount();
		sendEmail(player, message);
	}
	
	public void ban(Player player) {
		ban(player, "You have been banned for violating community standards.");
	}
}
