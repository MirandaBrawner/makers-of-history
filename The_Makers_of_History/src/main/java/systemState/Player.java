package systemState;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import view.images.ExternalAvatar;

public class Player implements Comparable<Player> {
	
	private Server server;
	private long id;
	private String name;
	private String password;
	private String email;
	private String gender;
	private String description;
    private String language;
    private boolean active;
    private GregorianCalendar joinDate;
    private GregorianCalendar deactivationDate;
    private ExternalAvatar avatar;
    private ArrayList<Achievement> achievements;
    private ArrayList<PlayerGroup> groups;
    private ArrayList<Game> games;
    
    public Player(Server server, String name, String password, String email, 
    		String gender, String description, String language, ExternalAvatar avatar) {
    	this.server = server;
    	this.name = name;
    	this.password = password;
    	this.email = email;
    	this.gender = gender;
    	this.description = description;
    	this.language = language;
    	this.avatar = avatar;
    	this.joinDate = new GregorianCalendar();
    	this.active = true;
    	server.add(this);
    }
    
    public Player(Server server, String password, String email) {
    	this(server, "Player", password, email, "Not Specified", 
    			"Feel free to use this space to tell us about yourself.", "eng", new ExternalAvatar());
    }
    
    public void sendEmail(Player player, String message) {
    	
    }
    
    
    public void deactivateAccount() {
    	active = false;
    	deactivationDate = new GregorianCalendar();
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public GregorianCalendar getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(GregorianCalendar joinDate) {
		this.joinDate = joinDate;
	}

	public GregorianCalendar getDeactivationDate() {
		return deactivationDate;
	}

	public void setDeactivationDate(GregorianCalendar deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public ExternalAvatar getAvatar() {
		return avatar;
	}

	public void setAvatar(ExternalAvatar avatar) {
		this.avatar = avatar;
	}

	public ArrayList<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(ArrayList<Achievement> achievements) {
		this.achievements = achievements;
	}

	public ArrayList<PlayerGroup> getGroups() {
		return groups;
	}

	public void setGroups(ArrayList<PlayerGroup> groups) {
		this.groups = groups;
	}

	public ArrayList<Game> getGames() {
		return games;
	}

	public void setGames(ArrayList<Game> games) {
		this.games = games;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	@Override
	public int compareTo(Player otherPlayer) {
		if (id < otherPlayer.getId()) {
			return -1;
		} else if (id == otherPlayer.getId()) {
			return 0;
		} else return 1;
	}
	
	@Override
	public boolean equals(Object otherObj) {
		if (otherObj instanceof Player) {
			Player otherPlayer = (Player) otherObj;
			return id == otherPlayer.id;
		} else return false;
	}
}

