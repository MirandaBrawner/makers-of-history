package systemState;

import java.util.List;
import java.util.TreeMap;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

public class GameRule implements Comparable<GameRule> {
	private static final GameRuleOption blankChoice = new GameRuleOption(null, -1, "No Option Selected", false);
	private final Game game;
	private final int id;
	private final String scope;
	private final String name;
	private final MutableList<GameRuleOption> choices;
	private MutableList<GameRuleOption> allowedChoices;
	private final int defaultMutableListting;
	private TreeMap<Player, Integer> currentMutableListtings;
	
	public GameRule(Game game, int id, String scope, String name,
	MutableList<GameRuleOption> choices, int defaultMutableListting) {
		this.game = game;
		this.id = id;
		this.scope = scope;
		this.name = name;
		this.choices = Lists.mutable.empty();
		choices.forEach(choice -> {
			GameRuleOption choiceCopy = choice.deepCopy(this);
			this.choices.add(choiceCopy);
		});
		this.defaultMutableListting = defaultMutableListting;
		currentMutableListtings = new TreeMap<>();
		for (Player player: game.getPlayers().keySet()) {
			currentMutableListtings.put(player, defaultMutableListting);
		}
		allowedChoices = Lists.mutable.empty();
		allowedChoices.addAll(choices);
	}
	
	public GameRuleOption getOptionByID(int optionID) {
		for (GameRuleOption choice: choices) {
			if (choice.getId() == optionID) {
				return choice;
			}
		}
		return blankChoice;
	}
	
	public GameRuleOption getOptionByName(String optionName) {
		for (GameRuleOption choice: choices) {
			if (choice.getName().equals(optionName)) {
				return choice;
			}
		}
		return blankChoice;
	}
	
	public GameRuleOption getForPlayer(Player player) {
		if (currentMutableListtings.containsKey(player)) {
			return getOptionByID(currentMutableListtings.get(player));
		} else if (choices.size() > defaultMutableListting) {
			return getOptionByID(defaultMutableListting);
		}
		return blankChoice;
	}
	
	public void changeForPlayer(Player player, int newMutableListting) {
		if (currentMutableListtings.containsKey(player) && newMutableListting >= 0 && newMutableListting < choices.size()) {
			currentMutableListtings.put(player, newMutableListting);
			game.updateAchieveSafePlayer(player);
		}
	}
	
	public void changeForPlayer(Player player, GameRuleOption newMutableListting) {
		changeForPlayer(player, newMutableListting.getId());
	}
	
	public void changeForPlayer(Player player, String newMutableListting) {
		changeForPlayer(player, getOptionByName(newMutableListting).getId());
	}
	
	public void resetForPlayer(Player player) {
		changeForPlayer(player, defaultMutableListting);
	}
	
	public void changeForPlayerList(List<Player> playerList, int newMutableListting) {
		for (Player player: playerList) {
			changeForPlayer(player, newMutableListting);
		}
	}
	
	public void changeForPlayerList(List<Player> playerList, GameRuleOption newMutableListting) {
		changeForPlayerList(playerList, newMutableListting.getId());
	}
	
	public void changeForPlayerList(List<Player> playerList, String newMutableListting) {
		changeForPlayerList(playerList, getOptionByName(newMutableListting).getId());
	}
	
	public void resetForPlayerList(List<Player> playerList) {
		for (Player player: playerList) {
			changeForPlayer(player, defaultMutableListting);
		}
	}
	
	public void changeForAll(int newMutableListting) {
		for (Player player: currentMutableListtings.keySet()) {
			changeForPlayer(player, newMutableListting);
		}
	}
	
	public void changeForAll(GameRuleOption newMutableListting) {
		changeForAll(newMutableListting.getId());
	}
	
	public void changeForAll(String newMutableListting) {
		changeForAll(getOptionByName(newMutableListting).getId());
	}
	
	public void resetForAll() {
		for (Player player: currentMutableListtings.keySet()) {
			changeForPlayer(player, defaultMutableListting);
		}
	}
	
	public boolean isSelected(GameRuleOption ruleOption, Player player) {
		return currentMutableListtings.containsKey(player) && currentMutableListtings.get(player) == ruleOption.getId();
	}
	
	public void allow(GameRuleOption choice) {
		if (choices.contains(choice)) {
			allowedChoices.add(choice);
		}
	}
	
	public void allow(int choiceID) {
		GameRuleOption choice = getOptionByID(choiceID);
		if (choice.getId() != -1) {
			allow(choice);
		}
	}
	
	public void allow(String choiceName) {
		GameRuleOption choice = getOptionByName(choiceName);
		if (choice.getId() != -1) {
			allow(choice);
		}
	}
	
	public void block(GameRuleOption choice) {
		if (choices.contains(choice)) {
			allowedChoices.remove(choice);
			for (Player player: game.getPlayers().keySet()) {
				if (isSelected(choice, player)) {
					boolean found = false;
					boolean finished = false;
					int nextID = choice.getId();
					while (!finished && !found) {
						nextID = (choice.getId() + 1) % choices.size();
						GameRuleOption nextChoice = getOptionByID(nextID);
						if (nextChoice == null || nextChoice.getId() == -1) continue;
						found = nextChoice.isAllowed();
						finished = (nextID == choice.getId());
					}
					if (found) {
						changeForPlayer(player, nextID);
					} else {
						changeForPlayer(player, blankChoice);
					}
				}
			}
		}
	}
	
	public void block(int choiceID) {
		GameRuleOption choice = getOptionByID(choiceID);
		if (choice.getId() != -1) {
			block(choice);
		}
	}
	
	public void block(String choiceName) {
		GameRuleOption choice = getOptionByName(choiceName);
		if (choice.getId() != -1) {
			block(choice);
		}
	}
	
	public void allowAll() {
		for (GameRuleOption choice: choices) {
			allowedChoices.add(choice);
		}
	}
	
	public Game getGame() {
		return game;
	}
	public int getId() {
		return id;
	}
	public String getScope() {
		return scope;
	}
	public String getName() {
		return name;
	}
	public MutableList<GameRuleOption> getChoices() {
		MutableList<GameRuleOption> setCopy = Lists.mutable.empty();
		setCopy.addAll(choices);
		return setCopy;
	}
	public MutableList<GameRuleOption> getAllowedChoices() {
		return allowedChoices;
	}
	public void setAllowedChoices(MutableList<GameRuleOption> allowedChoices) {
		this.allowedChoices = allowedChoices;
	}
	public int getDefaultMutableListting() {
		return defaultMutableListting;
	}
	public TreeMap<Player, Integer> getCurrentMutableListtings() {
		return currentMutableListtings;
	}
	public void setCurrentMutableListtings(TreeMap<Player, Integer> currentMutableListtings) {
		this.currentMutableListtings = currentMutableListtings;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameRule)) return false;
		GameRule otherRule = (GameRule) o;
		return id == otherRule.id;
	}

	@Override
	public int compareTo(GameRule otherRule) {
		if (otherRule == null) return 1;
		return id - otherRule.id;
	}
}
