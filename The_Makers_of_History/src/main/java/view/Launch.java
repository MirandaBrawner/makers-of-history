package view;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Objects;
import java.util.Queue;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import models.LivingStatus;
import models.actors.Actor;
import models.actors.Branch;
import models.actors.Culture;
import models.actors.Gender;
import models.actors.Government;
import models.actors.Office;
import models.actors.PeopleGroup;
import models.actors.Person;
import models.actors.Religion;
import models.ideas.Decision;
import models.ideas.Policy;
import models.ideas.Technology;
import models.resources.Resource;
import systemState.CustomCharTimerTask;
import systemState.Game;
import systemState.GameRule;
import systemState.GameRuleOption;
import systemState.GameTimerTask;
import systemState.Player;
import systemState.Scenario;
import systemState.Server;
import systemState.Utilities;
import systemState.comparators.PeopleGroupComparator;
import systemState.comparators.RegionComparator;
import systemState.comparators.TechComparator;
import view.images.ExternalAvatar;
import view.menus.IndexedButton;
import view.menus.PolicySlider;
import view.menus.Popup;

public class Launch extends Application {
	public Stage stage;
	public long playerCharID;
	public String lang = "eng";
	public double windowWidth = 1900;
	public double windowHeight = 1000;
	public double earthRadius = 100;
	public double cameraHeight = 100;
	public double cameraLat = 0;
	public double cameraLong = 0;
	public String terrainMapFile = Utilities.getFullPathName("resources/images/maps", "diffuse.png");
	public boolean mouseOverMap = true;
	public double leftPanelWidth = 0.25;
	public double topPanelWidth = 0.05;
	public Color panelColor = Color.DARKGREY;
	public Color borderColor = Color.DARKGREY.darker();
	public VBox sidePanel;
	public ScrollPane scrollPanel;
	public HBox topPanel;
	public int year = 1832;
	public int month = 1;
	public int day = 1;
	public double speed = 0;
	public double savedSpeed = 1;
	public boolean paused = true;
	public boolean hardPaused = true;
	public boolean readyToUnpause = false;
	public long intervalLength = 500;
	public double timeOfDay = 0;
	public Text dateText = new Text("");
	public Timer timer = new Timer();
	public GameTimerTask gameTimerTask = new GameTimerTask(this);
	public HashMap<String, Button> topbarTabs = new HashMap<>();
	public String[] tabNames = { "Individuals", "Population", "Places", "Governments", "Laws", "Organizations",
			"Economy", "Technology", "Nature" };
	public Server server = new Server();
	public Player player = new Player(server, "Anonymous Player", "userPassword", "no email available", "Unknown",
			"No Description Yet", "eng", new ExternalAvatar());
	public Person playerChar;
	public Game game = new Game(server, "gamePassword", player);
	public TextField mainSearchBar = new TextField();
	public Button mainSearchButton = new Button("Search");
	public VBox nameArea = new VBox();
	public Person selectedPerson = null;
	public Person oldSelectedPerson = null;
	public Image portrait;
	public ImageView portraitView = new ImageView();
	public Text nameText = new Text("No Person Selected");
	public Text personBirthdayText = new Text("");
	public Text familyText = new Text("");
	public Text personCultureText = new Text("");
	public Text personReligionText = new Text("");
	public String activeTab = "Individuals";
	public String oldActiveTab = "Individuals";
	public Government selectedGovernment = null;
	public VBox officeArea = new VBox();
	public Text partyText = new Text();
	public Text opinionText = new Text();
	public Text populationText = new Text();
	public Resource selectedResource = null;
	public Text resourceCategory = new Text();
	public Text yourInventoryHeader = new Text("In Your Inventory");
	public Text yourInventoryText = new Text();
	public VBox yourInventoryArea = new VBox(yourInventoryHeader, yourInventoryText);
	public Text countryInventoryHeader = new Text("In Your Nation");
	public Text countryInventoryText = new Text();
	public VBox countryInventoryArea = new VBox(countryInventoryHeader, countryInventoryText);
	public Text globalInventoryHeader = new Text("Everywhere");
	public Text globalInventoryText = new Text();
	public VBox globalInventoryArea = new VBox(globalInventoryHeader, globalInventoryText);
	public Button leftPanelInteractButton = new Button("Interact with This Person");
	public Text playerCharHeader = new Text("This is Your Character");
	public Line divider = new Line();
	public Button produceButton = new Button("Produce");
	public Button consumeButton = new Button("Consume");
	public Button giveButton = new Button("Give");
	public Button sellButton = new Button("Sell");
	public Button purchaseButton = new Button("Purchase");
	public Button requestButton = new Button("Request");
	public Button stealButton = new Button("Steal");
	public MutableList<PeopleGroup> selectedGroupList = Lists.mutable.empty();
	public VBox selectedCharPane = null;
	public models.places.Region selectedRegion = null;
	public Actor selectedLawmaker = null;
	public Slider speedSlider = new Slider(-5, 6, -5);
	public Color errorColor = new Color(0.75, 0, 0, 1);
	public Text achieveSafeText = new Text("Achievements are Allowed");
	public boolean visitedChooseAny = false;
	public boolean visitedCreateChar = false;
	public boolean charSelected = false;
	public boolean customCharReady = false;
	public Button[] allButtons = { mainSearchButton, leftPanelInteractButton, produceButton, consumeButton, giveButton,
			sellButton, purchaseButton, requestButton, stealButton };
	public MutableList<Culture> customCultures = Lists.mutable.empty();
	public MutableList<Religion> customReligions = Lists.mutable.empty();
	public TextField customFullNameField = new TextField();
	public TextField customShortNameField = new TextField();
	public TextField customFamilyNameField = new TextField();
	public TextField customAgeField = new TextField();
	public ChoiceBox<String> customMonthMenu = new ChoiceBox<>();
	public ChoiceBox<Integer> customBirthdayMenu = new ChoiceBox<>();
	public Slider genderSlider = new Slider(0, 1, 0.5);
	public Slider assignedSlider = new Slider(0, 1, 0.5);
	public Slider apparentSlider = new Slider(0, 1, 0.5);
	public Slider kinseySlider = new Slider(0, 1, 0.5);
	public Slider attractionSlider = new Slider(0, 1, 0.5);
	public boolean[] groupFiltersOn = { true, true };
	public boolean groupMenuTimerOn = false;
	public boolean playerCharDeathEventFired = false;
	public VBox popupWindow = new VBox();
	public StackPane ingameCanvas = new StackPane();
	public Queue<Decision> pendingDecisions = new ConcurrentLinkedQueue<>();
	public Person nextPlayerChar = null;
	public String[] relationCategories = { "spouses", "children", "parents", "siblings", "friends" };
	public String[] childGroupNames = { "twins", "triplets", "quadruplets", "quintuplets", "sextuplets", "septuplets",
			"octuplets" };
	public MutableList<Person> tempChildList = Lists.mutable.empty();
	public String[] playerCharButtonNames = { "Family", "Work and Education", "Politics", "Social Life", "Travel" };
	public int selectedCharButton = -1;
	public VBox viewActorMainPanel = new VBox();
	public Group group = new Group();

	public void styleButton(Button button, boolean searchButton) {
		if (searchButton) {
			button.setOnAction(e -> searchForAny(mainSearchBar.getText()));
		}
		button.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
		button.setTextFill(Color.WHITE);
		button.setBackground(
				new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
		button.setOnMouseEntered(e -> {
			button.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
			button.setTextFill(Color.BLACK);
		});
		button.setOnMouseExited(e -> {
			button.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			button.setTextFill(Color.WHITE);
		});
		button.setMinWidth(200);
		button.setMinHeight(30);
		button.setTextAlignment(TextAlignment.CENTER);
	}

	public void styleButton(Button button) {
		styleButton(button, true);
	}

	public int daysInMonth(int year, int month) {
		int[] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (month == 1 && Utilities.isGregorianLeapYear(year)) {
			return 29;
		} else {
			return days[month];
		}
	}

	public String getDateString() {
		String era = "CE";
		int dispYear = year;
		if (year < 1) {
			era = "BCE";
			dispYear = 1 - year;
		} else if (year >= 1000)
			era = "";
		String monthName = Utilities.gregorianMonthNames(lang, month);
		if (speed > 15) {
			return String.format("%s %d %s", monthName, dispYear, era);
		} else if (speed > 0.5) {
			return String.format("%d %s %d %s", day, monthName, dispYear, era);
		} else {
			String clockString;
			int hour = (int) (timeOfDay * 24);
			if (hour < 12) {
				int clockHour = ((hour + 11) % 12) + 1;
				clockString = String.format("%d:00 AM", clockHour);
			} else {
				int clockHour = ((hour - 1) % 12) + 1;
				clockString = String.format("%d:00 PM", clockHour);
			}
			return String.format("%s, %d %s %d %s", clockString, day, monthName, dispYear, era);
		}
	}

	public synchronized void advanceTime() {
		if (paused || !readyToUnpause)
			return;
		double timePassed = speed * intervalLength / 1000.0;
		timeOfDay += timePassed;
		int daysPassed = 0;
		while (timeOfDay >= 1) {
			daysPassed++;
			timeOfDay--;
		}
		for (int d = 0; d < daysPassed; d++) {
			if (daysInMonth(year, month) == day) {
				day = 1;
				if (month == 11) {
					month = 0;
					Platform.runLater(() -> {
						game.clearUpMemory(10000);
					});
					year++;
				} else {
					month++;
				}
			} else {
				day++;
			}
		}
		dateText.setText(getDateString());
		game.setIngameDate(new GregorianCalendar(year, month, day));
		Platform.runLater(() -> {
			for (Actor actor : game.getActors()) {
				if (actor instanceof Person) {
					Person p = (Person) actor;
					if (game.getLivingPeople().contains(p)) {
						p.getOlder(timePassed);
					}
				}
			}
		});
		updatePersonAgeText();
		if (playerChar.getLivingStatus() == LivingStatus.DEAD && !playerCharDeathEventFired) {
			playerCharDeathEvent();
		}
		if (activeTab.equals("Individuals") && this.selectedPerson != null) {
			Platform.runLater(() -> {
				viewActor(selectedPerson.getId());
			});
		}
		if (!tempChildList.isEmpty()) {
			Platform.runLater(() -> {
				playerHasChildEvent();
			});
		}
	}

	public void playerCharDeathEvent() {
		pause();
		playerCharDeathEventFired = true;
		String eventTitle = "You Have Died";
		int deathRuleSetting = game.getGameRuleById(0).getForPlayer(player).getId();
		nextPlayerChar = null;
		if (game.getLivingPeople().isEmpty()) {
			deathRuleSetting = 4;
		}
		// 0 Play as Legal Heir 1 Play as Random Living Family Member,
		// 2 Play as Random Living Character, 3 Play as Living Character of my Choice 4
		// End Game
		String heirString = "";
		switch (deathRuleSetting) {
		case 0: {
			if (playerChar.getHeirs().isEmpty()) {
				deathRuleSetting = 4;
			} else {
				nextPlayerChar = (Person) game.lookupActorByID(playerChar.getHeirs().get(0));
				heirString = nextPlayerChar.getFullName();
			}
			break;
		}
		case 1: {
			if (playerChar.getHeirs().isEmpty()) {
				deathRuleSetting = 4;
			} else {
				int seed = (int) (Math.random() * playerChar.getHeirs().size());
				nextPlayerChar = (Person) game.lookupActorByID(playerChar.getHeirs().get(seed));
			}
			break;
		}
		case 2: {
			int seed = (int) (Math.random() * game.getLivingPeople().size());
			nextPlayerChar = game.getLivingPeople().get(seed);
			break;
		}
		}
		String[] successionRuleArray = { "You will now play as your heir, " + heirString + ".",
				"You will now play as a randomly selected family member.",
				"You will now play as a randomly selected character.",
				"You will now play as a character of your choice.", "This is the end of the game." };
		String eventText = String.format("You, %s, have died at the age of %d.\n%s", playerChar.getFullName(),
				(int) playerChar.getAgeYears(), successionRuleArray[deathRuleSetting]);
		ImmutableList<String> pathList = null;
		if (playerChar.getPortraitPath() != null && !playerChar.getPortraitPath().isEmpty()) {
			pathList = Lists.immutable.of("portraits/" + playerChar.getPortraitPath());
		}
		ImmutableList<String> choiceList = Lists.immutable.of("Life is too short.");
		Decision deathDecision = new Decision(this.game, "player_char_death", eventTitle, eventText, pathList,
				choiceList, Lists.mutable.empty(), Lists.mutable.empty(), 0, Double.POSITIVE_INFINITY, true);
		Popup deathPopup = new Popup(this, deathDecision);
		popupWindow = deathPopup.display();
		Platform.runLater(() -> {
			ingameCanvas.getChildren().addAll(popupWindow);
		});
	}

	public void conditionalPause() {
		if (game.getGameRuleById(5).getForPlayer(player).getId() == 0) {
			pause();
		}
	}

	public void conditionalUnpause() {
		if (game.getGameRuleById(6).getForPlayer(player).getId() == 0) {
			unpause();
		}
	}

	public void playerHasChildEvent() {
		conditionalPause();
		String eventTitle = "A Child Is Born";
		String parentString = playerChar.getGender().getParentNounSingular("");
		String eventText;
		if (tempChildList.size() == 1) {
			String childString = tempChildList.get(0).getGender().getBabyNounSingular("apparent");
			eventText = String.format("You have just become the %s of a new child. "
					+ "The child appears to be %s. What shall they be called?", parentString, childString);
		} else {
			int groupSizeIndex = tempChildList.size() - 2;
			if (groupSizeIndex < 0)
				groupSizeIndex = 0;
			if (groupSizeIndex >= childGroupNames.length) {
				groupSizeIndex = childGroupNames.length - 1;
			}
			String groupSizeString = childGroupNames[groupSizeIndex];
			eventText = String.format("You have just become the %s of %s. " + "What shall they be called?",
					parentString, groupSizeString);
		}
		ImmutableList<String> pathList = Lists.immutable.empty();
		ImmutableList<String> choiceList = Lists.immutable.of("Ok");
		MutableList<String> inputStrings = tempChildList.collect(ch -> ch.getGivenName());
		MutableList<Person> participants = Lists.mutable.withAll(tempChildList);
		tempChildList = Lists.mutable.empty();
		Decision childDecision = new Decision(this.game, "player_has_child", eventTitle, eventText, pathList,
				choiceList, inputStrings, participants, 0, 365, false);
		Popup childPopup = new Popup(this, childDecision);
		popupWindow = childPopup.display();
		Platform.runLater(() -> {
			if (!ingameCanvas.getChildren().contains(popupWindow)) {
				ingameCanvas.getChildren().addAll(popupWindow);
			}
		});
	}

	public void interactWithCharEvent(Person character) {
		if (character == null || character.getFullName() == null)
			return;
		String eventTitle = character.getFullName();
		String eventText = "";
		MutableList<String> pathList = Lists.mutable.empty();
		if (character.getPortraitPath() != null && !character.getPortraitPath().isEmpty()) {
			pathList.add("portraits/" + character.getPortraitPath());
		}
		String shortName = character.shortDisplayName(playerChar);
		MutableList<String> buttonTemplates = Lists.mutable.of("Gather information about %s", // 0
				"Make a statement about %s", // 1
				"Arrange a meeting with %s", // 2
				"Write a letter to %s", // 3
				"Make a phone call to %s", // 4
				"Make a gift to %s", // 5
				"Take action against %s" // 6
		);
		MutableList<String> choiceList = buttonTemplates.collect(template -> String.format(template, shortName));
		choiceList.add("Never mind"); // 7
		MutableList<String> inputStrings = Lists.mutable.empty();
		MutableList<Person> participants = Lists.mutable.of(character);
		Decision interactDecision = new Decision(this.game, "interact_char", eventTitle, eventText, pathList,
				choiceList, inputStrings, participants, 0, 365, false);
		Popup interactPopup = new Popup(this, interactDecision);
		if (character.getLivingStatus() != LivingStatus.ALIVE) {
			for (int choiceIndex = 2; choiceIndex < buttonTemplates.size(); choiceIndex++) {
				interactPopup.getBlockedChoices().add(choiceIndex);
			}
		}
		popupWindow = interactPopup.display();
		Platform.runLater(() -> {
			ingameCanvas.getChildren().addAll(popupWindow);
		});
	}

	public void scaleAll(Node node, double factor) {
		node.setScaleX(factor);
		node.setScaleY(factor);
		node.setScaleZ(factor);
	}

	// Convert a point from spherical to rectangular coordinates.
	public Point3D sphereToRect(double radius, double lat, double lon) {
		double theta = Math.PI * (lat + 90) / 180;
		double phi = Math.PI * lon / 180;
		double x = radius * Math.sin(theta) * Math.sin(phi);
		double y = radius * Math.cos(theta);
		double z = radius * Math.sin(theta) * Math.cos(phi);
		return new Point3D(x, y, z);
	}

	// Convert a point from rectangular to spherical coordinates. [6]
	public double[] rectToSphere(Point3D target) {
		double x = target.getX();
		double y = target.getY();
		double z = target.getZ();
		double r = Math.sqrt(x * x + y * y + z * z);
		double azimuth = Math.atan2(z, x) * 180 / Math.PI;
		if (x == 0) {
			azimuth = y > 0 ? 90 : 270;
		}
		double elevation = Math.acos(y / r) * 180 / Math.PI;
		azimuth += 60;
		azimuth %= 360;
		azimuth -= 180;
		double[] results = { r, azimuth, elevation - 90 };
		if (r == 0) {
			return new double[] { 0, 0, 0 };
		}
		return results;
	}

	public Point3D intersectionPoint(Point3D source, Point3D direction, Sphere target, double maxDistance,
			double step) {
		for (double dist = 0; dist < maxDistance; dist += step) {
			Point3D point = source.add(direction.multiply(dist));
			if (point.distance(new Point3D(target.getTranslateX(), target.getTranslateY(),
					target.getTranslateZ())) <= target.getRadius()) {
				return point;
			}
		}
		return null;
	}

	public void adjustSpeed(Object obj) {
		Slider s = (Slider) obj;
		double oldSpeed = speed;
		speed = Math.pow(2, s.getValue()) - Math.pow(2, s.getMin());
		double threshold = 0.000001;
		if (oldSpeed < threshold && speed >= threshold) {
			unpause();
		} else if (oldSpeed >= threshold && speed < threshold) {
			pause();
		}
	}

	public void showSpeed(Slider s) {
		double min = Math.pow(2, s.getMin());
		double value = Math.log(speed + min) / Math.log(2);
		s.setValue(value);
	}

	public void updatePersonAgeText() {
		GregorianCalendar birthCal = selectedPerson.getDateOfBirth();
		String monthName = Utilities.gregorianMonthNames(player.getLanguage(), birthCal.get(GregorianCalendar.MONTH));
		int age = (int) selectedPerson.getAgeYears();
		String message = String.format("Born %d %s %d (Age %d)", birthCal.get(GregorianCalendar.DAY_OF_MONTH),
				monthName, birthCal.get(GregorianCalendar.YEAR), age);
		if (selectedPerson.getLivingStatus().equals(LivingStatus.DEAD)) {
			GregorianCalendar deathCal = selectedPerson.getDateOfDeath();
			String deathMonthName = Utilities.gregorianMonthNames(player.getLanguage(),
					deathCal.get(GregorianCalendar.MONTH));
			message += String.format("\nDied %d %s %d", deathCal.get(GregorianCalendar.DAY_OF_MONTH), deathMonthName,
					deathCal.get(GregorianCalendar.YEAR));
		}
		personBirthdayText.setText(message);
	}

	public void resetGroupSelect() {
		selectedGroupList = Lists.mutable.empty();
		PeopleGroup everyone = (PeopleGroup) game.lookupActorByID(50_000);
		for (int classIndex = 0; classIndex < game.getGroupClassNames().length; classIndex++) {
			selectedGroupList.add(everyone);
		}
	}

	public void changeSelectedGroup(PeopleGroup newGroup, String className) {
		for (int classNameIndex = 0; classNameIndex < game.getGroupClassNames().length; classNameIndex++) {
			if (game.getGroupClassNames()[classNameIndex].equals(className)) {
				selectedGroupList.set(classNameIndex, newGroup);
				return;
			}
		}
	}

	public void changeTab(String tab) {
		pause(false);
		oldActiveTab = activeTab;
		activeTab = tab;
		for (Button visualTab : topbarTabs.values()) {
			if (topbarTabs.containsKey(tab) && topbarTabs.get(tab) == visualTab) {
				visualTab.setBackground(
						new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(5), Insets.EMPTY)));
				;
				visualTab.setTextFill(Color.BLACK);
			} else {
				visualTab.setBackground(
						new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
				;
				visualTab.setTextFill(Color.WHITE);
			}
		}
		if (tab.equals("Individuals")) {
			viewActor(selectedPerson.getId());
		} else if (tab.equals("Governments")) {
			viewActor(selectedGovernment.getId());
		} else if (tab.equals("Economy")) {
			viewResource(selectedResource);
		} else if (tab.equals("Population")) {
			if (selectedGroupList == null || selectedGroupList.isEmpty()) {
				resetGroupSelect();
			}
			viewPopulation();
		} else if (tab.equals("Technology")) {
			viewTechList();
		} else if (tab.equals("Places")) {
			if (selectedRegion == null) {
				viewPlace(0);
			} else {
				viewPlace(selectedRegion.getId());
			}
		} else if (tab.equals("Laws")) {
			viewLaws(selectedGovernment.getId());
		}
		unpause(false);
	}

	public void stylePane(Pane pane) {
		/*
		 * if (pane != null && pane.getChildren() != null &&
		 * !pane.getChildren().isEmpty()) { for (Node node : pane.getChildren()) { if
		 * (node instanceof Text) { ((Text) node).setFont(Font.font("Times New Roman",
		 * FontWeight.NORMAL, 16)); } else if (node instanceof Pane) { stylePane((Pane)
		 * node); } } }
		 */
	}

	public boolean isPlayer(Person person) {
		return person != null && playerChar != null && playerChar.getId() == person.getId();
	}

	public void displayListOfIndividuals(Person person, String relation) {
		MutableList<Long> relativeList = Lists.mutable.empty();
		String labelString = "Relatives";
		switch (relation.toLowerCase()) {
		case "parents": {
			relativeList = person.getLegalParents();
			labelString = "Parents";
			break;
		}
		case "children": {
			relativeList = person.getLegalChildren();
			labelString = "Children";
			break;
		}
		case "siblings": {
			relativeList = person.getLegalSiblings();
			labelString = "Siblings";
			break;
		}
		case "spouses": {
			relativeList = person.getSpouses();
			labelString = "Married To";
			break;
		}
		case "friends": {
			relativeList = person.getFriends();
			labelString = "Friends";
			break;
		}
		default:
			return;
		}
		if (relativeList == null || relativeList.isEmpty())
			return;
		Text listLabel = new Text(labelString + ":");
		FlowPane listArea = new FlowPane(); // [9]
		listArea.setAlignment(Pos.CENTER);
		listArea.setHgap(5);
		listArea.setVgap(5);
		listArea.setPrefWrapLength(windowWidth * leftPanelWidth * 0.8);
		listArea.getChildren().add(listLabel);
		listLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
		listLabel.setTextAlignment(TextAlignment.CENTER);
		for (int i = 0; i < relativeList.size(); i++) {
			Long actorId = relativeList.get(i);
			Actor actor = game.lookupActorByID(actorId);
			if (actor == null || !(actor instanceof Person))
				continue;
			Person relative = (Person) actor;
			String relativeDispName = relative.getFullName();
			if (!relative.getCanonicalName().isEmpty()) {
				relativeDispName = relative.getCanonicalName();
			}
			if (i < relativeList.size() - 1) {
				relativeDispName += ", ";
			}
			Text relativeLink = new Text(relativeDispName);
			relativeLink.setOnMouseClicked(e -> {
				viewActor(actorId);
			});
			relativeLink.setUnderline(true);
			relativeLink.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			relativeLink.setTextAlignment(TextAlignment.CENTER);
			listArea.getChildren().add(relativeLink);
		}
		viewActorMainPanel.getChildren().add(listArea);
	}

	public void viewActor(long id) {
		pause(false);
		for (Actor actor : game.getActors()) {
			if (actor.getId() == id) {
				if (actor instanceof Person) {
					Person person = (Person) actor;
					selectedPerson = person;
					activeTab = "Individuals";
					boolean loadAll = !(activeTab.equals(oldActiveTab) && 
							Objects.equals(selectedPerson, oldSelectedPerson));
					if (!person.getPortraitPath().isEmpty()) {
						String imagePath = Utilities.getFullPathName("resources/images/portraits",
								person.getPortraitPath());
						portrait = new Image(imagePath);
						portraitView.setImage(portrait);
					}
					updatePersonAgeText();
					String nameString = person.getFullName();
					if (!person.getCanonicalName().isEmpty()) {
						nameString = person.getCanonicalName();
					}
					if (isPlayer(person)) {
						nameString += " (You)";
					}
					nameText.setText(nameString);
					nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
					String familyName = "";
					String cultureName = "";
					HBox cultureArea = new HBox();
					cultureArea.setSpacing(5);
					cultureArea.setAlignment(Pos.CENTER);
					String relName = "";
					HBox relArea = new HBox();
					relArea.setSpacing(5);
					relArea.setAlignment(Pos.CENTER);
					if (selectedPerson != null) {
						familyName = selectedPerson.getFamilyName();
						String cultureLabelString = selectedPerson.getCultures().size() > 1 ? "Cultures:" : "Culture:";
						Text cultureLabel = new Text(cultureLabelString);
						cultureArea.getChildren().add(cultureLabel);
						cultureLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
						cultureLabel.setTextAlignment(TextAlignment.CENTER);
						if (selectedPerson.getCultures() == null || selectedPerson.getCultures().isEmpty()) {
							cultureLabel.setText("Culture: Unknown");
						} else {
							for (int i = 0; i < selectedPerson.getCultures().size(); i++) {
								Culture currentCulture = selectedPerson.getCultures().get(i);
								cultureName = currentCulture.getAdjective();
								if (cultureName.endsWith("o/a")) {
									String ending = person.getGender().getGenderValue("actual") < 0.5 ? "o" : "a";
									cultureName = cultureName.replace("o/a", ending);
								}
								if (i < selectedPerson.getCultures().size() - 1) {
									cultureName += ", ";
								}
								Text cultureLink = new Text(cultureName);
								cultureLink.setOnMouseClicked(e -> {
									selectedGroupList = game.singleGroupToGroupList(currentCulture);
									changeTab("Population");
								});
								cultureLink.setUnderline(true);
								cultureLink.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
								cultureLink.setTextAlignment(TextAlignment.CENTER);
								cultureArea.getChildren().add(cultureLink);
							}

							personCultureText.setText(String.format("%s: %s",
									selectedPerson.getCultures().size() > 1 ? "Cultures" : "Culture", cultureName));
						}
						String relLabelString = selectedPerson.getCultures().size() > 1 ? "Religions:" : "Religion:";
						Text relLabel = new Text(relLabelString);
						relArea.getChildren().add(relLabel);
						relLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
						relLabel.setTextAlignment(TextAlignment.CENTER);
						if (selectedPerson.getReligions() == null || selectedPerson.getReligions().isEmpty()) {
							relLabel.setText("Religion: None");
						} else {
							for (int i = 0; i < selectedPerson.getReligions().size(); i++) {
								Religion currentRel = selectedPerson.getReligions().get(i);
								relName = currentRel.getAdjective();
								if (relName.endsWith("o/a")) {
									String ending = person.getGender().getGenderValue("actual") < 0.5 ? "o" : "a";
									relName = relName.replace("o/a", ending);
								}
								if (i < selectedPerson.getReligions().size() - 1) {
									relName += ", ";
								}
								Text relLink = new Text(relName);
								relLink.setOnMouseClicked(e -> {
									changeSelectedGroup(currentRel, "Religion");
									changeTab("Population");
								});
								relLink.setUnderline(true);
								relLink.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
								relLink.setTextAlignment(TextAlignment.CENTER);
								relArea.getChildren().add(relLink);
							}

							personReligionText.setText(String.format("%s: %s",
									selectedPerson.getReligions().size() > 1 ? "Religions" : "Religion", relName));
						}
					}
					personReligionText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					personReligionText.setTextAlignment(TextAlignment.CENTER);
					if (familyName != null && !familyName.isEmpty()) {
						familyText.setText(String.format("Family: %s", familyName));
					}
					familyText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					familyText.setTextAlignment(TextAlignment.CENTER);
					portraitView.setFitHeight(windowHeight * 0.2);
					portraitView.setPreserveRatio(true);
					nameText.setTextAlignment(TextAlignment.CENTER);
					nameArea = new VBox(nameText);
					nameArea.setAlignment(Pos.CENTER);
					nameArea.setPadding(new Insets(10, 10, 10, 10));
					nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
					nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
					if (!person.getOffices().isEmpty()) {
						officeArea = new VBox();
						for (Office office : person.getOffices()) {
							String officeString;
							double genderNumber = person.getGender().getGenderValue("gender");
							if (genderNumber >= 0.6) {
								officeString = office.getFeminineFullName();
							} else if (genderNumber <= 0.4) {
								officeString = office.getMasculineFullName();
							} else {
								officeString = office.getFullName();
							}
							Text officeText = new Text(officeString);
							officeText.setTextAlignment(TextAlignment.CENTER);
							officeText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
							officeArea.getChildren().add(officeText);
						}
						officeArea.setSpacing(5);
						officeArea.setAlignment(Pos.CENTER);
					}
					personBirthdayText.setTextAlignment(TextAlignment.CENTER);
					personBirthdayText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					mainSearchBar.setPromptText("Look up a character...");
					mainSearchBar.setOnKeyReleased(e -> {
						if (e.getCode().equals(KeyCode.ENTER)) {
							searchForAny(mainSearchBar.getText());
						}
					});
					viewActorMainPanel.getChildren().removeAll(viewActorMainPanel.getChildren());
					if (loadAll) {
						sidePanel.getChildren().removeAll(sidePanel.getChildren());
						sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton, viewActorMainPanel);
					} else {
						sidePanel.getChildren().set(2, viewActorMainPanel);
					}
					if (!person.getPortraitPath().isEmpty()) {
						viewActorMainPanel.getChildren().add(portraitView);
					}
					viewActorMainPanel.getChildren().add(nameArea);
					if (!person.getOffices().isEmpty()) {
						viewActorMainPanel.getChildren().add(officeArea);
					}
					if (!person.getParties().isEmpty()) {
						partyText.setText("Party: " + person.getParties().get(0).getShortName());
						partyText.setTextAlignment(TextAlignment.CENTER);
						partyText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
						viewActorMainPanel.getChildren().add(partyText);
					}
					viewActorMainPanel.getChildren().addAll(personBirthdayText, familyText, cultureArea, relArea);
					for (String cat : relationCategories) {
						displayListOfIndividuals(person, cat);
					}
					javafx.scene.layout.Region vertPad = new javafx.scene.layout.Region();
					vertPad.setMinSize(100, 30);
					viewActorMainPanel.getChildren().add(vertPad);
					VBox.setVgrow(vertPad, Priority.ALWAYS);
					if (loadAll) {
						if (isPlayer(person)) {
							String playerHeaderString = "Available Actions";
							Text header = new Text(playerHeaderString.toUpperCase());
							header.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 18));
							header.setTextAlignment(TextAlignment.CENTER);
							sidePanel.getChildren().add(header);
							for (int actionCatIndex = 0; actionCatIndex < playerCharButtonNames.length; actionCatIndex++) {
								String actionCategory = playerCharButtonNames[actionCatIndex];
								IndexedButton button = new IndexedButton(actionCatIndex, actionCategory);

								button.setMinWidth(windowWidth * leftPanelWidth * 0.8);
								button.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
								if (selectedCharButton == actionCatIndex) {
									button.setBackground(new Background(
											new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
									button.setTextFill(Color.BLACK);
								} else {
									button.setBackground(new Background(new BackgroundFill(Color.DARKGREY.darker(),
											new CornerRadii(5), Insets.EMPTY)));
									button.setTextFill(Color.WHITE);
								}
								button.setOnMouseEntered(e -> {
									button.setBackground(new Background(
											new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
									button.setTextFill(Color.BLACK);
									selectedCharButton = button.index;
								});
								button.setOnMouseExited(e -> {
									button.setBackground(new Background(new BackgroundFill(Color.DARKGREY.darker(),
											new CornerRadii(5), Insets.EMPTY)));
									button.setTextFill(Color.WHITE);
									selectedCharButton = -1;
								});
								sidePanel.getChildren().add(button);
							}
						} else {
							String opinionString = person.opinionOf(playerChar);
							opinionText.setText(String.format("\n" + "%s's Opinion of You: %s",
									person.shortDisplayName(playerChar), opinionString));
							leftPanelInteractButton
									.setText(String.format("Interact with %s", person.shortDisplayName(playerChar)));
							styleButton(leftPanelInteractButton, false);
							leftPanelInteractButton.setOnAction(e -> interactWithCharEvent(person));
							opinionText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
							Text ownCharText = new Text(
									String.format("View Your Own Character: %s", playerChar.getFullName()));
							ownCharText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
							ownCharText.setUnderline(true);
							ownCharText.setOnMouseClicked(e -> viewActor(playerChar.getId()));
							sidePanel.getChildren().addAll(opinionText, leftPanelInteractButton, ownCharText);
						}
						javafx.scene.layout.Region vertPad2 = new javafx.scene.layout.Region();
						vertPad2.setMinSize(30, 30);
						sidePanel.getChildren().add(vertPad2);
					}
					oldSelectedPerson = selectedPerson;
				} else if (actor instanceof Government) {
					Government govt = (Government) actor;
					selectedGovernment = govt;
					nameText.setText(govt.getFullName());
					nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
					nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
					nameArea = new VBox(nameText);
					nameArea.setAlignment(Pos.CENTER);
					nameArea.setPadding(new Insets(10, 10, 10, 10));
					activeTab = "Governments";
					mainSearchBar.setPromptText("Look up a government...");
					mainSearchButton.setOnAction(e -> searchForAny(mainSearchBar.getText()));
					mainSearchBar.setOnKeyReleased(e -> {
						if (e.getCode().equals(KeyCode.ENTER)) {
							searchForAny(mainSearchBar.getText());
						}
					});
					if (!govt.getPortraitPath().isEmpty()) {
						String imagePath = Utilities.getFullPathName("resources/images/emblems",
								govt.getPortraitPath());
						portrait = new Image(imagePath);
						portraitView.setImage(portrait);
						portraitView.setFitWidth(windowWidth * leftPanelWidth * 0.6);
						portraitView.setPreserveRatio(true);
					}
					sidePanel.getChildren().removeAll(sidePanel.getChildren());
					sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton);
					if (!govt.getPortraitPath().isEmpty()) {
						sidePanel.getChildren().addAll(portraitView);
					}
					populationText.setText("Population: " + Utilities.estimateNumber(govt.getPopulation()));
					populationText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					sidePanel.getChildren().addAll(nameText, officeArea, populationText);
					officeArea.setSpacing(5);
					officeArea.setAlignment(Pos.CENTER);
					officeArea.getChildren().removeAll(officeArea.getChildren());
					if (govt != null && govt.getBranches() != null) {
						for (Branch b : govt.getBranches()) {
							if (b != null && b.getLeadershipOffices() != null) {
								for (Office office : b.getLeadershipOffices()) {
									HBox officeRow = new HBox();
									Text officeNameText = new Text(office.getShortName() + ": ");
									officeNameText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
									officeNameText.setTextAlignment(TextAlignment.CENTER);
									officeRow.getChildren().add(officeNameText);
									Text incumbentText = new Text("Unknown");
									officeRow.setSpacing(5);
									officeRow.setAlignment(Pos.CENTER);
									if (!office.getIncumbents().isEmpty()) {
										for (int i = 0; i < office.getIncumbents().size(); i++) {
											Person inc = office.getIncumbents().get(i);
											String holderName = inc.getFullName();
											String linkString = holderName;
											if (i < office.getIncumbents().size() - 1) {
												linkString += ",";
											}
											Text linkText = new Text(linkString);
											linkText.setOnMouseClicked(e -> {
												selectedPerson = inc;
												changeTab("Individuals");
											});
											linkText.setUnderline(true);
											linkText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
											officeRow.getChildren().add(linkText);

										}
									} else {
										officeRow.getChildren().add(incumbentText);
									}
									officeArea.getChildren().add(officeRow);
								}
							}
						}
					}
				}
			}
		}
		oldActiveTab = activeTab;
		unpause(false);
	}

	public void viewPopulation() {
		pause(false);
		String compoundName = game.getCompoundName(selectedGroupList);
		if (lang.equals("eng")) {
			compoundName = compoundName.replaceAll("Gay Women", "Lesbians").replaceAll("Gay Woman", "Lesbian");
		}
		nameText.setText(compoundName);
		nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.6);
		nameArea = new VBox(nameText);
		nameArea.setAlignment(Pos.CENTER);
		nameArea.setPadding(new Insets(20, 20, 20, 20));
		nameArea.setMaxHeight(windowHeight * 0.4);
		activeTab = "Population";
		mainSearchBar.setPromptText("Look up a people group...");
		mainSearchButton.setOnAction(e -> searchForAny(mainSearchBar.getText()));
		mainSearchBar.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				searchForAny(mainSearchBar.getText());
			}
		});
		populationText
				.setText("Population: " + Utilities.estimateNumber(game.getPopulationFromList(selectedGroupList)));
		populationText.setTextAlignment(TextAlignment.CENTER);
		populationText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
		sidePanel.getChildren().removeAll(sidePanel.getChildren());
		sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton, nameArea, populationText);
		MutableList<Long> supergroups = game.getSupergroupsFromList(selectedGroupList);
		FlowPane parentRow = new FlowPane(); // [9]
		parentRow.setAlignment(Pos.CENTER);
		parentRow.setHgap(5);
		parentRow.setVgap(5);
		parentRow.setPrefWrapLength(windowWidth * leftPanelWidth * 0.8);
		if (game.isEveryone(selectedGroupList)) {
			Text humanDesc = new Text("The Entire Human Population");
			humanDesc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			parentRow.getChildren().add(humanDesc);
		} else if (game.isSecondLevel(selectedGroupList)) {
			PeopleGroup singleGroup = game.getSingleGroup(selectedGroupList);
			String currentClassName = singleGroup.getClass().getSimpleName();
			String groupDisplayName = "People Groups";
			for (int classNameIndex = 0; classNameIndex < game.getGroupClassNames().length; classNameIndex++) {
				if (game.getGroupClassNames()[classNameIndex].equals(currentClassName)) {
					groupDisplayName = game.getGroupFilterNames()[classNameIndex];
				}
			}
			Text groupTypeText = new Text(String.format("One of the World's %s", groupDisplayName));
			groupTypeText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			groupTypeText.setTextAlignment(TextAlignment.CENTER);
			sidePanel.getChildren().add(groupTypeText);
			Text humanLinkIntro = new Text("Part of the");
			humanLinkIntro.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			Text humanLink = new Text("General Population");
			humanLink.setOnMouseClicked(e -> {
				resetGroupSelect();
				viewPopulation();
			});
			humanLink.setUnderline(true);
			humanLink.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			parentRow.getChildren().addAll(humanLinkIntro, humanLink);
		} else {
			Text linkIntro = new Text("Part of the larger");
			linkIntro.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			parentRow.getChildren().add(linkIntro);
			int index = 0;
			for (long parentID : supergroups) {
				PeopleGroup parentGroup = (PeopleGroup) game.lookupActorByID(parentID);
				String parentString = parentGroup.getAdjective();
				Text parentText = new Text(parentString);
				parentText.setOnMouseClicked(e -> {
					selectedGroupList = game.singleGroupToGroupList(parentGroup);
					viewPopulation();
				});
				parentText.setUnderline(true);
				parentText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
				if (supergroups.size() >= 3 && index < supergroups.size() - 1) {
					parentString += ",";
				}
				parentRow.getChildren().addAll(parentText);
				if (index == supergroups.size() - 2) {
					Text andText = new Text("and");
					andText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					parentRow.getChildren().add(andText);
				}
				index++;
			}
			String closingString = "population";
			if (supergroups.size() > 1) {
				closingString += "s";
			}
			closingString += ".";
			Text closingText = new Text(closingString);
			closingText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			parentRow.getChildren().add(closingText);
		}
		sidePanel.getChildren().add(parentRow);
		MutableList<MutableList<Long>> subgroupIdSet = game.getSubgroupsFromList(selectedGroupList);
		if (subgroupIdSet != null && !(subgroupIdSet.isEmpty())) {
			sidePanel.getChildren().add(new Text(""));
			Text subgroupHeader = new Text("This group includes the following people groups:");
			subgroupHeader.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			subgroupHeader.setTextAlignment(TextAlignment.CENTER);
			sidePanel.getChildren().add(subgroupHeader);

			for (int catIndex = 0; catIndex < game.getGroupClassNames().length
					&& catIndex < selectedGroupList.size(); catIndex++) {
				sidePanel.getChildren().add(new Text(""));
				Text catHeader = new Text(game.getGroupFilterNames()[catIndex].toUpperCase());
				catHeader.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 18));
				catHeader.setTextAlignment(TextAlignment.CENTER);
				sidePanel.getChildren().add(catHeader);

				MutableList<Long> partialSubgroupList = Lists.mutable.empty();
				for (MutableList<Long> groupTuple : subgroupIdSet) {
					long currentChildId = groupTuple.get(catIndex);
					if (currentChildId != selectedGroupList.get(catIndex).getId()) {
						partialSubgroupList.add(currentChildId);
					}
				}
				partialSubgroupList.sort(new PeopleGroupComparator(game));

				for (long childId : partialSubgroupList) {
					PeopleGroup childGroup = (PeopleGroup) game.lookupActorByID(childId);
					Text childText = new Text(childGroup.getExonym());
					childText.setOnMouseClicked(e -> {
						selectedGroupList.set(game.getGroupClassNameIndex(childGroup), childGroup);
						viewPopulation();
					});
					childText.setUnderline(true);
					childText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
					childText.setTextAlignment(TextAlignment.CENTER);
					sidePanel.getChildren().addAll(childText);
				}
				sidePanel.getChildren().add(new Text(""));
			}
		}
		unpause(false);
	}

	public void viewLaws(long actorID) {
		pause(false);
		Actor enactor = game.lookupActorByID(actorID);
		if (enactor == null)
			return;
		if (!(enactor instanceof Government))
			return;
		Government govt = (Government) enactor;
		activeTab = "Laws";
		mainSearchBar.setPromptText("Search...");
		mainSearchButton.setOnAction(e -> searchForAny(mainSearchBar.getText()));
		mainSearchBar.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				searchForAny(mainSearchBar.getText());
			}
		});
		sidePanel.getChildren().removeAll(sidePanel.getChildren());
		sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton);
		nameText.setText("Laws of " + govt.getFullName());
		nameText.setTextAlignment(TextAlignment.CENTER);
		nameArea = new VBox(nameText);
		nameArea.setAlignment(Pos.CENTER);
		nameArea.setPadding(new Insets(10, 10, 10, 10));
		nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
		sidePanel.getChildren().add(nameArea);
		for (String catName : game.getLawCategories().keySet()) {
			Line catLine = new Line();
			catLine.setStartX(-.45 * windowWidth * leftPanelWidth);
			catLine.setEndX(.45 * windowWidth * leftPanelWidth);
			catLine.setStartY(0);
			catLine.setEndY(0);
			catLine.setStrokeWidth(2);
			catLine.setStroke(Color.DARKGREY);
			Text catText = new Text(catName);
			catText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			catText.setTextAlignment(TextAlignment.CENTER);
			MutableList<Policy> policyModelSubList = game.getLawCategories().get(catName);
			MutableList<VBox> policyDisplaySubList = Lists.mutable.empty();
			for (Policy policy : policyModelSubList) {
				Text policyNameText = new Text(policy.getName());
				policyNameText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
				policyNameText.setTextAlignment(TextAlignment.CENTER);
				VBox policyListItem = new VBox(policyNameText);
				double policyValue = policy.getCurrentValues().get(govt);
				if (policyValue < 0)
					continue;
				MutableList<String> policyValueList = Utilities.splitByNewLine(policy.valueToLabel(policyValue));
				for (String policyValueString : policyValueList) {
					Text policyValueText = new Text(policyValueString);
					policyValueText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					policyValueText.setTextAlignment(TextAlignment.CENTER);
					policyListItem.getChildren().add(policyValueText);
				}
				PolicySlider lawSlider = new PolicySlider(policy.getName(), 0, 1, policyValue);
				lawSlider.setMaxWidth(windowWidth * leftPanelWidth * 0.3);
				lawSlider.setMinWidth(windowWidth * leftPanelWidth * 0.3);
				lawSlider.setDisable(true);
				policyListItem.getChildren().add(lawSlider);
				policyListItem.setPadding(new Insets(5, 5, 5, 5));
				policyListItem.setAlignment(Pos.CENTER);
				policyDisplaySubList.add(policyListItem);
				lawSlider.setOnMouseDragged(e -> {
					double oldPolicyValue = policy.getCurrentValues().get(govt);
					double newPolicyValue = lawSlider.getValue();
					if (oldPolicyValue != newPolicyValue) {
						String oldLabel = policy.valueToLabel(oldPolicyValue);
						String newLabel = policy.valueToLabel(newPolicyValue);
						if (!oldLabel.equals(newLabel)) {
							FilteredList<Node> oldLabelSplitList = policyListItem.getChildren()
									.filtered(node -> node instanceof Text && !node.equals(policyNameText));
							for (Node itemSubnode : policyListItem.getChildren()) {
								if (itemSubnode instanceof Text && !itemSubnode.equals(policyNameText)) {
									policyListItem.getChildren().remove(itemSubnode);
								}
							}
							MutableList<String> newValueList = Utilities
									.splitByNewLine(policy.valueToLabel(newPolicyValue));
							for (int lineIndex = 0; lineIndex < newValueList.size(); lineIndex++) {
								String partialValueString = newValueList.get(lineIndex);
								if (lineIndex < oldLabelSplitList.size()) {
									Text partialText = (Text) (policyListItem.getChildren().get(1 + lineIndex));
									partialText.setText(partialValueString);
								} else {
									Text policyValueText = new Text(partialValueString);
									policyValueText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
									policyValueText.setTextAlignment(TextAlignment.CENTER);
									policyListItem.getChildren().add(lineIndex + 1, policyValueText);
								}
							}
							for (int lineIndex = newValueList.size(); lineIndex < oldLabelSplitList
									.size(); lineIndex++) {
								policyListItem.getChildren().remove(lineIndex + 1);
							}
						}
					}
				});
				lawSlider.setOnMouseReleased(e -> {
					game.getPolicyByName(policy.getName()).getCurrentValues().put(govt, lawSlider.getValue());
					viewLaws(actorID);
				});
			}
			if (policyDisplaySubList.isEmpty())
				continue;
			sidePanel.getChildren().add(catLine);
			sidePanel.getChildren().add(catText);
			for (VBox policyListItem : policyDisplaySubList) {
				sidePanel.getChildren().add(policyListItem);
			}
		}
		unpause(false);
	}

	public void viewResource(Resource res) {
		pause(false);
		nameText.setText(res.getName());
		nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		Text[] invHeaders = { yourInventoryHeader, countryInventoryHeader, globalInventoryHeader };
		Text[] inventories = { yourInventoryText, countryInventoryText, globalInventoryText };
		VBox[] invAreas = { yourInventoryArea, countryInventoryArea, globalInventoryArea };
		for (int inv = 0; inv < 3; inv++) {
			String invString = "";
			double stockpile = 62587;
			double produced = 347;
			double consumed = 248;
			double imported = 34;
			double exported = 19;
			double balance = produced - consumed;
			if (inv < 2) {
				balance += (imported - exported);
				invString = String.format("Stockpile: %.2f\n" + "Produced: %.2f\n Consumed: %.2f\n"
						+ "Imported: %.2f\n Exported: %.2f\n " + "Balance: %.2f", stockpile, produced, consumed,
						imported, exported, balance);
			} else {
				invString = String.format("Stockpile: %.2f\n" + "Produced: %.2f\nConsumed: %.2f\n" + "Balance: %.2f",
						stockpile, produced, consumed, balance);
			}
			inventories[inv].setText(invString);
			inventories[inv].setTextAlignment(TextAlignment.CENTER);
			inventories[inv].setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			invAreas[inv].setAlignment(Pos.CENTER);
			invAreas[inv].setSpacing(2);
			invHeaders[inv].setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
		}
		produceButton.setText(String.format("Adjust %s Production", res.getName()));
		consumeButton.setText(String.format("Adjust %s Consumption", res.getName()));
		giveButton.setText(String.format("Give Away Some of Your %s", res.getName()));
		sellButton.setText(String.format("Sell Some of Your %s", res.getName()));
		requestButton.setText(String.format("Request Some More %s", res.getName()));
		purchaseButton.setText(String.format("Attempt to Buy Some %s", res.getName()));
		stealButton.setText(String.format("Attempt to Steal Some %s", res.getName()));
		Button[] resButtons = { produceButton, consumeButton, giveButton, sellButton, requestButton, purchaseButton,
				stealButton };
		for (Button rb : resButtons) {
			rb.setMinWidth(windowWidth * leftPanelWidth * 0.6);
			rb.setMaxWidth(windowWidth * leftPanelWidth * 0.6);
		}
		mainSearchBar.setPromptText("Look up a resource or inventory...");
		mainSearchBar.setOnKeyPressed(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				searchForAny(mainSearchBar.getText());
			}
		});
		mainSearchButton.setOnAction(e -> searchForAny(mainSearchBar.getText()));
		sidePanel.getChildren().removeAll(sidePanel.getChildren());
		sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton, nameText, yourInventoryArea,
				countryInventoryArea, globalInventoryArea, produceButton, consumeButton, giveButton, sellButton,
				requestButton, purchaseButton, stealButton);
		sidePanel.setSpacing(10);
		stylePane(sidePanel);
		stylePane(topPanel);
		unpause(false);
	}

	public void viewTechList() {
		savedSpeed = speed;
		speed = 0;
		showSpeed(speedSlider);
		paused = true;
		sidePanel.getChildren().removeAll(sidePanel.getChildren());
		nameText.setText("MutableList of Innovations");
		nameText.setTextAlignment(TextAlignment.CENTER);
		nameArea = new VBox(nameText);
		nameArea.setAlignment(Pos.CENTER);
		nameArea.setPadding(new Insets(10, 10, 10, 10));
		nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
		sidePanel.getChildren().add(nameArea);
		MutableList<Technology> techList = Lists.mutable.empty();
		game.getTechnologies().forEach(tech -> techList.add(tech));
		techList.sort(new TechComparator());
		techList.stream().filter(t -> t.getYear() < year).forEachOrdered(tech -> {
			Text techNameText = new Text(tech.getName());
			techNameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			techNameText.setTextAlignment(TextAlignment.CENTER);
			Text techYearText = new Text("Invented " + Utilities.estimatedYearToString(tech.getYear(), 1480));
			techYearText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			techYearText.setTextAlignment(TextAlignment.CENTER);
			VBox techListItem = new VBox(techNameText, techYearText);
			techListItem.setPadding(new Insets(5, 5, 5, 5));
			techListItem.setAlignment(Pos.CENTER);
			sidePanel.getChildren().add(techListItem);
		});
		speed = savedSpeed;
		showSpeed(speedSlider);
		paused = false;
	}

	public void viewPlace(long placeID) {
		pause(false);
		models.places.Region region = game.lookupRegionByID(placeID);
		if (region == null)
			return;
		sidePanel.getChildren().removeAll(sidePanel.getChildren());
		mainSearchBar.setPromptText("Look up a place...");
		mainSearchBar.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				searchForAny(mainSearchBar.getText());
			}
		});
		personReligionText.setTextAlignment(TextAlignment.CENTER);
		mainSearchButton.setOnAction(e -> searchForAny(mainSearchBar.getText()));
		sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton);
		nameText.setText(region.getName());
		nameText.setTextAlignment(TextAlignment.CENTER);
		nameArea = new VBox(nameText);
		nameArea.setAlignment(Pos.CENTER);
		nameArea.setPadding(new Insets(10, 10, 10, 10));
		nameText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
		sidePanel.getChildren().add(nameArea);
		if (region.getParents() != null && !region.getParents().isEmpty()) {
			MutableList<Long> supergroups = region.getParents();
			MutableList<Long> regionParentList = Lists.mutable.empty();
			supergroups.forEach(reg -> regionParentList.add(reg));
			FlowPane parentRow = new FlowPane(); // [9]
			parentRow.setAlignment(Pos.CENTER);
			parentRow.setHgap(5);
			parentRow.setVgap(5);
			parentRow.setPrefWrapLength(windowWidth * leftPanelWidth * 0.8);
			Text linkIntro = new Text("Part of ");
			linkIntro.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			parentRow.getChildren().add(linkIntro);
			for (int index = 0; index < supergroups.size(); index++) {
				long parentID = regionParentList.get(index);
				models.places.Region parentRegion = (models.places.Region) game.lookupRegionByID(parentID);
				String parentString = parentRegion.getName();
				Text parentText = new Text(parentString);
				parentText.setOnMouseClicked(e -> viewPlace(parentID));
				parentText.setUnderline(true);
				parentText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
				if (supergroups.size() >= 3 && index < supergroups.size() - 1) {
					parentText.setText(parentText.getText() + ", ");
				} else if (index == supergroups.size() - 1) {
					parentText.setText(parentText.getText() + ".");
				}
				parentRow.getChildren().addAll(parentText);
				if (index == supergroups.size() - 2) {
					Text andText = new Text("and");
					andText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					parentRow.getChildren().add(andText);
				}
			}
			sidePanel.getChildren().add(parentRow);
		}
		if (region.getChildren() != null && !region.getChildren().isEmpty()) {
			MutableList<Long> childList = Lists.mutable.empty();
			region.getChildren().forEach(reg -> childList.add(reg));
			childList.sort(new RegionComparator(game));
			Text subgroupHeader = new Text("This region includes the following areas:");
			subgroupHeader.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			subgroupHeader.setTextAlignment(TextAlignment.CENTER);
			sidePanel.getChildren().add(subgroupHeader);
			for (long childID : childList) {
				models.places.Region childGroup = (models.places.Region) game.lookupRegionByID(childID);
				Text childText = new Text(childGroup.getName());
				childText.setOnMouseClicked(e -> viewPlace(childID));
				childText.setUnderline(true);
				childText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
				childText.setTextAlignment(TextAlignment.CENTER);
				sidePanel.getChildren().addAll(childText);
			}
			sidePanel.getChildren().add(new Text("\n\n"));
		}
		unpause(false);
	}

	public void searchForActor(String actorName) {
		Actor actor = game.lookupActorByName(actorName);
		if (actor != null) {
			viewActor(actor.getId());
		}
	}

	public void searchForGovernment(String govtName) {
		Government govt = game.getGovernmentByName(govtName);
		if (govt != null) {
			viewActor(govt.getId());
		}
	}

	public void searchForResource(String resName) {
		Resource res = game.getResourceByName(resName);
		if (res != null) {
			viewResource(res);
		}
	}

	public void searchForAny(String entityName) {
		Actor actor = game.lookupActorByName(entityName);
		if (actor != null) {
			if (activeTab.equals("Laws")) {
				viewLaws(actor.getId());
			} else if (actor instanceof Person || actor instanceof Government) {
				viewActor(actor.getId());
			} else if (actor instanceof PeopleGroup) {
				PeopleGroup newGroup = (PeopleGroup) actor;
				selectedGroupList = game.singleGroupToGroupList(newGroup);
				viewPopulation();
			}
			return;
		}
		models.places.Region region = game.lookupRegionByName(entityName);
		if (region != null) {
			viewPlace(region.getId());
			return;
		}
		Resource res = game.getResourceByName(entityName);
		if (res != null) {
			viewResource(res);
			return;
		}
	}

	public void pause(boolean hardPause) {
		if (paused)
			return;
		paused = true;
		if (hardPause) {
			readyToUnpause = false;
			savedSpeed = speed;
			speed = 0;
			showSpeed(speedSlider);
		}
	}

	public void pause() {
		pause(true);
	}

	public void unpause(boolean hardPause) {
		if (!paused && readyToUnpause)
			return;
		paused = false;
		if (hardPause) {
			readyToUnpause = true;
			hardPaused = false;
			speed = savedSpeed;
			showSpeed(speedSlider);
		}
	}

	public void unpause() {
		unpause(true);
	}

	public void pauseUnpause(boolean hardPause) {
		if (paused) {
			unpause(hardPause);
		} else {
			pause(hardPause);
		}
	}

	public void pauseUnpause() {
		pauseUnpause(true);
	}

	public void closeNextPopup() {
		if (ingameCanvas.getChildren().size() >= 3) {
			ingameCanvas.getChildren().remove(ingameCanvas.getChildren().size() - 1);
		}
	}
	
	public void jumpToEarthLocation(double newLat, double newLong) {
		double adjLong = -30 - newLong;
		double latDiff = cameraLat - newLat;
		double longDiff = cameraLong  - adjLong;
		cameraLat = newLat;
		cameraLong = adjLong;
		Rotate rotate = new Rotate(latDiff, 0, 0, 0, Rotate.X_AXIS);
		group.getTransforms().removeAll();
		group.getTransforms().add(rotate);
		group.setRotationAxis(new Point3D(0, 1, 0));
		group.setRotate(group.getRotate() - longDiff);
	}

	public void startScenario(GregorianCalendar startDate) {
		timer.cancel();
		timer = new Timer();
		game.updatePlayerChar(this);

		// Set up the background
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(panelColor);
		curtainCanvas.getChildren().add(curtain);

		// Set up the game model
		MutableList<Resource> resList = Lists.mutable.empty();
		resList.addAll(game.getResources());
		selectedResource = resList.get((int) (Math.random() * resList.size()));

		// Create a sphere [1]
		Sphere sphere = new Sphere(earthRadius);

		// Apply the map to the surface of the sphere [3]
		PhongMaterial earthMaterial = new PhongMaterial();
		earthMaterial.setDiffuseMap(new Image(terrainMapFile, 3072, 2048, true, false));
		sphere.setMaterial(earthMaterial);

		// Create a camera [1]
		PerspectiveCamera camera = new PerspectiveCamera(true);
		camera.setNearClip(.001);
		camera.setFarClip(1000);

		// Center the camera on the sphere. [2]
		group = new Group();
		group.setAutoSizeChildren(false);
		group.getChildren().add(camera);
		group.setRotationAxis(new Point3D(0, 1, 0));
		group.setRotate(180);
		group.setTranslateZ(200);

		// Set up the map display [1]
		Group totalGroup = new Group();
		totalGroup.getChildren().addAll(sphere, group);
		SubScene subscene = new SubScene(totalGroup, windowWidth * (1 - leftPanelWidth) - 20,
				windowHeight * (1 - topPanelWidth) - 20);
		subscene.setFill(Color.BLACK);
		subscene.setCamera(camera);
		subscene.setOnMouseEntered(e -> {
			mouseOverMap = true;
			subscene.requestFocus();
		});
		subscene.setOnMouseExited(e -> {
			mouseOverMap = false;
		});

		// Set up the map viewing area
		Pane mapPane = new Pane(subscene);
		mapPane.setMinWidth(windowWidth * (1 - leftPanelWidth));
		mapPane.setMinHeight(windowHeight * (1 - topPanelWidth));
		mapPane.setMaxWidth(windowWidth * (1 - leftPanelWidth));
		mapPane.setMaxHeight(windowHeight * (1 - topPanelWidth));
		mapPane.setBackground(new Background(new BackgroundFill(borderColor, CornerRadii.EMPTY, Insets.EMPTY)));
		mapPane.getChildren().get(0).setTranslateX(10);
		mapPane.getChildren().get(0).setTranslateY(10);

		// Set up the side person panel
		sidePanel = new VBox();
		mainSearchBar.setOnKeyReleased(e -> {
			if (e.getCode().equals(KeyCode.ENTER)) {
				searchForActor(mainSearchBar.getText());
			}
		});

		mainSearchButton.setOnAction(e -> searchForActor(mainSearchBar.getText()));
		nameText.setWrappingWidth(windowWidth * leftPanelWidth * 0.8);
		if (activeTab.equals("Individuals")) {
			portraitView.setFitHeight(windowHeight * 0.2);
			portraitView.setPreserveRatio(true);
			nameText.setTextAlignment(TextAlignment.CENTER);
			nameArea = new VBox(nameText);
			nameArea.setAlignment(Pos.CENTER);
			nameArea.setPadding(new Insets(10, 10, 10, 10));
			personBirthdayText.setTextAlignment(TextAlignment.CENTER);
			mainSearchBar.setPromptText("Look up a character...");
			mainSearchBar.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			mainSearchBar.setFocusTraversable(false);
			mainSearchBar.setMinWidth(windowWidth * leftPanelWidth * 0.65);
			personCultureText.setTextAlignment(TextAlignment.CENTER);
			personReligionText.setTextAlignment(TextAlignment.CENTER);
			mainSearchButton.setOnAction(e -> searchForActor(mainSearchBar.getText()));
			mainSearchButton.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			sidePanel.getChildren().removeAll(sidePanel.getChildren());
			sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton, portraitView, nameArea, personBirthdayText,
					familyText, personCultureText, personReligionText);

		} else if (activeTab.equals("Governments")) {
			mainSearchBar.setPromptText("Look up a government...");
			sidePanel.getChildren().removeAll(sidePanel.getChildren());
			sidePanel.getChildren().addAll(mainSearchBar, mainSearchButton, nameText);

		} else if (activeTab.equals("Economy")) {
			mainSearchBar.setPromptText("Look up a person, organization, or resource...");
			sidePanel.getChildren().removeAll(sidePanel.getChildren());
		}

		// Display the generic side panel
		mainSearchBar.setAlignment(Pos.CENTER);
		mainSearchBar.setMaxWidth(windowWidth * leftPanelWidth * 0.4);
		mainSearchBar.setMinWidth(windowWidth * leftPanelWidth * 0.4);
		sidePanel.setSpacing(5);
		sidePanel.setAlignment(Pos.TOP_CENTER);
		sidePanel.setBackground(new Background(new BackgroundFill(panelColor, CornerRadii.EMPTY, Insets.EMPTY)));
		viewActorMainPanel.setAlignment(Pos.CENTER);
		viewActorMainPanel.setSpacing(5);
		scrollPanel = new ScrollPane(sidePanel);
		scrollPanel.setMinWidth(windowWidth * leftPanelWidth);
		scrollPanel.setMinHeight(windowHeight * (1 - topPanelWidth));
		scrollPanel.setMaxWidth(windowWidth * leftPanelWidth);
		scrollPanel.setMaxHeight(windowHeight * (1 - topPanelWidth));
		scrollPanel.setFitToHeight(true);
		scrollPanel.setFitToWidth(true);
		scrollPanel.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollPanel.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		HBox mapAndSidePanel = new HBox(scrollPanel, mapPane);
		oldActiveTab = "";
		viewActor(10001);
		oldActiveTab = "";
		viewActor(playerChar.getId());

		// Set up the top panel
		dateText.setText(getDateString());
		dateText.setTextAlignment(TextAlignment.CENTER);
		dateText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 17));
		speedSlider.setOnDragDetected(e -> adjustSpeed(e.getSource()));
		speedSlider.setOnMouseDragged(e -> adjustSpeed(e.getSource()));
		speedSlider.setOnMouseDragReleased(e -> adjustSpeed(e.getSource()));
		speedSlider.setMaxSize(200, 50);
		speedSlider.setMinWidth(200);
		VBox dateArea = new VBox(dateText, speedSlider);
		dateArea.setSpacing(7);
		dateArea.setAlignment(Pos.CENTER);
		dateArea.setMinWidth(360);
		dateArea.setMaxWidth(360);
		topPanel = new HBox();
		for (int buttonIndex = 0; buttonIndex < tabNames.length; buttonIndex++) {
			String tabName = tabNames[buttonIndex];
			Button button = new Button(tabName);
			button.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			button.setTextFill(Color.WHITE);
			button.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			button.setOnMouseEntered(e -> {
				button.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
				button.setTextFill(Color.BLACK);
			});
			button.setOnMouseExited(e -> {
				if (activeTab.equals(tabName)) {
					button.setBackground(
							new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(5), Insets.EMPTY)));
					button.setTextFill(Color.BLACK);
				} else {
					button.setBackground(new Background(
							new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
					button.setTextFill(Color.WHITE);
				}
			});
			button.setOnAction(e -> changeTab(tabName));
			topbarTabs.put(tabName, button);
			topPanel.getChildren().add(button);

			javafx.scene.layout.Region tabSpace = new javafx.scene.layout.Region();
			HBox.setHgrow(tabSpace, Priority.ALWAYS); // [10]
			topPanel.getChildren().add(tabSpace);
		}
		topPanel.getChildren().add(dateArea);
		topPanel.setAlignment(Pos.CENTER);
		topPanel.setBackground(new Background(new BackgroundFill(panelColor, CornerRadii.EMPTY, Insets.EMPTY)));
		topPanel.setMinWidth(windowWidth);
		topPanel.setMinHeight(windowHeight * topPanelWidth);
		topPanel.setMaxWidth(windowWidth);
		topPanel.setMaxHeight(windowHeight * topPanelWidth);
		topPanel.setPadding(new Insets(5, 5, 5, windowWidth * leftPanelWidth));
		VBox overallPane = new VBox(topPanel, mapAndSidePanel);
		curtainCanvas.getChildren().add(overallPane);
		ingameCanvas = curtainCanvas;
		Scene scene = new Scene(curtainCanvas, windowWidth, windowHeight);
		resetGroupSelect();
		changeTab("Individuals");

		// Set up the timed events [8]
		timer.scheduleAtFixedRate(gameTimerTask, intervalLength, intervalLength);

		// Add zoom and rotation controls [1] [4]
		stage.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
			if (mouseOverMap) {

				switch (e.getCode()) {

				// Pause or unpause when the spacebar is pressed.
				case SPACE: {
					pauseUnpause();
					break;
				}

				// Zoom in when R is pressed [1]
				case R: {
					double newAltitude = 8;
					if (cameraHeight > 8 * (16.0 / 15.0)) {
						newAltitude = cameraHeight * (15.0 / 16.0);
					}
					cameraHeight = newAltitude;
					break;
				}
				// Zoom out when F is pressed [1]
				case F: {
					double newAltitude = 300;
					if (cameraHeight < 300 * (15.0 / 16.0)) {
						newAltitude = cameraHeight * (16.0 / 15.0);
					}
					cameraHeight = newAltitude;
					break;
				}
				// Move west when A is pressed. [4]
				case A: {
					double rotationAmount = cameraHeight / 50;
					cameraLong += rotationAmount;
					if (cameraLong > 180) {
						cameraLong -= 360;
					} else if (cameraLong <= -180) {
						cameraLong += 360;
					}
					group.setRotationAxis(new Point3D(0, 1, 0));
					group.setRotate(group.getRotate() + rotationAmount);
					break;
				}
				// Move east when D is pressed [4]
				case D: {
					double rotationAmount = cameraHeight / 50;
					cameraLong -= rotationAmount;
					if (cameraLong > 180) {
						cameraLong -= 360;
					} else if (cameraLong <= -180) {
						cameraLong += 360;
					}
					group.setRotationAxis(new Point3D(0, 1, 0));
					group.setRotate(group.getRotate() - rotationAmount);
					break;
				}
				// Move north when W is pressed. [4]
				case W: {
					double rotationAmount = cameraHeight / 50;
					double newLat = 85;
					if (cameraLat < 85 - rotationAmount) {
						newLat = cameraLat + rotationAmount;
						cameraLat = newLat;
						Rotate rotate = new Rotate(-rotationAmount, 0, 0, 0, Rotate.X_AXIS);
						group.getTransforms().removeAll();
						group.getTransforms().add(rotate);
					}
					break;
				}
				// Move south when S is pressed. [4]
				case S: {
					double rotationAmount = cameraHeight / 50;
					double newLat = -85;
					if (cameraLat > -85 + rotationAmount) {
						newLat = cameraLat - rotationAmount;
						cameraLat = newLat;
						Rotate rotate = new Rotate(rotationAmount, 0, 0, 0, Rotate.X_AXIS);
						group.getTransforms().removeAll();
						group.getTransforms().add(rotate);
					}
					break;
				}
				case J: {
					jumpToEarthLocation(32, -81);
				}
				default:
					break;
				}
				Point3D cameraPos = sphereToRect(cameraHeight + earthRadius, cameraLat, cameraLong);
				group.setTranslateX(cameraPos.getX());
				group.setTranslateY(cameraPos.getY());
				group.setTranslateZ(cameraPos.getZ());
			}
		});
		stage.setScene(scene);
		stage.show();
	}

	public void updateAchieveSafeText() {
		if (game.getAchieveSafePlayers().contains(player)) {
			achieveSafeText.setText("Achievements are Allowed");
			achieveSafeText.setFill(Color.BLACK);
		} else {
			achieveSafeText.setText("Achievements are Blocked");
			achieveSafeText.setFill(errorColor);
		}
	}

	public void chooseRules(Scenario scenario) {
		GregorianCalendar startDate = scenario.getDate();
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(Color.SLATEGREY);
		curtainCanvas.getChildren().add(curtain);

		Text rulesTitleText = new Text("Game Settings");
		rulesTitleText.setTextAlignment(TextAlignment.CENTER);
		rulesTitleText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 26));
		VBox rulesTitleArea = new VBox(rulesTitleText);
		rulesTitleArea.setAlignment(Pos.CENTER);
		rulesTitleArea.setPadding(new Insets(15, 0, 0, 0));

		Text playerCharText = new Text("You have not Selected a Character");
		if (playerChar != null && playerChar.getFullName() != null && !playerChar.getFullName().isEmpty()) {
			String playerNameIntroString = "You are Playing as";
			playerCharText.setText(String.format("%s %s", playerNameIntroString, playerChar.getFullName()));
		}
		playerCharText.setTextAlignment(TextAlignment.CENTER);
		playerCharText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 18));
		VBox playerMenu = new VBox(rulesTitleText, playerCharText);

		Button backButton = new Button("Back");
		backButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
		backButton.setTextFill(Color.WHITE);
		backButton.setBackground(
				new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
		backButton.setOnMouseEntered(e -> {
			backButton.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
			backButton.setTextFill(Color.BLACK);
		});
		backButton.setOnMouseExited(e -> {
			backButton.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			backButton.setTextFill(Color.WHITE);
		});
		backButton.setMinWidth(200);
		backButton.setMinHeight(30);
		backButton.setTextAlignment(TextAlignment.CENTER);
		backButton.setOnAction(e -> {
			if (visitedCreateChar) {
				createChar(scenario);
			} else if (visitedChooseAny) {
				chooseAnyChar(scenario);
			} else {
				chooseKeyChar(scenario);
			}
		});

		Button playButton = new Button("Next");
		playButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
		playButton.setTextFill(Color.WHITE);
		playButton.setBackground(
				new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
		playButton.setOnMouseEntered(e -> {
			playButton.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
			playButton.setTextFill(Color.BLACK);
		});
		playButton.setOnMouseExited(e -> {
			playButton.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			playButton.setTextFill(Color.WHITE);
		});
		playButton.setMinWidth(200);
		playButton.setMinHeight(30);
		playButton.setTextAlignment(TextAlignment.CENTER);
		playButton.setOnAction(e -> {
			startScenario(startDate);
		});

		VBox rulesArea = new VBox();
		rulesArea.setAlignment(Pos.CENTER);
		rulesArea.setMaxWidth(windowWidth * 0.75);
		rulesArea.setPrefHeight(windowHeight * 6);
		rulesArea.setMaxHeight(Double.MAX_VALUE);
		rulesArea.setSpacing(7);
		MutableList<GameRule> ruleList = Lists.mutable.empty();
		ruleList.addAll(game.getGameRules());
		for (int ruleIndex = 0; ruleIndex < ruleList.size(); ruleIndex++) {
			GameRule gameRule = ruleList.get(ruleIndex);
			if (gameRule.getScope().equals("game"))
				continue;
			MutableList<String> ruleLabelStringList = Utilities.splitByNewLine(gameRule.getName());
			if (ruleLabelStringList.isEmpty())
				continue;
			Text ruleLabel = new Text(ruleLabelStringList.get(0));
			ruleLabel.setTextAlignment(TextAlignment.LEFT);
			ruleLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			VBox ruleLabelArea = new VBox(ruleLabel);
			ruleLabelArea.setAlignment(Pos.TOP_LEFT);
			ruleLabelArea.setSpacing(2);
			for (int ruleLabelLineIndex = 1; ruleLabelLineIndex < ruleLabelStringList.size(); ruleLabelLineIndex++) {
				Text ruleLabelLine = new Text(ruleLabelStringList.get(ruleLabelLineIndex));
				ruleLabelLine.setTextAlignment(TextAlignment.LEFT);
				ruleLabelLine.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
				ruleLabelArea.getChildren().add(ruleLabelLine);
			}
			ruleLabel.setTextAlignment(TextAlignment.LEFT);
			ruleLabel.setFont(Font.font("Times New Roman", FontWeight.BOLD, 16));
			HBox ruleRow = new HBox(ruleLabelArea);
			ruleRow.setAlignment(Pos.TOP_LEFT);
			ruleRow.setMaxWidth(rulesArea.getMaxWidth());
			double optionSpacing = 15;
			ruleRow.setSpacing(optionSpacing);

			if (ruleIndex > 0) {
				Line dividingLine = new Line();
				dividingLine.setStartX(-.5 * rulesArea.getMaxWidth());
				dividingLine.setEndX(.5 * rulesArea.getMaxWidth());
				dividingLine.setStartY(0);
				dividingLine.setEndY(0);
				dividingLine.setStrokeWidth(2);
				dividingLine.setStroke(Color.DARKGREY);
				rulesArea.getChildren().add(dividingLine);
			}

			ToggleGroup toggleGroup = new ToggleGroup();
			MutableList<GameRuleOption> ruleOptionList = Lists.mutable.empty();
			ruleOptionList.addAll(gameRule.getChoices());
			for (int ruleOptionIndex = 0; ruleOptionIndex < ruleOptionList.size(); ruleOptionIndex++) {
				GameRuleOption ruleOption = ruleOptionList.get(ruleOptionIndex);
				javafx.scene.layout.Region paddingRegion = new javafx.scene.layout.Region();
				HBox.setHgrow(paddingRegion, Priority.ALWAYS); // [10] [11]
				MutableList<String> optionStringList = Utilities.splitByNewLine(ruleOption.getName());
				if (optionStringList.isEmpty())
					continue;
				RadioButton optionRadio = new RadioButton(optionStringList.get(0));
				optionRadio.setToggleGroup(toggleGroup);
				optionRadio.setSelected(gameRule.isSelected(ruleOption, player));
				optionRadio.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
				optionRadio.setDisable(!ruleOption.isAllowed());
				if (ruleOption.isAllowed()) {
					optionRadio.setTextFill(Color.BLACK);
					optionRadio.setOnAction(e -> {
						toggleGroup.selectToggle(optionRadio);
						gameRule.changeForPlayer(player, ruleOption);
						updateAchieveSafeText();
					});
				} else {
					optionRadio.setTextFill(Color.GREY);
				}
				VBox radioColumn = new VBox(optionRadio);
				radioColumn.setAlignment(Pos.TOP_CENTER);
				radioColumn.setSpacing(2);
				for (int optionLineIndex = 1; optionLineIndex < optionStringList.size(); optionLineIndex++) {
					Text optionLineText = new Text(optionStringList.get(optionLineIndex));
					optionLineText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					optionLineText.setTextAlignment(TextAlignment.CENTER);
					if (!ruleOption.isAllowed()) {
						optionLineText.setFill(Color.DARKGREY);
					}
					radioColumn.getChildren().add(optionLineText);
				}
				if (!ruleOption.isAchieveSafe()) {
					Text blockAchieveText = new Text("Blocks Achievements");
					blockAchieveText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
					blockAchieveText.setTextAlignment(TextAlignment.CENTER);
					if (ruleOption.isAllowed()) {
						blockAchieveText.setFill(errorColor);
					} else {
						blockAchieveText.setFill(Color.DARKGREY);
					}
					radioColumn.getChildren().add(blockAchieveText);
				}
				if (!ruleOption.isAllowed()) {
					MutableList<String> blockedOptionStringList = Utilities
							.splitByNewLine("This Setting has been Blocked\n" + "by the Game Admins");
					for (String blockedLineString : blockedOptionStringList) {
						Text adminBlockText = new Text(blockedLineString);
						adminBlockText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
						adminBlockText.setTextAlignment(TextAlignment.CENTER);
						adminBlockText.setFill(Color.DARKGREY);
						radioColumn.getChildren().add(adminBlockText);
					}
				}
				ruleRow.getChildren().addAll(paddingRegion, radioColumn);
			}
			VBox.setVgrow(ruleRow, Priority.ALWAYS); // [10] [11]
			rulesArea.getChildren().add(ruleRow);
			if (ruleIndex > 0) {
				javafx.scene.layout.Region vertPadding = new javafx.scene.layout.Region();
				vertPadding.setMinHeight(10);
				VBox.setVgrow(vertPadding, Priority.ALWAYS); // [10] [11]
				rulesArea.getChildren().add(vertPadding);
			}
		}
		updateAchieveSafeText();

		Line lowerBoundLine = new Line();
		lowerBoundLine.setStartX(-.5 * rulesArea.getMaxWidth());
		lowerBoundLine.setEndX(.5 * rulesArea.getMaxWidth());
		lowerBoundLine.setStartY(0);
		lowerBoundLine.setEndY(0);
		lowerBoundLine.setStrokeWidth(2);
		lowerBoundLine.setStroke(Color.DARKGREY);
		rulesArea.getChildren().add(lowerBoundLine);
		scrollPanel = new ScrollPane(rulesArea);
		double scrollMarginWidth = 60;
		scrollPanel.setMinWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMaxWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMinHeight(windowHeight * 0.6 + scrollMarginWidth);
		scrollPanel.setMaxHeight(windowHeight * 0.6 + scrollMarginWidth);
		rulesArea.setTranslateX(scrollMarginWidth / 2);
		rulesArea.setTranslateY(scrollMarginWidth / 2);
		scrollPanel.setFitToHeight(true);
		scrollPanel.setFitToWidth(true);
		scrollPanel.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setBackground(Background.EMPTY);
		scrollPanel.setStyle("-fx-background: rgb(0, 0, 0, 0);\n" + "-fx-background-color: rgb(0, 0, 0, 0);"); // [12]

		achieveSafeText.setTextAlignment(TextAlignment.CENTER);
		achieveSafeText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 18));
		HBox buttonRow = new HBox(backButton, achieveSafeText, playButton);
		buttonRow.setAlignment(Pos.CENTER);
		buttonRow.setSpacing(windowWidth * 0.1);
		buttonRow.setPadding(new Insets(10));

		playerMenu.getChildren().addAll(scrollPanel, buttonRow);
		playerMenu.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
		playerMenu.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
		playerMenu.setMaxHeight(windowHeight * 0.85);
		playerMenu.setMinHeight(windowHeight * 0.85);
		playerMenu.setMaxWidth(windowWidth * 0.85);
		playerMenu.setMinWidth(windowWidth * 0.85);
		playerMenu.setSpacing(20);
		playerMenu.setAlignment(Pos.CENTER);
		curtainCanvas.getChildren().add(playerMenu);
		Scene scene = new Scene(curtainCanvas, windowWidth, windowHeight);
		stage.setScene(scene);
		stage.show();

	}

	public void checkCustomChar(Button playButton) {
		customCharReady = (!customCultures.isEmpty() && !customFullNameField.getText().isEmpty()
				&& customAgeField.getText().matches("[0-9]+"));
		playButton.setDisable(!customCharReady);
	}

	public void createChar(Scenario scenario) {
		visitedCreateChar = true;
		customCharReady = false;
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(Color.SLATEGREY);
		curtainCanvas.getChildren().add(curtain);
		int dispYear = year;
		String era = "CE";
		if (scenario.getDate().get(GregorianCalendar.ERA) == 0) {
			era = "BCE";
			dispYear = 1 - year;
		}
		if (era.equals("CE") && year >= 1000) {
			era = "";
		}
		Text titleDate = new Text(
				String.format("%s - %d %s\n" + "Create a Character", scenario.getTitle(), dispYear, era));
		titleDate.setTextAlignment(TextAlignment.CENTER);
		titleDate.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 26));

		Button backButton = new Button("Back");
		styleButton(backButton);
		backButton.setOnAction(e -> {
			afterChooseScenario(scenario);
		});

		Button playButton = new Button("Continue");
		styleButton(playButton);
		playButton.setOnAction(e -> {
			Person person = new Person(game, customFullNameField.getText());
			game.getIndividuals().add(person);
			game.getActors().add(person);
			person.setCultures(customCultures);
			person.setReligions(customReligions);
			int ageInt = Integer.parseInt(customAgeField.getText());
			int trueYear = scenario.getDate().get(GregorianCalendar.YEAR);
			if (scenario.getDate().get(GregorianCalendar.ERA) == 0) {
				trueYear = 1 - trueYear;
			}
			int birthYear = trueYear - ageInt;
			int birthMonthNumber = customMonthMenu.getSelectionModel().getSelectedIndex();
			int birthdayOfMonth = customBirthdayMenu.getSelectionModel().getSelectedItem();
			if (birthMonthNumber > 0 || birthdayOfMonth > 1) {
				birthYear--;
			}
			birthdayOfMonth = Utilities.clipDayOfMonth(birthYear, birthMonthNumber, birthdayOfMonth);
			GregorianCalendar birthdate = new GregorianCalendar(birthYear, birthMonthNumber, birthdayOfMonth);
			person.setDateOfBirth(birthdate);
			Gender customGender = new Gender(game, genderSlider.getValue(), assignedSlider.getValue(),
					apparentSlider.getValue(), attractionSlider.getValue(), kinseySlider.getValue());
			person.setGender(customGender);
			person.setAgingOn(true);
			if (game.getLivingPeople() == null) {
				game.setLivingPeople(Lists.mutable.empty());
			}
			game.getLivingPeople().add(person);
			playerChar = person;
			chooseRules(scenario);
		});
		CustomCharTimerTask cctt = new CustomCharTimerTask(this, playButton);
		timer.scheduleAtFixedRate(cctt, 100, 100);
		playButton.setDisable(true);

		VBox overallMenu = new VBox();
		HBox nameFieldRow = new HBox();
		TextField[] nameFields = { customFullNameField, customShortNameField, customFamilyNameField };
		String[] nameFieldLabels = { "Full Name (Required)", "Short Name (Optional)",
				"Family or Dynasty Name (Optional)" };
		for (int i = 0; i < nameFields.length; i++) {
			Text fieldLabel = new Text(nameFieldLabels[i]);
			fieldLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			fieldLabel.setTextAlignment(TextAlignment.CENTER);
			TextField field = nameFields[i];
			field.setAlignment(Pos.CENTER);
			field.setMaxWidth(windowWidth * 0.2);
			field.setMinWidth(windowWidth * 0.2);
			VBox nameFieldWithLabel = new VBox(fieldLabel, field);
			nameFieldWithLabel.setAlignment(Pos.CENTER);
			nameFieldRow.getChildren().add(nameFieldWithLabel);
			if (i < nameFields.length - 1) {
				javafx.scene.layout.Region horizPad = new javafx.scene.layout.Region();
				HBox.setHgrow(horizPad, Priority.ALWAYS);
				nameFieldRow.getChildren().add(horizPad);
			}
		}
		overallMenu.getChildren().add(nameFieldRow);

		Text ageLabel = new Text("Age (Required)");
		ageLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
		ageLabel.setTextAlignment(TextAlignment.CENTER);
		customAgeField.setAlignment(Pos.CENTER);
		customAgeField.setMaxWidth(windowWidth * 0.2);
		customAgeField.setMinWidth(windowWidth * 0.2);
		VBox ageWithLabel = new VBox(ageLabel, customAgeField);
		ageWithLabel.setAlignment(Pos.CENTER);
		ageWithLabel.setMinWidth(windowWidth * 0.2);
		javafx.scene.layout.Region horizPadLeft = new javafx.scene.layout.Region();
		HBox.setHgrow(horizPadLeft, Priority.ALWAYS);
		nameFieldRow.getChildren().add(horizPadLeft);

		Text monthLabel = new Text("Date of Birth - Month");
		monthLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
		monthLabel.setTextAlignment(TextAlignment.CENTER);
		for (int monthIndex = 1; monthIndex <= 12; monthIndex++) {
			String monthName = Utilities.gregorianMonthNames(lang, monthIndex - 1);
			customMonthMenu.getItems().add(monthName);
		}
		customMonthMenu.getSelectionModel().select(0);
		customMonthMenu.setMinWidth(windowWidth * 0.1);
		VBox monthWithLabel = new VBox(monthLabel, customMonthMenu);
		monthWithLabel.setMinWidth(windowWidth * 0.2);
		monthWithLabel.setAlignment(Pos.CENTER);
		javafx.scene.layout.Region horizPadRight = new javafx.scene.layout.Region();
		HBox.setHgrow(horizPadRight, Priority.ALWAYS);
		nameFieldRow.getChildren().add(horizPadRight);

		Text dayLabel = new Text("Date of Birth - Day");
		dayLabel.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
		dayLabel.setTextAlignment(TextAlignment.CENTER);
		for (int dayIndex = 1; dayIndex <= 31; dayIndex++) {
			customBirthdayMenu.getItems().add(dayIndex);
		}
		customBirthdayMenu.getSelectionModel().select(Integer.valueOf(1));
		customBirthdayMenu.setMinWidth(windowWidth * 0.05);
		VBox dayWithLabel = new VBox(dayLabel, customBirthdayMenu);
		dayWithLabel.setMinWidth(windowWidth * 0.2);
		dayWithLabel.setAlignment(Pos.CENTER);

		HBox ageRow = new HBox(ageWithLabel, horizPadLeft, monthWithLabel, horizPadRight, dayWithLabel);
		ageRow.setAlignment(Pos.CENTER);
		overallMenu.getChildren().add(ageRow);

		Text genderTitle = new Text("Gender");
		genderTitle.setTextAlignment(TextAlignment.CENTER);
		genderTitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
		overallMenu.getChildren().add(genderTitle);

		HBox genderRow = new HBox();
		genderRow.setAlignment(Pos.CENTER);
		String[] genderSubHeadings = { "True Gender", "Assigned Gender", "Apparent Gender" };
		String[] genderExplanations = { "(How You See Yourself)", "(How You Were Classified At Birth)",
				"(How You Are Perceived By Strangers)" };
		String[][] genderDirectionGuides = { { "Male", "Nonbinary", "Female" }, { "Male", "Nonbinary", "Female" },
				{ "Masculine", "Androgynous", "Feminine" } };
		Slider[] genderSliders = { genderSlider, assignedSlider, apparentSlider };
		for (int colIndex = 0; colIndex < 3; colIndex++) {
			Text genSubHeadText = new Text(genderSubHeadings[colIndex]);
			Text genExpText = new Text(genderExplanations[colIndex]);
			for (Text genText : new Text[] { genSubHeadText, genExpText }) {
				genText.setTextAlignment(TextAlignment.CENTER);
				genText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			}
			HBox dirGuidesRow = new HBox();
			dirGuidesRow.setAlignment(Pos.CENTER);
			dirGuidesRow.setSpacing(windowWidth * 0.06);
			for (String dirLabelString : genderDirectionGuides[colIndex]) {
				Text dirGuideText = new Text(dirLabelString);
				dirGuideText.setTextAlignment(TextAlignment.CENTER);
				dirGuideText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 12.5));
				dirGuidesRow.getChildren().add(dirGuideText);
			}
			Slider currentSlider = genderSliders[colIndex];
			VBox genderCol = new VBox(genSubHeadText, genExpText, dirGuidesRow, currentSlider);
			genderCol.setAlignment(Pos.CENTER);
			genderCol.setMinWidth(windowWidth * 0.20);
			genderRow.getChildren().add(genderCol);
			if (colIndex < 2) {
				javafx.scene.layout.Region genderRowSpace = new javafx.scene.layout.Region();
				HBox.setHgrow(genderRowSpace, Priority.ALWAYS);
				genderRow.getChildren().add(genderRowSpace);
			}
		}
		overallMenu.getChildren().add(genderRow);

		Text oriTitle = new Text("Sexual Orientation");
		oriTitle.setTextAlignment(TextAlignment.CENTER);
		oriTitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
		overallMenu.getChildren().add(oriTitle);

		HBox oriRow = new HBox();
		genderRow.setAlignment(Pos.CENTER);
		String[] oriSubHeadings = { "Preferred Gender", "Stength of Sexual Attraction", "Apparent Gender" };
		String[][] oriDirectionGuides = { { "Prefer Men", "No Preference", "Prefer Women" },
				{ "None (Asexual)", "Typical", "High" }, };
		Slider[] oriSliders = { kinseySlider, attractionSlider };
		for (int colIndex = 0; colIndex < 2; colIndex++) {
			Text oriHeaderText = new Text(oriSubHeadings[colIndex]);
			oriHeaderText.setTextAlignment(TextAlignment.CENTER);
			oriHeaderText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			HBox dirGuidesRow = new HBox();
			dirGuidesRow.setAlignment(Pos.CENTER);
			dirGuidesRow.setSpacing(windowWidth * 0.06);
			for (String dirLabelString : oriDirectionGuides[colIndex]) {
				Text dirGuideText = new Text(dirLabelString);
				dirGuideText.setTextAlignment(TextAlignment.CENTER);
				dirGuideText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 12.5));
				dirGuidesRow.getChildren().add(dirGuideText);
			}
			Slider currentSlider = oriSliders[colIndex];
			VBox oriCol = new VBox(oriHeaderText, dirGuidesRow, currentSlider);
			oriCol.setAlignment(Pos.CENTER);
			oriCol.setMinWidth(windowWidth * 0.20);
			oriRow.getChildren().add(oriCol);
			if (colIndex == 0) {
				javafx.scene.layout.Region oriRowSpace = new javafx.scene.layout.Region();
				HBox.setHgrow(oriRowSpace, Priority.ALWAYS); // [10]
				oriRow.getChildren().add(oriRowSpace);
			}
		}
		overallMenu.getChildren().add(oriRow);

		HBox culSelectionMenu = new HBox();
		Text cultureTitle = new Text("Cultures");
		cultureTitle.setTextAlignment(TextAlignment.CENTER);
		cultureTitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
		overallMenu.getChildren().addAll(cultureTitle);
		overallMenu.setAlignment(Pos.CENTER);
		overallMenu.setSpacing(15);

		MutableList<Long> cultureIDlist = Lists.mutable.empty();
		game.getCultures().forEach(c -> cultureIDlist.add(c.getId()));
		MutableList<Culture> cultureList = Lists.mutable.empty();
		Collections.sort(cultureIDlist, new PeopleGroupComparator(game));
		cultureIDlist.forEach(cid -> cultureList.add((Culture) game.lookupActorByID(cid)));

		VBox latestCol = new VBox();
		int colHeight = 1 + cultureList.size() / 6;
		for (int i = 0; i < cultureList.size(); i++) {
			if (i >= colHeight && i % colHeight == 0) {
				culSelectionMenu.getChildren().add(latestCol);
				javafx.scene.layout.Region horizPad = new javafx.scene.layout.Region();
				horizPad.setMinSize(0, 0);
				HBox.setHgrow(horizPad, Priority.ALWAYS); // [10]
				culSelectionMenu.getChildren().add(horizPad);
				latestCol.setAlignment(Pos.CENTER_LEFT);
				latestCol = new VBox();
			}
			Culture culture = cultureList.get(i);
			CheckBox check = new CheckBox(culture.getExonym());
			check.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			check.setTextFill(Color.BLACK);
			check.setOnAction(e -> {
				if (check.isSelected()) {
					customCultures.add(culture);
				} else {
					customCultures.remove(culture);
				}
			});
			latestCol.getChildren().add(check);
			if (i % colHeight != colHeight - 1) {
				javafx.scene.layout.Region paddingRegion = new javafx.scene.layout.Region();
				paddingRegion.setMinSize(0, 0);
				VBox.setVgrow(paddingRegion, Priority.ALWAYS); // [10]
				latestCol.getChildren().add(paddingRegion);
			}
		}
		if (!latestCol.getChildren().isEmpty()) {
			culSelectionMenu.getChildren().add(latestCol);
			latestCol.setAlignment(Pos.CENTER_LEFT);
		}

		ScrollPane culScroll = new ScrollPane(culSelectionMenu);
		double scrollMarginWidth = 60;
		culScroll.setMinWidth(windowWidth * 0.72 + scrollMarginWidth);
		culScroll.setMaxWidth(windowWidth * 0.72 + scrollMarginWidth);
		culScroll.setMinHeight(windowHeight * 0.25 + scrollMarginWidth);
		culScroll.setMaxHeight(windowHeight * 0.25 + scrollMarginWidth);
		culScroll.setFitToHeight(true);
		culScroll.setFitToWidth(true);
		culScroll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		culScroll.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		culScroll.setBackground(Background.EMPTY);
		culScroll.setStyle("-fx-background: rgb(200, 200, 200, 1);\n" + "-fx-background-color: rgb(200, 200, 200, 1);\n"
				+ " -fx-border-radius: 30px;"); // [12]
		culScroll.setPadding(new Insets(20, 20, 20, 20));
		culScroll.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(2))));
		overallMenu.getChildren().add(culScroll);

		Text religionTitle = new Text("Religions");
		religionTitle.setTextAlignment(TextAlignment.CENTER);
		religionTitle.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 20));
		MutableList<Long> religionIdList = Lists.mutable.empty();
		game.getReligions().forEach(c -> religionIdList.add(c.getId()));
		MutableList<Religion> religionList = Lists.mutable.empty();
		Collections.sort(religionIdList, new PeopleGroupComparator(game));
		religionIdList.forEach(rid -> {
			if (game.lookupActorByID(rid) instanceof Religion) {
				religionList.add((Religion) game.lookupActorByID(rid));
			}
		});

		HBox relSelectionMenu = new HBox();
		VBox latestRelCol = new VBox();
		int relColHeight = 1 + religionList.size() / 6;
		for (int i = 0; i < religionList.size(); i++) {
			if (i >= relColHeight && i % relColHeight == 0) {
				relSelectionMenu.getChildren().add(latestRelCol);
				javafx.scene.layout.Region horizPad = new javafx.scene.layout.Region();
				horizPad.setMinSize(0, 0);
				HBox.setHgrow(horizPad, Priority.ALWAYS);
				relSelectionMenu.getChildren().add(horizPad);
				latestRelCol.setAlignment(Pos.CENTER_LEFT);
				latestRelCol = new VBox();
			}
			Religion religion = religionList.get(i);
			CheckBox check = new CheckBox(religion.getExonym());
			check.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			check.setTextFill(Color.BLACK);
			check.setOnAction(e -> {
				if (check.isSelected()) {
					customReligions.add(religion);
				} else {
					customReligions.remove(religion);
				}
			});
			latestRelCol.getChildren().add(check);
			if (i % relColHeight != relColHeight - 1) {
				javafx.scene.layout.Region paddingRegion = new javafx.scene.layout.Region();
				paddingRegion.setMinSize(0, 0);
				VBox.setVgrow(paddingRegion, Priority.ALWAYS);
				latestRelCol.getChildren().add(paddingRegion);
			}
		}
		if (!latestRelCol.getChildren().isEmpty()) {
			relSelectionMenu.getChildren().add(latestRelCol);
			latestRelCol.setAlignment(Pos.CENTER_LEFT);
		}
		ScrollPane relScroll = new ScrollPane(relSelectionMenu);
		relScroll.setMinWidth(windowWidth * 0.72 + scrollMarginWidth);
		relScroll.setMaxWidth(windowWidth * 0.72 + scrollMarginWidth);
		relScroll.setMinHeight(windowHeight * 0.25 + scrollMarginWidth);
		relScroll.setMaxHeight(windowHeight * 0.25 + scrollMarginWidth);
		relScroll.setFitToHeight(true);
		relScroll.setFitToWidth(true);
		relScroll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		relScroll.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		relScroll.setBackground(Background.EMPTY);
		relScroll.setStyle("-fx-background: rgb(200, 200, 200, 1);\n" + "-fx-background-color: rgb(200, 200, 200, 1);\n"
				+ " -fx-border-radius: 30px;"); // [12]
		relScroll.setPadding(new Insets(20, 20, 20, 20));
		relScroll.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(2))));
		overallMenu.getChildren().addAll(religionTitle, relScroll);

		HBox buttonRow = new HBox(backButton, playButton);
		buttonRow.setAlignment(Pos.CENTER);
		buttonRow.setSpacing(windowWidth * 0.1);

		scrollPanel = new ScrollPane(overallMenu);
		scrollPanel.setMinWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMaxWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMinHeight(windowHeight * 0.6 + scrollMarginWidth);
		scrollPanel.setMaxHeight(windowHeight * 0.6 + scrollMarginWidth);
		scrollPanel.setFitToHeight(true);
		scrollPanel.setFitToWidth(true);
		scrollPanel.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setBackground(Background.EMPTY);
		scrollPanel.setStyle("-fx-background: rgb(0, 0, 0, 0);\n" + "-fx-background-color: rgb(0, 0, 0, 0);"); // [12]
		scrollPanel.setPadding(new Insets(20, 20, 20, 20));
		VBox playerMenu = new VBox(titleDate, scrollPanel, buttonRow);
		playerMenu.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
		playerMenu.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
		playerMenu.setMaxHeight(windowHeight * 0.85);
		playerMenu.setMinHeight(windowHeight * 0.85);
		playerMenu.setMaxWidth(windowWidth * 0.85);
		playerMenu.setMinWidth(windowWidth * 0.85);
		playerMenu.setSpacing(20);
		playerMenu.setAlignment(Pos.CENTER);
		curtainCanvas.getChildren().add(playerMenu);
		Scene scene = new Scene(curtainCanvas, windowWidth, windowHeight);
		stage.setScene(scene);
		stage.show();
	}

	public void chooseAnyChar(Scenario scenario) {
		visitedChooseAny = true;
		charSelected = false;
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(Color.SLATEGREY);
		curtainCanvas.getChildren().add(curtain);

		String era = scenario.getDate().get(GregorianCalendar.ERA) == 0 ? "BCE" : "CE";
		if (era.equals("CE") && year >= 1000) {
			era = "";
		}
		Text titleDate = new Text(String.format("%s - %d %s", scenario.getTitle(), year, era));
		titleDate.setTextAlignment(TextAlignment.CENTER);
		titleDate.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 26));

		Button backButton = new Button("Back");
		styleButton(backButton);
		backButton.setOnAction(e -> {
			chooseKeyChar(scenario);
		});

		Button playButton = new Button("Continue");
		styleButton(playButton);
		playButton.setOnAction(e -> {
			chooseRules(scenario);
		});
		playButton.setDisable(true);
		;

		HBox selectionMenu = new HBox();
		scrollPanel = new ScrollPane(selectionMenu);
		double scrollMarginWidth = 60;
		scrollPanel.setMinWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMaxWidth(windowWidth * 0.75 + scrollMarginWidth);
		scrollPanel.setMinHeight(windowHeight * 0.6 + scrollMarginWidth);
		scrollPanel.setMaxHeight(windowHeight * 0.6 + scrollMarginWidth);
		selectionMenu.setTranslateX(scrollMarginWidth / 2);
		selectionMenu.setTranslateY(scrollMarginWidth / 2);
		scrollPanel.setFitToHeight(true);
		scrollPanel.setFitToWidth(true);
		scrollPanel.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPanel.setBackground(Background.EMPTY);
		scrollPanel.setStyle("-fx-background: rgb(0, 0, 0, 0);\n" + "-fx-background-color: rgb(0, 0, 0, 0);"); // [12]
		VBox latestCol = new VBox();
		MutableList<Person> livingList = Lists.mutable.empty();
		livingList.addAll(game.getLivingPeople());
		ToggleGroup tg = new ToggleGroup();
		int numberOfCols = 4;
		int colHeight = 1 + livingList.size() / numberOfCols;
		for (int i = 0; i < livingList.size(); i++) {
			if (i >= colHeight && i % colHeight == 0) {
				selectionMenu.getChildren().add(latestCol);
				javafx.scene.layout.Region horizPad = new javafx.scene.layout.Region();
				horizPad.setMinSize(0, 0);
				HBox.setHgrow(horizPad, Priority.ALWAYS);
				horizPad.setMaxSize(75, 0);
				selectionMenu.getChildren().add(horizPad);
				latestCol.setAlignment(Pos.CENTER_LEFT);
				latestCol = new VBox();
			}
			Person person = livingList.get(i);
			RadioButton radio = new RadioButton(person.getFullName());
			radio.setAlignment(Pos.CENTER_LEFT);
			radio.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			radio.setTextFill(Color.BLACK);
			radio.setToggleGroup(tg);
			radio.setOnAction(e -> {
				tg.selectToggle(radio);
				playerChar = person;
				charSelected = true;
				playButton.setDisable(!charSelected);
			});
			latestCol.getChildren().add(radio);
			if (i % colHeight != colHeight - 1) {
				javafx.scene.layout.Region paddingRegion = new javafx.scene.layout.Region();
				paddingRegion.setMinSize(0, 0);
				VBox.setVgrow(paddingRegion, Priority.ALWAYS);
				latestCol.getChildren().add(paddingRegion);
			}
		}
		if (!latestCol.getChildren().isEmpty()) {
			selectionMenu.getChildren().add(latestCol);
			latestCol.setAlignment(Pos.CENTER_LEFT);
		}

		HBox buttonRow = new HBox(backButton, playButton);
		buttonRow.setAlignment(Pos.CENTER);
		buttonRow.setSpacing(windowWidth * 0.1);

		VBox playerMenu = new VBox(titleDate, scrollPanel, buttonRow);
		playerMenu.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
		playerMenu.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
		playerMenu.setMaxHeight(windowHeight * 0.85);
		playerMenu.setMinHeight(windowHeight * 0.85);
		playerMenu.setMaxWidth(windowWidth * 0.85);
		playerMenu.setMinWidth(windowWidth * 0.85);
		playerMenu.setSpacing(20);
		playerMenu.setAlignment(Pos.CENTER);
		curtainCanvas.getChildren().add(playerMenu);
		Scene scene = new Scene(curtainCanvas, windowWidth, windowHeight);
		stage.setScene(scene);
		stage.show();
	}

	public void chooseKeyChar(Scenario scenario) {
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(Color.SLATEGREY);
		curtainCanvas.getChildren().add(curtain);
		String era = scenario.getDate().get(GregorianCalendar.ERA) == 0 ? "BCE" : "CE";
		if (era.equals("CE") && year >= 1000) {
			era = "";
		}
		Text titleDate = new Text(String.format("%s - %d %s", scenario.getTitle(), year, era));
		titleDate.setTextAlignment(TextAlignment.CENTER);
		titleDate.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 26));
		VBox characterSelector = new VBox();
		characterSelector.setAlignment(Pos.CENTER);
		HBox latestRow = new HBox();
		MutableList<VBox> charPanes = Lists.mutable.empty();
		for (int i = 0; i <= scenario.getKeyCharacters().size() + 2; i++) {
			if (i >= 8 && i % 8 == 0) {
				characterSelector.getChildren().add(latestRow);
				latestRow.setAlignment(Pos.CENTER);
				latestRow = new HBox();
			}
			long personID = -1;
			Person person;
			if (i < scenario.getKeyCharacters().size()) {
				personID = scenario.getKeyCharacters().get(i);
				person = (Person) game.lookupActorByID(personID);
			} else {
				person = new Person(game, "Another Historical Character");
				person.setId(scenario.getKeyCharacters().size() - 1 - i);
				person.setPortraitPath("");
				if (person.getId() == -2) {
					person.setFullName("Random Historical Character");
				} else if (person.getId() == -3) {
					person.setFullName("Create Your Own Character");
				}
			}
			VBox charPane = new VBox();
			charPane.setAlignment(Pos.CENTER);
			if (person == null)
				continue;
			StackPane imagePane = new StackPane();
			if (!person.getPortraitPath().isEmpty()) {
				String imagePath = Utilities.getFullPathName("resources/images/portraits", person.getPortraitPath());
				Image face = new Image(imagePath);
				ImageView charView = new ImageView();
				charView.setImage(face);
				charView.setFitHeight(160);
				charView.setFitWidth(130);
				charView.setPreserveRatio(true);
				imagePane.getChildren().add(charView);
				if (person.getFullName().length() > 17) {
					charView.setFitHeight(140);
				}
			}
			imagePane.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
			imagePane.setMaxWidth(140);
			imagePane.setMinWidth(140);
			imagePane.setMaxHeight(170);
			imagePane.setMinHeight(170);
			if (person.getFullName().length() > 17) {
				imagePane.setMinHeight(140);
				imagePane.setMaxHeight(140);
			}
			charPane.getChildren().add(imagePane);
			Text charNameText = new Text(person.getFullName());
			charNameText.setTextAlignment(TextAlignment.CENTER);
			charNameText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 15));
			charNameText.setWrappingWidth(140);
			charPane.getChildren().add(charNameText);
			charPane.setMinHeight(210);
			charPane.setMaxHeight(210);
			charPane.setMinWidth(170);
			charPane.setMaxWidth(170);
			charPane.setBackground(
					new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
			charPane.setBorder(new Border(
					new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(2))));
			charPane.setOnMouseClicked(e -> {
				selectedCharPane = charPane;
				for (VBox otherPane : charPanes) {
					otherPane.setBackground(
							new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
				}
				charPane.setBackground(
						new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
				playerChar = person;
			});
			charPane.setOnMouseEntered(e -> {
				charPane.setBackground(
						new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
			});
			charPane.setOnMouseExited(e -> {
				if (charPane != selectedCharPane) {
					charPane.setBackground(
							new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
				}

			});
			latestRow.getChildren().add(charPane);
			charPanes.add(charPane);
		}
		if (!latestRow.getChildren().isEmpty()) {
			characterSelector.getChildren().add(latestRow);
			latestRow.setAlignment(Pos.CENTER);
		}
		Button backButton = new Button("Back");
		backButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
		backButton.setTextFill(Color.WHITE);
		backButton.setBackground(
				new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
		backButton.setOnMouseEntered(e -> {
			backButton.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
			backButton.setTextFill(Color.BLACK);
		});
		backButton.setOnMouseExited(e -> {
			backButton.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			backButton.setTextFill(Color.WHITE);
		});
		backButton.setMinWidth(200);
		backButton.setMinHeight(30);
		backButton.setTextAlignment(TextAlignment.CENTER);
		backButton.setOnAction(e -> {
			chooseKeyChar(scenario);
		});

		Button playButton = new Button("Continue");
		playButton.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
		playButton.setTextFill(Color.WHITE);
		playButton.setBackground(
				new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
		playButton.setOnMouseEntered(e -> {
			playButton.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
			playButton.setTextFill(Color.BLACK);
		});
		playButton.setOnMouseExited(e -> {
			playButton.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			playButton.setTextFill(Color.WHITE);
		});
		playButton.setMinWidth(200);
		playButton.setMinHeight(30);
		playButton.setTextAlignment(TextAlignment.CENTER);
		playButton.setOnAction(e -> {
			if (playerChar == null || playerChar.getId() == -3) {
				createChar(scenario);
			} else if (playerChar.getId() == -2) {
				int livingCount = game.getLivingPeople().size();
				int randNum = (int) (Math.random() * livingCount);
				Person[] livingArray = {};
				playerChar = game.getLivingPeople().toArray(livingArray)[randNum];
			} else if (playerChar.getId() == -1) {
				chooseAnyChar(scenario);
			} else {
				chooseRules(scenario);
			}
		});
		HBox buttonRow = new HBox(backButton, playButton);
		buttonRow.setAlignment(Pos.CENTER);
		buttonRow.setSpacing(windowWidth * 0.1);
		VBox playerMenu = new VBox(titleDate, characterSelector, buttonRow);
		playerMenu.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
		playerMenu.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
		playerMenu.setMaxHeight(windowHeight * 0.85);
		playerMenu.setMinHeight(windowHeight * 0.85);
		playerMenu.setMaxWidth(windowWidth * 0.85);
		playerMenu.setMinWidth(windowWidth * 0.85);
		playerMenu.setSpacing(20);
		playerMenu.setAlignment(Pos.CENTER);
		curtainCanvas.getChildren().add(playerMenu);
		Scene scene = new Scene(curtainCanvas, windowWidth, windowHeight);
		stage.setScene(scene);
		stage.show();
	}

	public void afterChooseScenario(Scenario scenario) {
		GregorianCalendar startDate = scenario.getDate();
		game.setIngameDate(startDate);
		year = startDate.get(GregorianCalendar.YEAR);
		if (startDate.get(GregorianCalendar.ERA) == 0) {
			year = 1 - year;
		}
		month = startDate.get(GregorianCalendar.MONTH);
		day = startDate.get(GregorianCalendar.DAY_OF_MONTH);
		game.launch();
		game.joinWithConsole(this);
		for (Person p : game.getIndividuals()) {
			if (p.getDateOfBirth().compareTo(game.getIngameDate()) <= 0) {
				if (p.getDateOfDeath() == null || game.getIngameDate().compareTo(p.getDateOfDeath()) < 0) {
					game.getLivingPeople().add(p);
				}
			}
		}
		if (game.getLivingPeople().isEmpty()) {
			createChar(scenario);
		} else {
			chooseKeyChar(scenario);
		}
	}

	@Override
	public void start(Stage primaryStage) {
		for (Button button : allButtons) {
			styleButton(button);
		}
		visitedChooseAny = false;
		visitedCreateChar = false;
		stage = primaryStage;
		StackPane curtainCanvas = new StackPane();
		Rectangle curtain = new Rectangle();
		curtain.setHeight(windowHeight);
		curtain.setWidth(windowWidth);
		curtain.setFill(Color.SLATEGREY);
		curtainCanvas.getChildren().add(curtain);
		VBox playerMenu = new VBox();
		playerMenu.setSpacing(10);
		playerMenu.setAlignment(Pos.CENTER);
		Text titleText = new Text("THE MAKERS OF HISTORY");
		titleText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 48));
		titleText.setTextAlignment(TextAlignment.CENTER);
		titleText.setFill(Color.WHITE);
		playerMenu.getChildren().add(titleText);
		for (Scenario scenario : server.getScenarios()) {
			String era = scenario.getDate().get(GregorianCalendar.ERA) == 0 ? " BCE" : " CE";
			int year = scenario.getDate().get(GregorianCalendar.YEAR);
			if (era.equals(" CE") && year >= 1000) {
				era = "";
			}
			Text titleDate = new Text(String.format("%s\n%d%s", scenario.getTitle(), year, era));
			titleDate.setFont(Font.font("Times New Roman", FontWeight.BOLD, 20));
			titleDate.setTextAlignment(TextAlignment.CENTER);
			Button button = new Button("Play Scenario");
			button.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
			button.setTextFill(Color.WHITE);
			button.setBackground(
					new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
			button.setOnMouseEntered(e -> {
				button.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY)));
				button.setTextFill(Color.BLACK);
			});
			button.setOnMouseExited(e -> {
				button.setBackground(
						new Background(new BackgroundFill(Color.DARKGREY.darker(), new CornerRadii(5), Insets.EMPTY)));
				button.setTextFill(Color.WHITE);
			});
			button.setMinWidth(100);
			button.setMinHeight(30);
			button.setTextAlignment(TextAlignment.CENTER);
			button.setOnAction(e -> {
				afterChooseScenario(scenario);
			});
			VBox leftBox = new VBox(titleDate, button);
			leftBox.setAlignment(Pos.CENTER);
			leftBox.setSpacing(10);
			leftBox.setMaxWidth(230);
			leftBox.setMinWidth(230);
			leftBox.setMaxHeight(170);
			leftBox.setMinHeight(170);
			String imagePath = Utilities.getFullPathName("resources/images/scenarios", scenario.getImage());
			Image scenarioIcon = new Image(imagePath);
			ImageView scenarioView = new ImageView();
			scenarioView.setImage(scenarioIcon);
			scenarioView.setFitHeight(160);
			scenarioView.setFitWidth(160);
			scenarioView.setPreserveRatio(true);
			StackPane imagePane = new StackPane(scenarioView);
			imagePane.setBorder(new Border(
					new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
			imagePane.setMaxWidth(170);
			imagePane.setMinWidth(170);
			imagePane.setMaxHeight(170);
			imagePane.setMinHeight(170);
			Text scenarioDesc = new Text(scenario.getDescription());
			scenarioDesc.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 15));
			scenarioDesc.setTextAlignment(TextAlignment.JUSTIFY);
			scenarioDesc.setWrappingWidth(windowWidth * 0.65);
			HBox scenarioRow = new HBox(leftBox, imagePane, scenarioDesc);
			scenarioRow.setBackground(
					new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
			scenarioRow.setBorder(new Border(
					new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(5))));
			scenarioRow.setAlignment(Pos.CENTER);
			scenarioRow.setMaxWidth(windowWidth * 0.9);
			scenarioRow.setMinWidth(windowWidth * 0.9);
			scenarioRow.setMinHeight(170);
			scenarioRow.setMaxHeight(170);
			scenarioRow.setSpacing(10);
			playerMenu.getChildren().add(scenarioRow);
		}
		curtainCanvas.getChildren().add(playerMenu);
		Scene chooseScenarioScene = new Scene(curtainCanvas, windowWidth, windowHeight);
		primaryStage.setTitle("The Makers of History");
		primaryStage.setScene(chooseScenarioScene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(e -> System.exit(0));
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/*
	 * Sources Used: [1] Genuine Coder, 2018. "JavaFX 3D Tutorial #1 - Basics
	 * Concepts and Making a Sphere". YouTube. Retrieved from
	 * https://www.youtube.com/watch?v=nBjwBmH8OhQ, 24 April 2019. [2] Genuine
	 * Coder, 2018. "JavaFX 3d Tutorial #2 - Camera vs Objects". YouTube. Retrieved
	 * from https://www.youtube.com/watch?v=CO0q99ZJouM, 24 April 2019. [3] Genuine
	 * Coder, 2018. "JavaFX 3d Tutorial #13 - Earth Simulation - Preparing the
	 * Earth." YouTube. Retrieved from
	 * https://www.youtube.com/watch?v=3yp6vhbud2U&list=
	 * PLhs1urmduZ295Ryetga7CNOqDymN_rhB_&index=14, 25 April 2019. [4] Genuine
	 * Coder, 2018. "JavaFX 3d Tutorial #3 - Rotating 3D objects with Keyboard
	 * Input." YouTube. Retrieved from
	 * https://www.youtube.com/watch?v=dNtZVVJ-lBg&list=
	 * PLhs1urmduZ295Ryetga7CNOqDymN_rhB_&index=4 25 April 2019. [5]
	 * "Creating JavaFX Image from Data Array". StackOverflow. Retrieved from
	 * https://stackoverflow.com/questions/42323334/creating-javafx-image-from-data-
	 * array 9 November 2019. [6] "Spherical Coordinate System." Wikipedia.
	 * https://en.wikipedia.org/wiki/Spherical_coordinate_system 5 November 2019.
	 * [7] "Getting Started with JavaFX 3D Graphics". Oracle. Retrieved from
	 * https://docs.oracle.com/javafx/8/3d_graphics/picking.htm 10 November 2019.
	 * [8] Andrewmu and Yassin Hajaj. "How do I set a timer in Java?" StackOverflow.
	 * Retrieved from
	 * https://stackoverflow.com/questions/4044726/how-to-set-a-timer-in-java, 8
	 * December 2019. [9] Jenkov, Jakob. "JavaFX FlowPane". Retrieved from
	 * http://tutorials.jenkov.com/javafx/flowpane.html [10] Jeff G.
	 * "JavaFX HBox HGrow Priority". StackOverflaow. Retrieved from
	 * https://stackoverflow.com/questions/37191212/javafx-hbox-hgrow-priority, 26
	 * January 2020. [11] JOSEMAFUEN, abarisone, ItachiUchiha.
	 * "JavaFX HBox Alignment". StackOverflow, 2015-16. Retrieved from
	 * https://stackoverflow.com/questions/29707882/javafx-hbox-alignment, 26
	 * January 2020. [12] Jon Onstott, eychef.
	 * "ScrollPanes in JavaFX 8 always have gray background". StackOverflow,
	 * 2014-19. Retrieved from
	 * https://stackoverflow.com/questions/22952531/scrollpanes-in-javafx-8-always-
	 * have-gray-background, 26 January 2020.
	 */

}