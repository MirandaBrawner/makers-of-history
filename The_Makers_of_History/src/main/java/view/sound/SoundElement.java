package view.sound;

public class SoundElement {
	
	private String filePath;
	private double seconds;
	
	private int[] getHMSvalues() {
		int hourValue = (int)(seconds / 3600);
		int minuteValue = ((int)(seconds / 60)) % 60;
		int secondValue = ((int)(seconds) % 60);
		int[] result = new int[3];
		result[0] = hourValue;
		result[1] = minuteValue;
		result[2] = secondValue;
		return result;
	}
	
	private String getHMSstring() {
		int[] array = getHMSvalues();
		return String.format("%d:%d:%d", array[0], array[1], array[2]);
	}
}