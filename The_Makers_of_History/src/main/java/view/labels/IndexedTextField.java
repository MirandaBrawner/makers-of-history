package view.labels;

import javafx.scene.control.TextField;

public class IndexedTextField extends TextField {
	
	public final int index;
	
	public IndexedTextField(int index) {
		this.index = index;
	}
	
	public IndexedTextField(int index, String text) {
		super(text);
		this.index = index;
	}

}
