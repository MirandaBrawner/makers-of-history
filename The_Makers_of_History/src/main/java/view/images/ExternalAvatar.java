package view.images;
public class ExternalAvatar {
	private String title;
	private String description;
	private String filePath;
	
	public ExternalAvatar(String title, String description, String filePath) {
		this.title = title;
		this.description = description;
		this.filePath = filePath;
	}
	
	public ExternalAvatar() {
		
		title = "A Maker of History";
		description = "You can add a description to your avatar here.";
		
		/* "Blue Marble" by Harrison Schmitt and Ron Evans. NASA, 1972. 
		 Retrieved from https://www.nasa.gov/multimedia/imagegallery/image_feature_329.html,
		 23 November 2019. */
		filePath = "src/main/resources/images/blueMarble.jpg";
		
	}
}