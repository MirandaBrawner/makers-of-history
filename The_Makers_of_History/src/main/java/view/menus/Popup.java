package view.menus;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import models.ideas.Decision;
import systemState.Utilities;
import view.Launch;
import view.labels.IndexedTextField;

@SuppressWarnings("restriction")
public class Popup {
	
	private Launch launch;
	private Decision decision;
	private double widthFraction;
	private double heightFraction;
	private MutableList<IndexedTextField> fieldList = Lists.mutable.empty();
	private MutableList<Integer> blockedChoices = Lists.mutable.empty();
	
	public Popup(Launch launch, Decision decision) {
		this(launch, decision, 0.4, 0.4);
	}
	
	public Popup(Launch launch, Decision decision, double widthFraction, double heightFraction) {
		super();
		this.launch = launch;
		this.decision = decision;
		this.widthFraction = widthFraction;
		this.heightFraction = heightFraction;
	}

	public VBox display() {
		Text titleText = new Text(decision.getTitle());
		titleText.setFont(Font.font("Times New Roman", FontWeight.BOLD, 24));
		titleText.setTextAlignment(TextAlignment.CENTER);
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.CENTER);
		vbox.setSpacing(5);
		if (decision.getPicturePaths() != null && !decision.getPicturePaths().isEmpty()) {
			HBox imageRow = new HBox();
			for (String path: decision.getPicturePaths()) {
				String fullImagePath = Utilities.getFullPathName("resources/images", path);
				Image eventPicture = new Image(fullImagePath);
				ImageView eventPictureView = new ImageView(eventPicture);
				eventPictureView.setFitHeight(200);
				eventPictureView.setFitWidth(200);
				eventPictureView.setPreserveRatio(true);
				imageRow.getChildren().add(eventPictureView);
			}
			imageRow.setAlignment(Pos.CENTER);
			vbox.getChildren().add(imageRow);
			javafx.scene.layout.Region vertPad = new javafx.scene.layout.Region();
			vertPad.setMinSize(10, 10);
			vbox.getChildren().add(vertPad);
			VBox.setVgrow(vertPad, Priority.SOMETIMES);
		}
		if (decision.getText() != null && !decision.getText().isEmpty()) {
			Text mainText = new Text(decision.getText());
			mainText.setFont(Font.font("Times New Roman", FontWeight.NORMAL, 16));
			mainText.setTextAlignment(TextAlignment.CENTER);
			vbox.getChildren().add(mainText);
			javafx.scene.layout.Region vertPad2 = new javafx.scene.layout.Region();
			vertPad2.setMinSize(10, 10);
			vbox.getChildren().add(vertPad2);
			VBox.setVgrow(vertPad2, Priority.SOMETIMES);
		}
		for (int inputIndex = 0; inputIndex < decision.getInputStrings().size(); inputIndex++) {
			String startingString = decision.getInputStrings().get(inputIndex);
			IndexedTextField inputBox = new IndexedTextField(inputIndex, startingString);
			inputBox.setAlignment(Pos.CENTER);
			inputBox.setMaxWidth(100);
			vbox.getChildren().add(inputBox);
			fieldList.add(inputBox);
		}
		for (int choiceIndex = 0; choiceIndex < decision.getChoices().size(); choiceIndex++) {
			String optionText = decision.getChoices().get(choiceIndex);
			IndexedButton button = new IndexedButton(choiceIndex, optionText);
			vbox.getChildren().add(button);
			button.setFont(Font.font("Times New Roman", FontWeight.BOLD, 15));
			button.setTextFill(Color.DARKGREY.darker());
			button.setOnMouseEntered(e -> {
				button.setTextFill(Color.BLACK);
			});
			button.setOnMouseExited(e -> {
				button.setTextFill(Color.DARKGREY.darker());
			});
			button.setMinWidth(100);
			// button.setMinWidth(launch.windowWidth * widthFraction * 0.8);
			 button.setMaxWidth(launch.windowWidth * widthFraction * 0.8);
			button.setMinHeight(30);
			button.setTextAlignment(TextAlignment.CENTER);
			if (blockedChoices.contains(choiceIndex)) {
				button.setDisable(true);
				button.setText("(Option Unavailable)");
				button.setTextFill(Color.GREY);
			}
			button.setOnAction(e -> {
				for (int i = 0; i < decision.getInputStrings().size() && i < fieldList.size(); i++) {
					decision.getInputStrings().set(i, fieldList.get(i).getText());
				}
				launch.game.handleDecision(decision, launch, button.index);
			});
		}
		javafx.scene.layout.Region vertPad3 = new javafx.scene.layout.Region();
		vertPad3.setMinSize(100, 30);
		vbox.getChildren().add(vertPad3);
		VBox.setVgrow(vertPad3, Priority.ALWAYS);
		double scrollMarginWidth = 60;
		ScrollPane scrollBox = new ScrollPane(vbox);
		scrollBox.setMinWidth(launch.windowWidth * widthFraction);
		scrollBox.setMaxWidth(launch.windowWidth * widthFraction);
		scrollBox.setMinHeight(launch.windowHeight * heightFraction);
		scrollBox.setMaxHeight(launch.windowHeight * heightFraction);
		scrollBox.setFitToHeight(true);
		scrollBox.setFitToWidth(true);
		scrollBox.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollBox.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollBox.setBackground(Background.EMPTY);
		scrollBox.setStyle("-fx-background: rgb(0, 0, 0, 0);\n" + "-fx-background-color: rgb(0, 0, 0, 0);\n"
				+ " -fx-border-radius: 0px;"); // [1]
		scrollBox.setPadding(new Insets(20, 20, 20, 20));
		scrollBox.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(2))));
		
		VBox boxWithTitle = new VBox(titleText, scrollBox);
		boxWithTitle.setBackground(new Background(new BackgroundFill(
				launch.panelColor, CornerRadii.EMPTY, Insets.EMPTY)));
		boxWithTitle.setBorder(new Border(new BorderStroke(
				Color.WHITE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(10))));
		boxWithTitle.setMaxWidth(launch.windowWidth * widthFraction + 1.5 * scrollMarginWidth);
		boxWithTitle.setMinWidth(launch.windowWidth * widthFraction + 1.5 * scrollMarginWidth);
		boxWithTitle.setMaxHeight(launch.windowHeight * heightFraction + 1.5 * scrollMarginWidth);
		boxWithTitle.setAlignment(Pos.CENTER);
		boxWithTitle.setSpacing(10);
		return boxWithTitle;
	}

	@Override
	public String toString() {
		return decision.getTitle();
	}

	public Launch getLaunch() {
		return launch;
	}

	public Decision getDecision() {
		return decision;
	}

	public double getWidthFraction() {
		return widthFraction;
	}

	public double getHeightFraction() {
		return heightFraction;
	}

	public MutableList<Integer> getBlockedChoices() {
		return blockedChoices;
	}

	public void setBlockedChoices(MutableList<Integer> blockedChoices) {
		this.blockedChoices = blockedChoices;
	}
}

/* Sources Used:
 * [1] Jon Onstott, eychef.
	 * "ScrollPanes in JavaFX 8 always have gray background". StackOverflow,
	 * 2014-19. Retrieved from
	 * https://stackoverflow.com/questions/22952531/scrollpanes-in-javafx-8-always-
	 * have-gray-background, 26 January 2020.
 */
