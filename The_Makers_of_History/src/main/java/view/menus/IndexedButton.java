package view.menus;

import javafx.scene.control.Button;

public class IndexedButton extends Button {
	
	public final int index;
	
	public IndexedButton(int index) {
		this.index = index;
	}
	
	public IndexedButton(int index, String text) {
		super(text);
		this.index = index;
	}
	

}
