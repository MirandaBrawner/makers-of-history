package view.menus;

import javafx.scene.control.Slider;

@SuppressWarnings("restriction")
public class PolicySlider extends Slider {
	
	private String policyName;
	
	public PolicySlider(String policyName, double min, double max, double value) {
		super(min, max, value);
		this.policyName = policyName;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	
	
}
