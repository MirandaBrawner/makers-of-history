package systemState;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

class TestTime {

	@Test @Ignore
	void testEraFormatting() {
		int[] inputArray = {0, -1, -999, -1000, 1, 999, 1000, 999999};
		String[] expArray = {"1 BCE", "2 BCE", "1000 BCE", "1001 BCE", "1 CE", "999 CE", "1000", "999999"};
		for (int i = 0; i < inputArray.length; i++) {
			int year = inputArray[i];
			String expected = expArray[i];
			String result = Utilities.exactYearToString(year);
			String message = expected.equals(result) ? "Success" : "Failure";
			System.out.printf("Test %d: %s. (Expected \"%s\", got \"%s\".)\n", i, message, expected, result);
			assertEquals(expected, result);
		}
	}
	
	@Test @Ignore
	void testYearEstimate() {
		int[] inputArray = {-5000, -5200, -4001, -301, -20, -9, -5, 7, 79, 112, 561, 1029, 1657};
		String[] expArray1 = {"c. 5000 BCE", "c. 5200 BCE", "c. 4000 BCE", "c. 300 BCE", "c. 20 BCE", 
				"c. 10 BCE", "c. 1 CE", "c. 10 CE", "c. 80 CE", "c. 110 CE", "c. 560 CE", "c. 1030", "1657"};
		String[] expArray2 = {"c. 5000 BCE", "c. 5200 BCE", "c. 4000 BCE", "302 BCE", "21 BCE", 
				"10 BCE", "6 BCE", "7 CE", "79 CE", "112 CE", "561 CE", "1029", "1657"};
		int cutoff1 = 1480;
		int cutoff2 = -450;
		System.out.println();
		for (int i = 0; i < inputArray.length; i++) {
			int year = inputArray[i];
			String exp1 = expArray1[i];
			String result1 = Utilities.estimatedYearToString(year, cutoff1);
			String message1 = exp1.equals(result1) ? "Success" : "Failure";
			System.out.printf("Test %d: %s. Rounded %d using %d as cutoff date.\n"
					+ "Expected \"%s\", got \"%s\".\n\n", 2*i, message1, year, cutoff1, exp1, result1);
			assertEquals(exp1, result1);
			
			String exp2 = expArray2[i];
			String result2 = Utilities.estimatedYearToString(year, cutoff2);
			String message2 = exp1.equals(result1) ? "Success" : "Failure";
			System.out.printf("Test %d: %s. Rounded %d using %d as cutoff date.\n"
					+ "Expected \"%s\", got \"%s\".\n\n", 2*i + 1, message2, year, cutoff2, exp2, result2);
			assertEquals(exp2, result2);
		}
	}
	
	@Test
	void testClipDay() {
		int[][] inputs = {
				{2008, 1, 29},
				{2007, 1, 29},
				{1900, 1, 29},
				{2000, 1, 29},
				{1970, 0, 32},
				{1980, 0, -1},
				{1750, 8, 31},
				{1990, 0, 0}
		};
		int[] exp = {29, 28, 28, 29, 31, 1, 30, 1};
		for (int index = 0; index < inputs.length; index++) {
			int[] date = inputs[index];
			int actual = Utilities.clipDayOfMonth(date[0], date[1], date[2]);
			boolean result = (actual == exp[index]);
			String message = result ? "Success" : "Failure";
			System.out.printf("Testing %d/%d/%d: %s. Expected %d, got %d.\n",
					date[1] + 1, date[2], date[0], message, exp[index], actual);
			assertEquals(exp[index], actual);
		}
		System.out.println("Truncate Date Test Complete.");
	}

}
