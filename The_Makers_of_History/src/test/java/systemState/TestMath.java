package systemState;

import static org.junit.jupiter.api.Assertions.*; 
import org.junit.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMath {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test @Ignore
	void testAgeFunction() {
		double relError = 0.01;
		double[][] testData = {
				{-2.5, 0.0062},
				{-1, 0.1587},
				{-0.8, 0.2119},
				{-0.5, 0.3085},
				{0, 0.5},
				{0.6, 0.7257},
				{1.5, 0.9332},
				{2.1, 0.9821},
				{3, 0.9987}
		};
		for (double[] row: testData) {
			double result = Utilities.normalCDF(row[0]);
			//System.out.printf("Expected: %.4f, Actual: %.4f\n", row[1], result);
			assertTrue(Utilities.isCloseToRelative(row[1], result, relError));
		}
	}
	
	@Test
	void testRounding() {
		assertTrue(Math.abs(300 - Utilities.roundToSigDigitsFraction(324.5, 1)) < 0.000001);
		assertTrue(Math.abs(75000 - Utilities.roundToSigDigitsFraction(74986.2, 2)) < 0.000001);
		assertTrue(Math.abs(10000 - Utilities.roundToSigDigitsFraction(9957.45, 1)) < 0.000001);
		assertTrue(Math.abs(-223000 - Utilities.roundToSigDigitsFraction(-222997.6, 3)) < 0.000001);
		assertTrue(Math.abs(7654320 - Utilities.roundToSigDigitsFraction(7654321, 6)) < 0.000001);
		System.out.println(Utilities.roundToSigDigitsFraction(Math.PI, 3));
		assertTrue(Math.abs(3.14 - Utilities.roundToSigDigitsFraction(Math.PI, 3)) < 0.000001);
		System.out.println(Utilities.roundToSigDigitsFraction(1.09, 2));
		assertTrue(Math.abs(1.1 - Utilities.roundToSigDigitsFraction(1.09, 2)) < 0.000001);
		System.out.println(Utilities.roundToSigDigitsFraction(-1.09, 2));
		assertTrue(Math.abs(-1.1 - Utilities.roundToSigDigitsFraction(-1.09, 2)) < 0.000001);
		
		
	}
	
	@Test
	void testNumberWords() {
		assertEquals("45 Million", Utilities.estimateNumber(44_987_201));
		assertEquals("7.9 Trillion", Utilities.estimateNumber(7_867_452_098_234L));
		assertEquals("250,000", Utilities.estimateNumber(251_357));
		assertEquals("790 Billion", Utilities.estimateNumber(789_924_493_087L));
		assertEquals("-3600", Utilities.estimateNumber(-3625));
		assertEquals("-4.2", Utilities.estimateNumber(-4.18));
		assertEquals("-10", Utilities.estimateNumber(-9.99));
		assertEquals("-8.0", Utilities.estimateNumber(-7.99));
		assertEquals("Undefined", Utilities.estimateNumber(Double.NaN));
		assertEquals("-Infinity", Utilities.estimateNumber(Double.NEGATIVE_INFINITY));
		assertEquals("-0.000050", Utilities.estimateNumber(-.0000499));
		assertEquals("-0.0032", Utilities.estimateNumber(-.00323));
	}

}
