package systemState;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestRegex {

	@Test
	void testIfInteger() {
		String pattern = "[-]?[0-9]+";
		String[] inputs = {"0", "1", "9", "10", "-0", "-1", "-2", "-9", "-100", "-00",
				"a", "-", ".", "[", "1-1", "-1-1", "1-", "--2", "1a", "-3.5"};
		ArrayList<Boolean> expList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			expList.add(true);
		}
		for (int i = 0; i < 10; i++) {
			expList.add(false);
		}
		for (int i = 0; i < inputs.length; i++) {
			System.out.printf("Testing %s\n", inputs[i]);
			assertEquals(expList.get(i), inputs[i].matches(pattern));
		}
	}

}
