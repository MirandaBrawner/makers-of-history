package systemState;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class TestStringFormatting {

	@Test
	void testSplitByNewline() {
		String[] inputArray = {
			"Line 0\nLine 1",
			"Line 0\\nLine 1\nLine 2",
			"\\nLine 1\nLine 2\\n",
			"\\nLine 1\n\n\\nLine 4"
		};
		String[][] expOutputMatrix = {
			{"Line 0", "Line 1"},
			{"Line 0", "Line 1", "Line 2"},
			{"", "Line 1", "Line 2", ""},
			{"", "Line 1", "", "", "Line 4"}
		};
		for (int testCase = 0; testCase < inputArray.length && testCase < expOutputMatrix.length; testCase++) {
			System.out.printf("Starting Test Case %d.\n", testCase);
			List<String> actualList = Utilities.splitByNewLine(inputArray[testCase]);
			assertEquals(expOutputMatrix[testCase].length, actualList.size());
			System.out.println("Output length matched.");
			for (int lineIndex = 0; lineIndex < expOutputMatrix[testCase].length; lineIndex++) {
				assertEquals(expOutputMatrix[testCase][lineIndex], actualList.get(lineIndex));
				System.out.printf("Line %d matched.\n", lineIndex);
			}
			System.out.printf("Test Case %d Passed.\n", testCase);
			if (testCase < inputArray.length - 1) {
				System.out.println();
			}
		}
	}

}
