package model;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.GregorianCalendar;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import models.actors.Actor;
import models.actors.Culture;
import models.actors.Gender;
import models.actors.Person;
import systemState.Game;
import systemState.NameHandler;
import systemState.Player;
import systemState.Server;
import systemState.Utilities;

class TestCultureNames {
	Server server = Mockito.mock(Server.class);
	Player player = Mockito.mock(Player.class);
	Game game = new Game(server, "password", player);
	

	{
		game.setIngameDate(new GregorianCalendar());
		game.launch();
	}


	@Test
	@Ignore
	void testNameRevision() {
		String[] inputs = {"51817;Criollos;s;51814", "51822;Puerto Rican Criollos;s;51817",
				"51830;Mestizos;s;51814,50011", "51837;Venezuelan Mestizos;s;51830"};
		String[] expected = {"51817;Criollos/as;Criollo/a;51814", 
				"51822;Puerto Rican Criollos/as;Puerto Rican Criollo/a;51817",
				"51830;Mestizos/as;Mestizo/a;51814,50011", 
				"51837;Venezuelan Mestizos/as;Venezuelan Mestizo/a;51830"};
		for (int i = 0; i < inputs.length; i++) {
			String before = inputs[i];
			String exp = expected[i];
			String actual = Utilities.cultureNameTransform(before);
			String status = actual.equals(exp) ? "Success" : "Failure";
			System.out.printf("Test %d: %s. Expected\"%s\",\n"
					+ "Got \"%s\"\n", i, status, exp, actual);
		}
	}

	@Test
	void testFamilyNameVariation() {
		String[] fNames = "Aleksandra Alena Anechka Anastasia".split(" ");
		String[] mNames = "Aleksandr Anatoli Andrei Anton".split(" ");
		String[] surnames = "Smirnov(a) Ivanov(a) Popov(a) Antonov(na/ich)".split(" ");
		String[] expectedArray = ("Aleksandra Smirnova,Alena Ivanova,Anechka Popova,Anastasia Antonovna,"
				+ "Aleksandr Smirnov,Anatoli Ivanov,Andrei Popov,Anton Antonovich").split(",");
		String nameRule = "gs,f=x(x),m=x(),(f/m)";
		int round = 0;
		Game mockGame = Mockito.mock(Game.class);
		Mockito.when(mockGame.nextActorId()).thenReturn(Long.valueOf(round));
		Mockito.when(mockGame.getIngameDate()).thenReturn(new GregorianCalendar());
		String genericName = "Example Name";
		NameHandler handler = new NameHandler(mockGame);
		for (round = 0; round < fNames.length + mNames.length; round++) {
			Person person = new Person(mockGame, genericName);
			person.setGender(new Gender(mockGame));
			person.getGender().setGender(round < fNames.length ? 1 : 0);
			String givenName = round < fNames.length ? fNames[round] : mNames[round % fNames.length];
			String genericSurname = surnames[round % fNames.length];
			String customSurname = handler.transformName(genericSurname, nameRule, person, true);
			String expected = expectedArray[round];
			String actual = handler.getOrderedName(nameRule, givenName, customSurname, "");
			// System.out.printf("Expected \"%s\", got \"%s\".\n", expected, actual);
			assertEquals(expected, actual);
		}
	}

	@Test
	void testUTFcharLoading() {
		Culture everyone = game.getCultureByName("People");
		MutableList<MutableList<String>> defaultNameMatrix = 
				game.getNameHandler().getNameListsForSeveralCultures(Lists.mutable.of(everyone));
		assertNotNull(defaultNameMatrix);
		assertEquals(game.NAME_LIST_NUMBER, defaultNameMatrix.size());
		MutableList<String> feminineGivenNames = defaultNameMatrix.get(1);
		assertTrue(feminineGivenNames.contains("Bogl�rka"));
		assertTrue(feminineGivenNames.contains("Ma�a"));
		assertTrue(feminineGivenNames.contains("Mar�a Jos�"));
	}
	
	@Test
	void testNameGeneration() {
		Culture turkish = game.getCultureByName("Anatolian Turks");
		Culture fula = game.getCultureByName("Fula");
		assertNotNull(fula);
		assertNotNull(turkish);
		Actor actor = game.lookupActorByID(2);
		assertNotNull(actor);
		assertTrue(actor instanceof Person);
		Person mahmud = (Person) actor;
		
		actor = game.lookupActorByID(18);
		assertNotNull(actor);
		assertTrue(actor instanceof Person);
		Person bello = (Person) actor;
		
		actor = game.lookupActorByID(24);
		assertNotNull(actor);
		assertTrue(actor instanceof Person);
		Person salmon = (Person) actor;
		
		MutableList<Person> otherParents = Lists.mutable.empty();
		int numberOfChildren = 20;
		for (int i = 0; i < numberOfChildren; i++) {
			mahmud.haveChild(otherParents);
			bello.haveChild(otherParents);
			salmon.haveChild(otherParents);
		}
		assertEquals(numberOfChildren, mahmud.getLegalChildren().size());
		String filePath = "src/test/resources/testOutput/testPrintNames.txt";
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(filePath), "UTF-8"));
			for (long childId: mahmud.getLegalChildren()) {
				Person child = (Person) game.lookupActorByID(childId);
				bw.write(child.getFullName());
				bw.newLine();
			}
			otherParents = Lists.mutable.empty();
			for (long childId: bello.getLegalChildren()) {
				Person child = (Person) game.lookupActorByID(childId);
				bw.write(child.getFullName());
				bw.newLine();
			}
			for (long childId: salmon.getLegalChildren()) {
				Person child = (Person) game.lookupActorByID(childId);
				bw.write(child.getFullName());
				bw.newLine();
			}
			bw.flush();
			bw.close();
		} catch (FileNotFoundException ex) {
			System.out.printf("A problem has occurred. The file %s was not found.", filePath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}

/* Sources: 
 * Tchrist. "Write a file in UTF-8 using FileWriter (Java)?" Stack Overflow. 
 https://stackoverflow.com/questions/9852978/write-a-file-in-utf-8-using-filewriter-java
 * */
