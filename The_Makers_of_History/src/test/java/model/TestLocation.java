package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Scanner;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import models.places.MapLocation;

public class TestLocation {

	private String locationDataFileName = "src/test/resources/testInput/locationsTestInput.txt";
	private MutableList<MapLocation> places = Lists.mutable.empty();
	private MapLocation currentPlace;
	private int depth = 0;

	@Test
	@Ignore
	public void testReadCityData() {
		File file = new File(locationDataFileName);
		try {
			Scanner scanner = new Scanner(file);

			while(scanner.hasNextLine()) {
				scanLine(scanner.nextLine());
			}
			scanner.close();

		} catch (FileNotFoundException ex) {
			Assertions.fail("Input data file for locations was not found.");
		}
		int expNumberOfPlaces = 3;
		Assertions.assertEquals(expNumberOfPlaces, places.size());
		MutableList<String> nameList = places.collect(place -> place.getName());
		nameList.forEach(name -> System.out.println(name));
		Assertions.assertTrue(nameList.containsAll(Lists.mutable.of(
				"Example Town", "Somewhere", "Village")));
		
	}
	
	public void scanLine(String line) {
		if (line.isEmpty()) return;
		if (line.startsWith("}") && depth == 1) {
			places.add(currentPlace);
			depth--;
			if (line.length() > 1) {
				scanLine(line.substring(1));
			}
		} else if (line.startsWith("{") && depth == 0) {
			currentPlace = new MapLocation();
			depth++;
		} else if (depth == 1) {
			String[] data = line.split(":");
			if (data.length >= 2) {
			String fieldName = data[0].trim();
			String value = data[1].trim();
			if (fieldName.equals("id")) {
				currentPlace.setId(Long.parseLong(value));
			} else if (fieldName.equals("name")) {
				currentPlace.setName(value);
			} else if (fieldName.startsWith("lat")) {
				currentPlace.setLatitude(Double.parseDouble(value));
			} else if (fieldName.startsWith("lon")) {
				currentPlace.setLongitude(Double.parseDouble(value));
			}
			}
		}
	}
}
