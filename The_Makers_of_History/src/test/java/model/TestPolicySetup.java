package model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import models.ideas.Policy;
import systemState.Utilities;

class TestPolicySetup {

	@Test
	void testPolicy() {
		String[] array = {"Very Low", "Low", "Moderate", "Very High", "High"};
		List<String> optionList = Arrays.asList(array);
		Policy policy = new Policy(null, 1, "Income Tax", optionList, null);
		List<Double> numbers = policy.getThresholds();
		assertEquals(4, numbers.size());
		double[] expValues = {0.2, 0.4, 0.6, 0.8};
		double errorLimit = 0.001;
		for (int i = 0; i < numbers.size(); i++) {
			boolean result = Utilities.isCloseToAbsolute(expValues[i], numbers.get(i), errorLimit);
			assertTrue(result);
		}
		double[] testValues = {0.1, 0.3, 0.4};
		String[] expLabels = {"Very Low", "Low", "Moderate"};
		for (int i = 0; i < 3; i++) {
			String actualLabel = policy.valueToLabel(testValues[i]);
			assertEquals(expLabels[i], actualLabel);
		}
	}

}
