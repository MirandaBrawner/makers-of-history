package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Agent {
	
	boolean living;
	double age;
	
	Agent() {
		living = true;
		age = 0;
	}
	void getOlder() {
		if (living) {
			age += (1 / 365.0);
			double seed = Math.random();
			if (seed < oldAgeDeathChance(age)) {
				living = false;
			}
		}
	}
	
	public static double oldAgeDeathChance(double age) {
		double yearlyChance = 0.001;
		double[] thresholds = {20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120};
		double[] values = {0, 0.001, 0.002, 0.005, 0.011, 0.023, 0.058, 0.164, 0.352, 0.573, 0.9};
		if (age <= thresholds[0]) {
			yearlyChance = values[0];
		} else if (age > thresholds[thresholds.length - 1]) {
			yearlyChance = values[thresholds.length - 1];
		} else {
			for (int i = 1; i < thresholds.length; i++) {
				if (age <= thresholds[i] && age > thresholds[i - 1]) {
					double deltaY = values[i] - values[i - 1];
					double deltaX = thresholds[i] - thresholds[i - 1];
					double slope = deltaY / deltaX;
					double offset = age - thresholds[i - 1];
					yearlyChance = slope * offset + values[i - 1];
				}
			}
		}
		return yearlyChance / 365.0;
	}
	
	public static int howManyAreAlive(List<Agent> list) {
		int howMany = 0;
		for (Agent a: list) {
			if (a.living) {
				howMany++;
			}
		}
		return howMany;
	}
	
}

class TestPerson {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	public List<Agent> createAgents(int count) {
		ArrayList<Agent> list = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			list.add(new Agent());
		}
		return list;
	}
	
	public void advanceOneDay(List<Agent> list) {
		for (Agent a: list) {
			a.getOlder();
		}
	}

	@Test
	void testAging() {
		List<Agent> list = createAgents(1000);
		int count = list.size();
		for (int year = 1; year <= 150; year++) {
			for (int day = 0; day < 365; day++) {
				advanceOneDay(list);
			}
			count = Agent.howManyAreAlive(list);
			System.out.printf("After %d years, %d out of %d people are still alive.\n",
					year, count, list.size());
		}
		assertEquals(0, count);
	}

}
